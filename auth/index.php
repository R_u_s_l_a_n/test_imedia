<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
if ($USER->IsAuthorized()) {
	LocalRedirect('/');
	die;
}
$APPLICATION->SetTitle("Восстановление пароля");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND', '/local/templates/.default/front/dist/assets/images/header-inner2.png');
$APPLICATION->SetPageProperty('SECOND_CLASS', 'inner-header__product');
?>
    <section class="delivery-page">
        <div class="container">
            <div class="row">
				<? $APPLICATION->IncludeComponent(
					"bitrix:system.auth.changepasswd",
					"flat",
					Array("AUTH_RESULT" => $APPLICATION->arAuthResult)
				); ?>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>