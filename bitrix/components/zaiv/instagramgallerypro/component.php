<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$MODULE_ID = "zaiv.instagramgallerypro"; 
$MODULE_PARAM = CModule::IncludeModuleEx("zaiv.instagramgallerypro");
use Bitrix\Main\Config\Option;

if($MODULE_PARAM && Option::get('zaiv.instagramgallerypro', 'enable') == "Y"){

	if(Option::get($MODULE_ID, 'access_long_token')){
		$expiresDate = strtotime(Option::get($MODULE_ID, 'token_expires_date'));
		$currentDate = strtotime(date("Y-m-d H:i:s"));
		if(($expiresDate - $currentDate) < 864000){
			$url = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=".Option::get($MODULE_ID, 'access_long_token');
			$output = file_get_contents($url);
			$outputObj = json_decode($output);
			if($outputObj->access_token && $outputObj->expires_in){
				Option::set($MODULE_ID, 'expires_in', $outputObj->expires_in);
				Option::set($MODULE_ID, 'access_long_token', $outputObj->access_token);
				Option::set($MODULE_ID, 'token_expires_date',date("Y-m-d H:i:s",time() + $outputObj->expires_in));
			}
		}
	}

	if($this->StartResultCache()){
		if($MODULE_PARAM == 3){
			$arResult['ERRORS'][] = "ERROR. Demo period expired";
		}else{
			$arParams['GALLERY_UID'] = "gallery".md5(rand(1,1000000000));
			$arParams['MEDIA_COUNT'] = intval($arParams['MEDIA_COUNT']);
			if(!$arParams['MEDIA_COUNT']) $arParams['MEDIA_COUNT'] = 9;
			
			if(!count($arParams['SOURCE']) && $arParams['SOURCE_TYPE'] == 'HASHTAG'){
				$arResult['ERRORS'][] = "No source(s) matches";
			}
	
			$tmpSourceArr = array();
			switch($arParams[SOURCE_TYPE]){								
				case "HASHTAG":
					foreach($arParams['SOURCE'] as $sourceID){
						$sourceID = trim($sourceID," #");
						if($sourceID){
							$tmpSourceArr[] = $sourceID;
						}
					}			
					break;
				case "LOCATION":
					break;
			}
			$arParams['SOURCE'] = $tmpSourceArr;
			unset($tmpSourceArr);
			
			if(!count($arResult['ERRORS'])){
				$zaivInstagramGalleryproObj = new CZaivInstagramgallerypro();
				$arResult = $zaivInstagramGalleryproObj->getGallery($arParams);			
				$arResult = $APPLICATION->ConvertCharsetArray($arResult, "UTF8", (LANG_CHARSET)?LANG_CHARSET:SITE_CHARSET);			
				
				for($i=0;$i<count($arResult['ITEMS']);$i++){
					if($arParams['DEL_SPEC_SYMBOLS_IN_DESCRIPTION']=="Y"){
						$arResult['ITEMS'][$i]['DESCRIPTION'] = str_replace("?"," ",$arResult['ITEMS'][$i]['DESCRIPTION']);
					}
					$arResult['ITEMS'][$i]['DESCRIPTION'] = str_replace('"','&#34;',$arResult['ITEMS'][$i]['DESCRIPTION']);
				}
				
				if(!count($arResult['ITEMS']) && !ini_get('allow_url_fopen')){
					$arResult['ERRORS'][] = "ERROR. PHP allow_url_fopen param must be \"ON\"";	
				}

				if(count($arResult['ERRORS'])){
					$arResult['ERRORS'] = array_merge($arResult['ERRORS'],$zaivInstagramGalleryproObj->errors);
				}else{
					$arResult['ERRORS'] = $zaivInstagramGalleryproObj->errors;
				}

				for($i=0;$i<count($arResult[ITEMS]);$i++){
					$arResult['ITEMS'][$i]['CREATE_TIME_SHORT'] = ConvertTimeStamp($arResult['ITEMS'][$i]['CREATE_TIME_UNIX'], "SHORT");
					$arResult['ITEMS'][$i]['CREATE_TIME'] = ConvertTimeStamp($arResult['ITEMS'][$i]['CREATE_TIME_UNIX'], "FULL");
				}				
			}
		}
		$this->IncludeComponentTemplate();
	}
}
?>