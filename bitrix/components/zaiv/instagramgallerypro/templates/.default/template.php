<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$arParams['CURRENT_COUNT'] = 0;
?>
<?if($arParams['NOINDEX_WIDGET']=="Y"){?><!--googleoff: all--><!--noindex--><?}?>
<?if($arParams['SHOW_PARSING_ERRORS']=="Y"){
	foreach($arResult['ERRORS'] as $errorItem){
		echo "<div class=\"zaiv-instagram-gallery-error\">$errorItem</div>";
	}
}?>
<div class="zaiv-instagram-gallery-container">
	<div class="zaiv-instagram-gallery-media <?=$arParams['GALLERY_UID']?>">
		<?foreach($arResult[ITEMS] as $arItem){
			$arParams['CURRENT_COUNT']++;
			
			if($arItem['TYPE'] == 'video' && $arParams['SHOW_VIDEO_IMAGE_PREVIEW'] == 'N'){
				$currentSourceUrl = "#".$arItem['SHORTCODE'];
				?>			
				<video id="<?=$arItem['SHORTCODE']?>" style="display:none;" preload="auto" controls loop poster="<?=$arItem['IMAGE_PREVIEW']?>" src="<?=$arItem['VIDEO_SOURCE']?>">
					Your browser doesn't support HTML5 video tag.
				</video>				
				<?				
			}else{
				$currentSourceUrl = $arItem['IMAGE_DETAIL'];
			}?>
			<a 
				href="<?=($arParams['SHOW_TYPE']=='WEBSITE')?$currentSourceUrl:$arItem['LINK']?>" 
				<?=($arParams['NOINDEX_LINKS']=='Y')?'rel="nofollow"':''?>
				target="_blank"
				style="background-image:url(<?=$arItem['IMAGE_PREVIEW']?>)"
				<?if($arItem['DESCRIPTION']){?>
					data-caption="<?=$arItem['DESCRIPTION']?>"
					title="<?=$arItem['DESCRIPTION']?>"
				<?}?>
				>
				<?if(
					(($arItem['TYPE'] == 'sidecar' || $arItem['TYPE'] == 'carousel') && $arParams['SHOW_SIDECAR_ICON']== 'Y' && $arParams['SHOW_SIDECAR_MEDIAS']=='Y' && $arParams['SHOW_SIDECAR_ITEMS_ON_PAGE']!='Y') ||
					($arItem['TYPE'] == 'video' && $arParams['SHOW_VIDEO_ICON']!= 'N')
				){?>
					<span class="<?=$arItem['TYPE']?>"></span>
				<?}?>
			</a>			
			<?
			if($arParams['CURRENT_COUNT'] >= $arParams['MEDIA_COUNT']) break;
			
			if($arParams['SHOW_SIDECAR_MEDIAS']=='Y' && ($arItem['TYPE'] == 'sidecar' || $arItem['TYPE'] == 'carousel') && count($arItem['SIDECAR'])>0 && $arParams['SHOW_TYPE']=='WEBSITE'){
				foreach($arItem['SIDECAR'] as $sidecarItem){
					if($arParams['SHOW_SIDECAR_ITEMS_ON_PAGE']=='Y'){
						$arParams['CURRENT_COUNT']++;
						?>
						<a 
							href="<?=$sidecarItem['IMAGE_DETAIL']?>" 
							<?=($arParams['NOINDEX_LINKS']=="Y")?'rel="nofollow"':''?> 
							target="_blank"
							data-type="<?=($arItem['TYPE']!='video')?'image':''?>"							
							style="background-image:url(<?=$sidecarItem['IMAGE_PREVIEW']?>)"
							<?if($arItem['DESCRIPTION']){?>
								data-caption="<?=$arItem['DESCRIPTION']?>"
								title="<?=$arItem['DESCRIPTION']?>"
							<?}?>
							>
						</a>
					<?}else{?>
						<a 
							href="<?=$sidecarItem['IMAGE_DETAIL']?>" 
							style="display:none"
							<?=($arParams['NOINDEX_LINKS']=="Y")?'rel="nofollow"':''?>
							<?if($arItem['DESCRIPTION']){?>
								data-caption="<?=$arItem['DESCRIPTION']?>"
								title="<?=$arItem['DESCRIPTION']?>"
							<?}?>
							>
						</a>
					<?}
					if($arParams['CURRENT_COUNT'] >= $arParams['MEDIA_COUNT']) break;
				}
			}
			if($arParams['CURRENT_COUNT'] >= $arParams['MEDIA_COUNT']) break;
		}?>
	</div>
</div>
<?if($arParams['NOINDEX_WIDGET']=="Y"){?><!--/noindex--><!--googleon: all--><?}?>

<?if($arParams['SHOW_TYPE'] == "WEBSITE"){?>
	<script>
		$(document).ready(function(){
		<?
			switch($arParams['PLUGIN_TYPE']){
				case "FANCYBOX3":
					?>										
					var $links = $('.zaiv-instagram-gallery-media.<?=$arParams['GALLERY_UID']?> a');
					$links.on('click', function(){
						$.fancybox.open( $links,{
							buttons : [
								'fullScreen',
								'zoom',
								'close'
							]
						},$links.index(this));
						return false;
					});
					<?
					break;
				case "MAGNIFICPOPUP":
					?>
					$('.zaiv-instagram-gallery-media.<?=$arParams['GALLERY_UID']?>').magnificPopup({
						delegate: 'a',
						type: 'image',
						gallery:{
							enabled:true
						}
					});
					<?
					break;
			}
		?>
		});
	</script>
<?}?>