<?
IncludeModuleLangFile(__FILE__);
use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem;
Class CSotbitOrderphone
{
    public $error = array();
    public $order = 0;
    private $userID = 0;
    private $fields = [];
    private $arParams = array();
    private $currency = "";
    private $orderPrice = 0;
    private $password = "";

    CONST EVENT_ADD_ORDER = "SOTBIT_NEW_ORDER_PHONE";

    function __construct($arParams, $fields)
    {
        $this->arParams = $arParams;
        $this->siteID = isset($arParams["SITE_ID"])?$arParams["SITE_ID"]:SITE_ID;
        $this->fields = $fields;
    }


    function GetDemo()
    {
        $module_id = 'sotbit.orderphone';
        $module_status = CModule::IncludeModuleEx($module_id);
        if($module_status == '3')
        {   echo GetMessage('sotbit_error_demo');
            return false;
        }

        return true;

    }

    function CheckFields()
    {
        if($this->arParams["SEND_EVENT"]=="Y")
        {
            $arFilter = array(
                "TYPE_ID" => self::EVENT_ADD_ORDER,
                "LID"     => "ru"
            );

            $arET = CEventType::GetList($arFilter)->Fetch();

            if(!$arET)
            {
                $et = new CEventType;
                $et->Add(array(
                    "LID"           => "ru",
                    "EVENT_NAME"    => self::EVENT_ADD_ORDER,
                    "NAME"          => GetMessage('sotbit_new_order_phone_order_name'),
                    "DESCRIPTION"   => ""
                ));
                $arFields = array(
                    "ACTIVE" => "Y",
                    "EVENT_NAME" => self::EVENT_ADD_ORDER,
                    "LID" => $this->siteID,
                    "EMAIL_FROM" => "#EMAIL_FROM#",
                    "EMAIL_TO" => "#EMAIL_TO#",
                    //"BBC" => "#BBC#",
                    "SUBJECT" => GetMessage('sotbit_new_order_phone_order_name'),
                    "BODY_TYPE" => "text",
                    "MESSAGE" => GetMessage('sotbit_new_order_phone_order_message')
                );//return;
                $mes = new CEventMessage;
                $mesID = $mes->Add($arFields);
            }
        }
    }

    function checkEmail($email, $status) {
        if ($status == 'Y') {
            $filter = Array(
                'EMAIL' => $email,
            );
            $rsUsers = CUser::GetList(($by="date_register"), ($order="desc"), $filter, array("SELECT"=>array("ID")));
            while($arUsers = $rsUsers->Fetch()) {
                $userId = $arUsers["ID"];
            }
            if ($userId) {
                $email = explode('@', $email)[0] . '1' . '@' . explode('@', $email)[1];
                $this->checkEmail($email, $status);
            } else {
                return $email;
            }
        } else {
            return $email;
        }
    }

    function StartAjax()
    {
        global $USER;

        if(!$this->GetDemo()){
            return false;
        }

        if (!$USER->IsAuthorized())
        {
            if ($this->arParams['SELECT_USER'] == 'one') {
                $noAuth = true;
                $this->userID = (int) $this->arParams['ONE_USER_ID'];

                $filter = Array(
                    'ID' => $this->userID,
                );
                $rsUsers = CUser::GetList(($by="date_register"), ($order="desc"), $filter, array("SELECT"=>array("ID")));
                while($arUsers = $rsUsers->Fetch()) {
                    $oneUserID = $arUsers["ID"];
                }

                if (empty($oneUserID)) {
                    $this->error[] = GetMessage("SOP_ORDER_ERROR_ONE_USER");
                    return false;
                }

            } else {
                $email_uniq = COption::GetOptionString('main', 'new_user_email_uniq_check');

                $filter = Array(
                    'PERSONAL_PHONE' => $this->arParams["order_phone"],
                );
                $rsUsers = CUser::GetList(($by="date_register"), ($order="desc"), $filter, array("SELECT"=>array("ID")));
                while($arUsers = $rsUsers->Fetch()) {
                    $this->userID = $arUsers["ID"];
                }

                if (!$this->userID) {
                    $phone = $this->arParams["order_phone"];

                    if (!empty($this->arParams["order_email"])) {
                        $email = $this->arParams["order_email"];
                    } elseif (!empty($this->arParams["EMAIL_MASK"]) && strripos($this->arParams["EMAIL_MASK"], "@")) {
                        $email = $this->maskReplacement($this->arParams["EMAIL_MASK"]);
                    } else {
                        $email = str_replace(array(" ", "(", ")", "+", "_", "*", "-"), "", $this->fields['PHONE']) . '@' . $_SERVER['HTTP_HOST'];
                    }
                    $email = $this->checkEmail($email, $email_uniq);
                    $this->fields['EMAIL'] = $email;

                    if (!empty($this->arParams["LOGIN_MASK"])) {
                        $login = $this->maskReplacement($this->arParams["LOGIN_MASK"]);
                    } else {
                        $login = str_replace(array(" ", "(", ")", "+", "_", "*", "-"), "", $this->fields['PHONE']) . '@' . $_SERVER['HTTP_HOST'];
                    }

                    $this->AddUser($login, $phone, $email);
                    if ($this->arParams["SMS_CONFIRM"] == 'Y') {
                        $errorSendLP = $this->sendSMSLoginPassword($phone, $login, $this->password);
                        if (!empty($errorSendLP)) $this->error[] = $errorSendLP[0];
                    }
                } else {
                    $this->error[] = GetMessage("SOP_ORDER_ERROR_PHONE_ISSET");
                    return false;
                }
            }
        } else {
            $userID = $USER->GetID();
            $this->userID = $userID;
        }

        $USER->Authorize($this->userID);

        if(count($this->error)>0) return false;

        if(!isset($this->arParams["PRODUCT_ID"]) || empty($this->arParams["PRODUCT_ID"]))
            $this->AddOrder();
        else
            $this->AddItemOrder();

        $userPassword = $this->password;
        $arUserDate = array(
            'LOGIN' => $USER->GetLogin(),
            'PASSWORD' => $userPassword
        );

        $this->AddOrderProps();

        /* Add user data */
        if ($this->arParams['SELECT_USER'] != 'one') {
            $prefix = ($this->arParams['PERSON_TYPE'] == 1) ? 'PERSONAL_' : 'WORK_';
            $uProps = [];
            foreach ($this->fields as $field => $val) {
                if (!empty($val)) $uProps[$prefix.$field] = $val;
            }
            $USER->Update($USER->GetID(), $uProps);
        }

        if ($noAuth) $USER->Logout();

        return $arUserDate;
    }

    function AddOrderProps()
    {
        $arOrderPropsValue = [];
        $dbOrderPropsValue = CSaleOrderPropsValue::GetList([], ['ORDER_ID' => $this->orderID]);
        while ($orderPropsValue = $dbOrderPropsValue->Fetch()) {
            $arOrderPropsValue[$orderPropsValue['ORDER_PROPS_ID']] = $orderPropsValue;
        }

        $arFilter = array("PERSON_TYPE_ID" => $this->arParams["PERSON_TYPE"]);

        $dbOrderProps = CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            $arFilter,
            false,
            false,
            array("ID", "NAME", "CODE")
        );

        while ($arOrderProps = $dbOrderProps->GetNext())
        {
            if (!empty($this->fields[$arOrderProps["CODE"]])) {
                $value = $this->fields[$arOrderProps["CODE"]];

                $arVars = !empty($arOrderPropsValue[$arOrderProps['ID']]) ? $arOrderPropsValue[$arOrderProps['ID']] : false;
                if ($arVars) {
                    CSaleOrderPropsValue::Update($arVars['ID'], array(
                        'NAME' => $arVars['NAME'],
                        'CODE' => $arVars['CODE'],
                        'ORDER_PROPS_ID' => $arVars['ORDER_PROPS_ID'],
                        'ORDER_ID' => $arVars['ORDER_ID'],
                        'VALUE' => $value,
                    ));
                } else {
                    CSaleOrderPropsValue::Add(array(
                        'NAME' => $arOrderProps['NAME'],
                        'CODE' => $arOrderProps['CODE'],
                        'ORDER_PROPS_ID' => $arOrderProps['ID'],
                        'ORDER_ID' => $this->orderID,
                        'VALUE' => $value,
                    ));
                }
            }
        }

        while ($arOrderProps = $dbOrderProps->Fetch())
        {
            $arResult["ORDER_PROPS"][$arOrderProps["ID"]] = $arOrderProps;
        }
    }

    function AddItemOrder()
    {
        global $USER, $APPLICATION, $DB;
        $QUANTITY = 1;
        $PRODUCT_ID = $this->arParams["PRODUCT_ID"];
        $rsProducts = \Bitrix\Catalog\ProductTable::getList(
            [
                'filter' => ['ID' => $PRODUCT_ID],
                'select' => [
                    'ID',
                    'CAN_BUY_ZERO',
                    'QUANTITY_TRACE',
                    'QUANTITY',
                    'WEIGHT',
                    'WIDTH',
                    'HEIGHT',
                    'LENGTH',
                    'TYPE'
                ]
            ]
        );
        if (!($arCatalogProduct = $rsProducts->fetch()))
        {
            $this->error[] = $APPLICATION->ThrowException(GetMessage('CATALOG_ERR_NO_PRODUCT'), "NO_PRODUCT");
            return false;
        }
        $dblQuantity = doubleval($arCatalogProduct["QUANTITY"]);
        $boolQuantity = ('Y' != $arCatalogProduct["CAN_BUY_ZERO"] && 'Y' == $arCatalogProduct["QUANTITY_TRACE"]);

        $strCallbackFunc = "";
        $strProductProviderClass = "CCatalogProductProvider";

        $arCallbackPrice = CSaleBasket::ReReadPrice($strCallbackFunc, "catalog", $PRODUCT_ID, $QUANTITY, "N", $strProductProviderClass);
        if (!is_array($arCallbackPrice) || empty($arCallbackPrice))
        {
            $this->error[] = $APPLICATION->ThrowException(GetMessage('CATALOG_PRODUCT_PRICE_NOT_FOUND'), "NO_PRODUCT_PRICE");
            return false;
        }

        $mxResult = \CCatalogSku::GetProductInfo($PRODUCT_ID);
        $arSelectProps = array();
        $product_properties0 = $product_properties1 = array();
        if(is_array($mxResult))
        {
            if(isset($this->arParams["PRODUCT_PROPS"]) && !empty($this->arParams["PRODUCT_PROPS"]) && isset($this->arParams["PRODUCT_PROPS_VALUE"]) && !empty($this->arParams["PRODUCT_PROPS_VALUE"]))
            {
                $product_properties0 = \CIBlockPriceTools::CheckProductProperties(
                    $this->arParams["IBLOCK_ID"],
                    $mxResult["ID"],
                    $this->arParams["PRODUCT_PROPS"],
                    $this->arParams["PRODUCT_PROPS_VALUE"],
                    false
                );
                if(!is_array($product_properties0))
                    $product_properties0 = array();
            }
            if(isset($this->arParams["OFFERS_PROPS"]) && !empty($this->arParams["OFFERS_PROPS"]))
            {
                $product_properties1 = \CIBlockPriceTools::GetOfferProperties(
                    $PRODUCT_ID,
                    $this->arParams["IBLOCK_ID"],
                    $this->arParams["OFFERS_PROPS"],
                    ""
                );
                if(!is_array($product_properties1)) $product_properties1 = array();
            }
            $product_properties = array_merge($product_properties0, $product_properties1);

        }
        elseif(isset($this->arParams["PRODUCT_PROPS"]) && !empty($this->arParams["PRODUCT_PROPS"]) && isset($this->arParams["PRODUCT_PROPS_VALUE"]) && !empty($this->arParams["PRODUCT_PROPS_VALUE"])){
            $product_properties = \CIBlockPriceTools::CheckProductProperties(
                $this->arParams["IBLOCK_ID"],
                $PRODUCT_ID,
                $this->arParams["PRODUCT_PROPS"],
                $this->arParams["PRODUCT_PROPS_VALUE"],
                false
            );
        }

        $arProduct = \CIBlockElement::GetList(
            Array(),
            array("ID"=>$PRODUCT_ID, "ACTIVE" => "Y", "ACTIVE_DATE" => "Y", "CHECK_PERMISSIONS" => "Y", "MIN_PERMISSION" => "R"),
            false,
            Array("nTopCount"=>1),
            array_merge(array("ID", "NAME", "DETAIL_PAGE_URL", "IBLOCK_ID", "XML_ID"), $arSelectProps)
        )->GetNext();

        if(!$arProduct)
            return false;

        $arIBlock = \CIBlock::GetList(
            array(),
            array("ID" => $arProduct["IBLOCK_ID"])
        )->Fetch();


        $order = Order::create($this->siteID, $this->userID);
        $order->setPersonTypeId($this->arParams["PERSON_TYPE"]);
        $order->setField('CURRENCY', $arCallbackPrice["CURRENCY"]);

        /* Comment on the order */
        $comment = $this->commentTemplateReplacement();
        $order->setField('USER_DESCRIPTION', $comment);


        $basket = Basket::create($this->siteID);
        $item = $basket->createItem('catalog', $PRODUCT_ID);
        $item->setFields(array(
            'QUANTITY' => ($boolQuantity && $dblQuantity < $QUANTITY ? $dblQuantity : $QUANTITY),
            'CURRENCY' => $arCallbackPrice["CURRENCY"],
            'LID' => $this->siteID,
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ));
        $basketPropertyCollection = $item->getPropertyCollection();
        if($product_properties){
            foreach($product_properties as $prop){
                $basketPropertyCollection->setProperty(array(
                    $prop
                ));
            }
        }
        $basketPropertyCollection->save();
        $order->setBasket($basket);

        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = Delivery\Services\Manager::getById($this->arParams["DELIVERY_ID"]);
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());


        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $paySystemService = PaySystem\Manager::getObjectById($this->arParams["PAY_SYSTEM_ID"]);

        $payment->setFields(array(
            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
        ));
        $payment->setField("SUM", $order->getPrice());
        $payment->setField("CURRENCY", $order->getCurrency());

        $propertyCollection = $order->getPropertyCollection();
        // �������
        $phoneProp = $propertyCollection->getPhone();
        $phoneProp->setValue($fields['PHONE']);
        // ���
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue($fields['NAME']);
        // email
        $emailProp = $propertyCollection->getUserEmail();
        $emailProp->setValue($fields['EMAIL']);


        //$order->doFinalAction(true);
        $result = $order->save();
        if($result->isSuccess()){
            if (CModule::IncludeModule("statistic"))
                CStatistic::Set_Event("sale2basket", "catalog", $arProduct["~DETAIL_PAGE_URL"]);

            $this->orderID = $order->getId();

            if($this->arParams["SEND_EVENT"]=="Y")
            {
                $this->CheckFields();
                $this->AddEvent();
            }
        }
    }

    function AddOrder()
    {
        global $APPLICATION;

        $arErrors = array();
        $arWarnings = array();

        $arShoppingCart = CSaleBasket::DoGetUserShoppingCart($this->siteID, $this->userID, intval(CSaleBasket::GetBasketUserID()), $arErrors);


        $arResultProps["USER_PROFILES"] = CSaleOrderUserProps::DoLoadProfiles($this->userID, $this->arParams["PERSON_TYPE"]);
        $arProfileTmp = array();

        if (!empty($arResultProps["USER_PROFILES"]) && is_array($arResultProps["USER_PROFILES"]))
        {
            foreach($arResultProps["USER_PROFILES"] as $key => $val)
            {
                if ($PROFILE_ID === "")
                {
                    $arResultProps["USER_PROFILES"][$key]["CHECKED"] = "Y";
                    $PROFILE_ID = $key;
                }
                elseif ($PROFILE_ID == $key)
                {
                    $arResultProps["USER_PROFILES"][$key]["CHECKED"] = "Y";
                }
            }
        }
        else
            $PROFILE_ID = (int)$PROFILE_ID;


        $userProfile = $arResultProps["USER_PROFILES"];
        $arPropValues = array();

        $arPropValues = $userProfile[$PROFILE_ID]["VALUES"];
        if ($this->arParams["PERSON_TYPE"] == 2) {
            $rsPaySystems = CSalePaySystem::GetList(array(), Array("LID"=>SITE_ID, "ACTIVE"=>"Y", "PERSON_TYPE_ID"=>2), false, false, array());
            $arPaySystems = array();
            while ($itemPay = $rsPaySystems->Fetch() ) {
                $arPaySystems[] = $itemPay;
            }
            $this->arParams['PAY_SYSTEM_ID'] = $arPaySystems[0]['ID'];
        }

        $arOrderDat = CSaleOrder::DoCalculateOrder(
            $this->siteID,
            $this->userID,
            $arShoppingCart,
            $this->arParams["PERSON_TYPE"],
            array(),
            $this->arParams["DELIVERY_ID"],
            $this->arParams["PAY_SYSTEM_ID"],
            array(),
            $arErrors,
            $arWarnings
        );

        /* Comment on the order */
        $comment = $this->commentTemplateReplacement();

        $arOrderFields = array(
            "LID" => $arOrderDat['LID'],
            "PERSON_TYPE_ID" => $arOrderDat['PERSON_TYPE_ID'],
            "PAYED" => "N",
            "CANCELED" => "N",
            "STATUS_ID" => "N",
            "PRICE" => $arOrderDat['PRICE'],
            "CURRENCY" => $arOrderDat['CURRENCY'],
            "USER_ID" => $arOrderDat['USER_ID'],
            "USER_DESCRIPTION" => $comment,
            "ADDITIONAL_INFO" => ""
        );

        $this->orderID = CSaleOrder::DoSaveOrder($arOrderDat, $arOrderFields, 0, $arErrors);
        if($this->orderID<=0){
            if($ex = $APPLICATION->GetException())
                $this->error[] = $ex->GetString();
            else
                $this->error[] = GetMessage("SOP_ORDER_ERROR");
        }

        if(count($arErrors)==0 && $this->arParams["SEND_EVENT"]=="Y")
        {
            $this->CheckFields();
            $this->AddEvent();
        }

    }

    function AddEvent()
    {
        global $DB, $USER, $APPLICATION;
        $strOrderList = "";
        $arBasketList = array();
        $arResult["ORDER_PRICE"] = $this->orderPrice;
        $arResult["BASE_LANG_CURRENCY"] = $this->currency;

        $dbBasketItems = CSaleBasket::GetList(
            array("NAME" => "ASC"),
            array("ORDER_ID" => $this->orderID),
            false,
            false,
            array("ID", "PRODUCT_ID", "NAME", "QUANTITY", "PRICE", "CURRENCY", "TYPE", "SET_PARENT_ID")
        );
        while ($arItem = $dbBasketItems->Fetch())
        {
            if (CSaleBasketHelper::isSetItem($arItem))
                continue;

            $arBasketList[] = $arItem;
        }

        $arBasketList = getMeasures($arBasketList);

        foreach ($arBasketList as $arItem)
        {
            $measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOP_SHT");

            $strOrderList .= $arItem["NAME"]." - ".$arItem["QUANTITY"]." ".$measureText.": ".SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
            $strOrderList .= "\n";
        }
        $arFields = Array(
            "ORDER_ID" => $this->orderID,
            "ORDER_DATE" => Date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT", $this->siteID))),
            "PRICE" => SaleFormatCurrency($arResult["ORDER_PRICE"], $arResult["BASE_LANG_CURRENCY"]),
            "EMAIL_TO" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
            "PRODUCT_LIST" => $strOrderList,
            "PHONE" => $this->phone,
            "EMAIL_FROM" => COption::GetOptionString("main", "email_from"),
            "SALE_EMAIL" => COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),

        );

        $bSend = true;
        $eventName = self::EVENT_ADD_ORDER;
        foreach(GetModuleEvents("sale", "OnOrderNewSendEmail", true) as $arEvent)
            if (ExecuteModuleEventEx($arEvent, Array($this->orderID, &$eventName, &$arFields))===false)
                $bSend = false;
        if($bSend)
        {
            $event = new CEvent;
            $event->Send($eventName, $this->siteID, $arFields, "N");
        }
    }

    function AddUser($login = null, $phone = null,  $email = null)
    {
        global $USER;
        $def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
        if($def_group!="") {
            $GROUP_ID = explode(",", $def_group);
            $arPolicy = $USER->GetGroupPolicy($GROUP_ID);
        } else
            $arPolicy = $USER->GetGroupPolicy(array());

        if ($this->arParams["USER_GROUP"]!=0)
            $GROUP_ID[] = $this->arParams["USER_GROUP"];

        $password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
        if($password_min_length <= 0)
            $password_min_length = 6;

        $password_chars = array(
            "abcdefghijklnmopqrstuvwxyz",
            "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
            "0123456789",
        );

        if($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
            $password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";

        if ($email)
            $emailRegister = $email;

        if ($login)
            $loginRegister = $login;

        if ($phone)
            $phoneRegister = $phone;

        $this->password = $NEW_PASSWORD = $NEW_PASSWORD_CONFIRM = randString($password_min_length+2, $password_chars);
        $nameRegister = ($this->fields['NAME'])?$this->fields['NAME']:'';

        $user = new CUser;
        $arAuthResult = $user->Add(Array(
                "LOGIN" => $loginRegister,
                "NAME" => $nameRegister,
                "PASSWORD" => $NEW_PASSWORD,
                "CONFIRM_PASSWORD" => $NEW_PASSWORD_CONFIRM,
                "EMAIL" => $emailRegister,
                "GROUP_ID" => $GROUP_ID,
                "ACTIVE" => "Y",
                "PHONE_NUMBER" => $phoneRegister,
                'PERSONAL_PHONE' => $phoneRegister,
                "LID" => $this->siteID,
            )
        );

        if (IntVal($arAuthResult) <= 0)
        {
            $this->error[] = $user->LAST_ERROR;
        }

        else $this->userID = $arAuthResult;

    }

    /**
     * Send to user SMS with verification code
     *
     * @param $userPhone
     * @return array|bool
     */
    public static function sendSMSCode($userPhone)
    {
        $phone = \Bitrix\Main\UserPhoneAuthTable::normalizePhoneNumber($userPhone);
        $code = randString(4, ["0123456789"]);

        $sms = new \Bitrix\Main\Sms\Event(
            'SMS_USER_CONFIRM_NUMBER',
            [
                'USER_PHONE' => $phone,
                'CODE' => $code,
            ]
        );
        $sms->setSite(SITE_ID);
        $sms->setLanguage(LANGUAGE_ID);
        $smsResult = $sms->send(true);

        if ($smsResult->isSuccess()) {
            $_SESSION['SMS_HASH'][] = md5($_SESSION['fixed_session_id'] . $code); // $_SESSION['fixed_session_id'] - salt
            $_SESSION['SMS_ATTEMPT']++;
            return false;
        } else {
            $error_message = $smsResult->getErrorMessages();
            return $error_message;
        }
    }

    /**
     * Send to user SMS with his temporary login and password
     *
     * @param $userPhone
     * @param $login
     * @param $password
     * @return array|bool
     */
    protected function sendSMSLoginPassword($userPhone, $login, $password)
    {
        $phone = \Bitrix\Main\UserPhoneAuthTable::normalizePhoneNumber($userPhone);

        $sms = new \Bitrix\Main\Sms\Event(
            'SMS_USER_LOGIN_PASSWORD',
            [
                'USER_PHONE' => $phone,
                'LOGIN' => $login,
                'PASSWORD' => $password,
            ]
        );
        $sms->setSite(SITE_ID);
        $sms->setLanguage('ru');
        $smsResult = $sms->send(true);

        if ($smsResult->isSuccess()) {
            return false;
        } else {
            $error_message = $smsResult->getErrorMessages();
            return $error_message;
        }
    }

    /**
     * Placeholder replacement in login/email mask
     *
     * @param $mask
     * @return string
     */
    protected function maskReplacement($mask)
    {
        $prepared = str_replace(array(" ", "|"), "", $mask);

        if (strripos($prepared, "#PHONE_NUMBERS#") !== false) {
            $phoneN = str_replace(array(" ", "(", ")", "+", "_", "*", "-"), "", $this->fields['PHONE']);
            $prepared = str_replace("#PHONE_NUMBERS#", $phoneN, $prepared);
        }

        if (strripos($prepared, "#NAME#") !== false) {
            if (!empty($this->fields["NAME"])) {
                /* if (NAME) contains Cyrillic characters - replace it */
                if (preg_match("/[�-��-߸�]/i", $this->fields["NAME"]) || preg_match("/[�-��-߸�]/iu", $this->fields["NAME"])) {
                    $params = array("replace_space" => ".", "replace_other" => "");
                    $name = Cutil::translit($this->fields["NAME"], "ru", $params);
                } else {
                    $name = str_replace(array("(", ")", "+", "_", "*", "-"), "", $this->fields["NAME"]);
                    $name = str_replace(" ", ".", $name);
                }

                $prepared = str_replace("#NAME#", $name, $prepared);
            } else {
                $prepared = str_replace("#NAME#", 'user', $prepared);
            }
        }

        if (strripos($prepared, "#EMAIL#") !== false) {
            if (!empty($this->arParams["order_email"])) {
                $prepared = str_replace("#EMAIL#", $this->arParams["order_email"], $prepared);
            }
        }

        if (strripos($prepared, "#HOST_NAME#") !== false) {
            $prepared = str_replace("#HOST_NAME#", $_SERVER['HTTP_HOST'], $prepared);
        }

        return $prepared;
    }

    /**
     * Placeholder replacement in comment template mask
     *
     * @return bool|mixed|string
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function commentTemplateReplacement()
    {
        if (!empty($this->arParams['COMMENT_TEMPLATE'])) {
            $comment = $this->arParams['COMMENT_TEMPLATE'];
        } else {
            if ($this->arParams['PERSON_TYPE'] == 2) {
                $comment = GetMessage("SOP_DEFAULT_COMMENT_TEMPLATE_2");
            } else {
                $comment = GetMessage("SOP_DEFAULT_COMMENT_TEMPLATE_1");
            }
        }
        $comment = \Bitrix\Main\Text\Encoding::convertEncodingToCurrent($comment);

        $replaceProps = [];

        $rs = CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            array(),
            false,
            false,
            array("ID", "PERSON_TYPE_ID", "CODE")
        );

        while ($prop = $rs->fetch()) {
            $replaceProps[] = $prop['CODE'];
        }

        $replaceProps[] = 'COMMENT';

        foreach ($replaceProps as $rp) {
            $comment = str_replace('#PROP_'.$rp.'#', $this->fields[$rp], $comment);
        }

        return $comment;
    }

}
?>
