<?
use Bitrix\Main,
	Bitrix\Main\Sms\TemplateTable,
    Bitrix\Main\SiteTable,
	Bitrix\Main\Config\Option;

IncludeModuleLangFile(__FILE__);
Class sotbit_orderphone extends CModule
{
	const MODULE_ID = 'sotbit.orderphone';
	var $MODULE_ID = 'sotbit.orderphone'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("sotbit.orderphone_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("sotbit.orderphone_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("sotbit.orderphone_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("sotbit.orderphone_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		//RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CSotbitOrderphone', 'OnBuildGlobalMenu');
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		//UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CSotbitOrderphone', 'OnBuildGlobalMenu');
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p.'/'.$item, $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/'.$item, $ReWrite = True, $Recursive = True);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function UnInstallFiles()
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
						continue;

					$dir0 = opendir($p0);
					while (false !== $item0 = readdir($dir0))
					{
						if ($item0 == '..' || $item0 == '.')
							continue;
						DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
					}
					closedir($dir0);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallSMSEvent();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallSMSEvent();
	}

	function InstallSMSEvent() {
        $arFilter = ["EVENT_NAME" => "SMS_USER_LOGIN_PASSWORD"];
        $arET = CEventType::GetList($arFilter)->Fetch();

        if (!$arET) {
            $et = new CEventType;
            $et->Add([
                "LID" => "ru",
                "EVENT_TYPE" => "sms",
                "EVENT_NAME" => "SMS_USER_LOGIN_PASSWORD",
                "NAME" => "���������� ����� � ������ �� ���",
                "DESCRIPTION" => "#USER_PHONE# - ����� ��������
                #LOGIN# - ����� ������������
                #PASSWORD# - ������ ������������",
            ]);
            $et->Add([
                "LID" => "en",
                "EVENT_TYPE" => "sms",
                "EVENT_NAME" => "SMS_USER_LOGIN_PASSWORD",
                "NAME" => "Sending login and password with SMS",
                "DESCRIPTION" => "#USER_PHONE# - phone number
                #LOGIN# - user login
                #PASSWORD# - user password",
            ]);
        }

        $entity = TemplateTable::getEntity();

        $template = $entity->createObject();
        $template->set("EVENT_NAME", "SMS_USER_LOGIN_PASSWORD");
        $template->set("ACTIVE", "Y");
        $template->set("LANGUAGE_ID", "ru");
        $template->set("SENDER", "#DEFAULT_SENDER#");
        $template->set("RECEIVER", "#USER_PHONE#");
        $template->set("MESSAGE", "#SITE_NAME#
		��������� ������ ��� �����
		�����: #LOGIN#
		������: #PASSWORD#");

        $rsSites = SiteTable::getList();
        while($arSite = $rsSites->fetch())
        {
            $site = Main\SiteTable::getEntity()->wakeUpObject($arSite['LID']);
            $template->addToSites($site);
        }

        $template->save();
        $template_id = $template['ID'];
        Option::set($this->MODULE_ID, "sms_template_id", $template_id);

        return true;
	}

	function UnInstallSMSEvent() {
        $id = (int) Option::get($this->MODULE_ID, "sms_template_id");
        TemplateTable::delete($id);

        $et = new CEventType;
        $et->Delete("SMS_USER_LOGIN_PASSWORD");

        return true;
    }
}
?>
