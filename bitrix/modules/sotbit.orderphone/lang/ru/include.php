<?
$MESS["SOP_NO_PRODUCT"] = "Товары отсутствуют в корзине";
$MESS["SOP_COMMENTS"] = "Заказ по телефону: ";
$MESS["SOP_ORDER_ERROR"] = "Ошибка оформления заказа!";
$MESS["SOP_ORDER_ERROR_ISSET"] = "Пользователь с таким Email уже зарегистрирован. Войдите на сайт, чтобы оформить заказ.";
$MESS["SOP_ORDER_ERROR_ONE_USER"] = "Пользователь с указанным для привязки заказа ID не существует";
$MESS["SOP_ORDER_ERROR_PHONE_ISSET"] = "Пользователь с таким номером уже зарегистрирован. Войдите на сайт, чтобы оформить заказ.";
$MESS["SOP_DEFAULT_COMMENT_TEMPLATE_1"] = "Пользователь #PROP_FIO# заказал(а) товары по телефону #PROP_PHONE# и оставил(а) комментарий: #PROP_COMMENT#";
$MESS["SOP_DEFAULT_COMMENT_TEMPLATE_2"] = "Контактное лицо #PROP_CONTACT_PERSON# компании #PROP_COMPANY# заказал(а) товары по телефону #PROP_PHONE# и оставил(а) комментарий: #PROP_COMMENT#";
$MESS["sotbit_new_order_phone_order_name"] = "Оформлен новый заказ по телефону № #ORDER_ID#";
$MESS["sotbit_new_order_phone_order_message"] = "Оформлен новый заказ по телефону № #ORDER_ID#\r\n\r\n Состав заказа:\r\n\r\n#PRODUCT_LIST# \r\n\r\n Номер телефона покупателя: #PHONE#\r\n\r\n Для просмотра заказа в интернет-магазине пройдите по ссылке http://#SERVER_NAME#/bitrix/admin/sale_order_detail.php?ID=#ORDER_ID#&lang=ru";
$MESS["SOP_SHT"] = "шт.";
$MESS["CATALOG_ERR_NO_PRODUCT"] = "Продукт в каталоге не найден";
$MESS["CATALOG_PRODUCT_PRICE_NOT_FOUND"] = "Цена не найдена";
$MESS["sotbit_error_demo"] = "<h3 style='color:red' >Срок дейставия <a target='_blank' href='http://marketplace.1c-bitrix.ru/solutions/sotbit.orderphone/'>демо-режима</a>  истек! Функционал модуля будет недоступен!</h3>";
?>