<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Config\Option;
$MODULE_ID = 'zaiv.instagramgallerypro';

if($APPLICATION->GetGroupRight($MODULE_ID) != "W"){
	echo('Access denied to settings of '.$MODULE_ID);
}else{
	if($_REQUEST['ACTION'] == "Y"){
		$arParam = array();
		$arParams['service_report_enable'] = Option::get($MODULE_ID, 'service_report_enable');
		$arParams['app_id'] = Option::get($MODULE_ID, 'app_id');
		$arParams['redirect_url'] = Option::get($MODULE_ID, 'redirect_url');
		$arParams['app_secret'] = Option::get($MODULE_ID, 'app_secret');
		$arParams['access_code'] = trim(Option::get($MODULE_ID, 'access_code'),'#_');
		$arParams['access_short_token'] = Option::get($MODULE_ID, 'access_short_token');
	
		switch($_REQUEST['TYPE']){
			case "SHORT":
				if($arParams['app_id'] && $arParams['redirect_url'] && $arParams['app_secret'] && $arParams['access_code']){
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,"https://api.instagram.com/oauth/access_token");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 
						http_build_query(
							array(
								'app_id' => $arParams['app_id'],
								'app_secret' => $arParams['app_secret'],
								'grant_type' => 'authorization_code',
								'redirect_uri' => $arParams['redirect_url'],
								'code' => $arParams['access_code'],
							)
						)
					);
					$output = curl_exec($ch);
					curl_close($ch);
			
					$outputObj = json_decode($output);
					if($outputObj->access_token && $outputObj->user_id){
						Option::set($MODULE_ID, 'user_id', $outputObj->user_id);
						Option::set($MODULE_ID, 'access_short_token', $outputObj->access_token);
						echo "ok";
					}else{
						echo "err_s2";
					}
				}else{
					echo "err_s1";
				}
			break;
			case "LONG":
				if($arParams['app_secret'] && $arParams['access_code']){
					$url = "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=".$arParams['app_secret']."&access_token=".$arParams['access_short_token'];
					$output = file_get_contents($url);
					$outputObj = json_decode($output);
					if($outputObj->access_token && $outputObj->expires_in){
						Option::set($MODULE_ID, 'expires_in', $outputObj->expires_in);
						Option::set($MODULE_ID, 'access_long_token', $outputObj->access_token);
						Option::set($MODULE_ID, 'token_expires_date',date("Y-m-d H:i:s",time() + $outputObj->expires_in));
						echo "ok";
					}else{
						echo "err_l2";
					}
				}else{
					echo "err_l1";
				}
				break;
			case "UPDATE":
				if($_REQUEST['access_long_token']){
					$returnArr = [];
					$url = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=".$_REQUEST['access_long_token'];
					$output = file_get_contents($url);
					$outputObj = json_decode($output);
					if($outputObj->access_token && $outputObj->expires_in){
						Option::set($MODULE_ID, 'expires_in', $outputObj->expires_in);
						Option::set($MODULE_ID, 'access_long_token', $outputObj->access_token);
						Option::set($MODULE_ID, 'token_expires_date',date("Y-m-d H:i:s",time() + $outputObj->expires_in));
						$returnArr['result'] = 'ok';
						$returnArr['access_long_token'] = $outputObj->access_token;
						$returnArr['token_expires_date'] = date("Y-m-d H:i:s",time() + $outputObj->expires_in);
					}else{
						$returnArr = ['result' => 'err_u2'];
					}
				}else{
					$returnArr = ['result' => 'err_u1'];
				}
				echo json_encode($returnArr);
				break;
		}
	}else{
		echo "err_1";
	}
}