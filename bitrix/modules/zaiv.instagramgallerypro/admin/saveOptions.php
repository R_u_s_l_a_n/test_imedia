<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Config\Option;
$MODULE_ID = 'zaiv.instagramgallerypro';

if($APPLICATION->GetGroupRight($MODULE_ID) != "W"){
	echo('Access denied to settings of '.$MODULE_ID);
}else{
	if($_REQUEST['UPDATE'] == "Y" && strlen($_REQUEST['PARAM_NAME']) && strlen($_REQUEST['PARAM_VALUE'])){
		Option::set($MODULE_ID, $_REQUEST['PARAM_NAME'], $_REQUEST['PARAM_VALUE']);
		echo "ok";
	}else{
		echo "err";
	}
}