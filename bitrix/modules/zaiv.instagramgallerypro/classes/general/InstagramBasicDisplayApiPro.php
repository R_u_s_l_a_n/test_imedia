<?
class CZaivInstagramBasicDisplayApiPro{
	var $GRAPH_USER 			= "https://graph.instagram.com/me/?fields=id,username,media_count,account_type&access_token=";
	var $GRAPH_MEDIA 			= "https://graph.instagram.com/me/media?limit=#LIMIT#&fields=id,caption,media_type,media_url,permalink,thumbnail_url,timestamp&access_token=";
	var $GRAPH_CHILDREN_MEDIA 	= "https://graph.instagram.com/#MEDIA_ID#/children?access_token=";
	var $GRAPH_MEDIA_ID 		= "https://graph.instagram.com/#MEDIA_ID#?fields=id,media_type,media_url,timestamp&access_token=";
	
	var $MEDIA_COUNT;

	var $INSTAGRAM_TOKEN;

	function __construct($INSTAGRAM_TOKEN,$MEDIA_COUNT=10)
	{
		$this->INSTAGRAM_TOKEN = $INSTAGRAM_TOKEN;
		$this->MEDIA_COUNT = $MEDIA_COUNT;
		$this->setEndpoints();
	}

	private function setEndpoints(){
		$this->GRAPH_USER .= $this->INSTAGRAM_TOKEN;
		$this->GRAPH_MEDIA = str_replace('#LIMIT#',$this->MEDIA_COUNT,$this->GRAPH_MEDIA).$this->INSTAGRAM_TOKEN;
		$this->GRAPH_CHILDREN_MEDIA .= $this->INSTAGRAM_TOKEN;
		$this->GRAPH_MEDIA_ID .= $this->INSTAGRAM_TOKEN;
	}

	public function getAccount(){
		$responseJson = $this->getEndPointContent($this->GRAPH_USER);
		$arrResponse = json_decode($responseJson);
		return $arrResponse;
	}

	public function getMedias(){	
		$responseJson = $this->getEndPointContent($this->GRAPH_MEDIA);
		$arrResponse = json_decode($responseJson);
		return $arrResponse;
	}

	private function getMediaId($MEDIA_ID){
		$responseJson = $this->getEndPointContent(str_replace("#MEDIA_ID#",$MEDIA_ID,$this->GRAPH_MEDIA_ID));
		$arrResponse = json_decode($responseJson);
		return $arrResponse;
	}

	public function getCarousel($MEDIA_ID){
		$responseIdsJson = $this->getEndPointContent(str_replace("#MEDIA_ID#",$MEDIA_ID,$this->GRAPH_CHILDREN_MEDIA));
		$arrIdsResponse = json_decode($responseIdsJson);	
		if(count($arrIdsResponse->data)){
			foreach($arrIdsResponse->data as $arItem){
				$arrResponse[] = $this->getMediaId($arItem->id);
			}
		}/**/
		return $arrResponse;
	}

	private function getEndPointContent($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');
		$content = curl_exec($ch);
		curl_close($ch);
		return $content;
	}
}