<?
/**
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
/**/
global $USER;
use Bitrix\Main\Config\Option;

if(!class_exists("InstagramScraper\Instagram")){
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/zaiv.instagramgallerypro/classes/general/InstagramScraper.php");
}
if(!class_exists("CZaivInstagramBasicDisplayApiPro")){
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/zaiv.instagramgallerypro/classes/general/InstagramBasicDisplayApiPro.php");
}
if(!class_exists("Unirest\Exception")){
	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/zaiv.instagramgallerypro/classes/general/Unirest.php");	
}

class CZaivInstagramgallerypro
{
	var $MODULE_ID = 'zaiv.instagramgallerypro';
	var $INSTAGRAM_OBJECT;
	var $arResult;
	var $proxyList;
	var $errors = array();
	var $needUseToken = false;

	function __construct()
	{
		$this->INSTAGRAM_OBJECT = new \InstagramScraper\Instagram();
		$this->arResult['ITEMS'] = array();
		$this->proxyList = "http://cdn.zaiv.ru/proxy-list.txt";
	}
	
	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
	
	}
	
	function getProxy(){		
		$arrProxy = file($this->proxyList);
		$proxy = explode(" ",$arrProxy[rand(0,count($arrProxy)-1)]);
		$proxy = explode(":",$proxy[0]);
		return array(
				"IP" => $proxy[0],
				"PORT" => $proxy[1]
		);
	}

	function getGallery($arParams){
		global $APPLICATION;
		if(Option::get($this->MODULE_ID, 'access_long_token')){
			$this->needUseToken = true;
		}

		$mediaObjArr = array();
		switch($arParams['SOURCE_TYPE']){
			case "TOKEN":
				if($this->needUseToken){
					$this->INSTAGRAM_BASIC_API_OBJECT = new CZaivInstagramBasicDisplayApiPro(Option::get($this->MODULE_ID, 'access_long_token'),$arParams['MEDIA_COUNT']);		
					$mediaObj = $this->INSTAGRAM_BASIC_API_OBJECT->getMedias();
					if(count($mediaObj->data)){
						$cnt = 0;
						foreach($mediaObj->data as $arItem){
							if($cnt++ < $arParams['MEDIA_COUNT']){																												
								$tmpMediaArray = array(
									"TYPE_TOKEN" 		=> $arItem->media_type,
									"LINK" 				=> $arItem->permalink,
									"IMAGE_PREVIEW" 	=> $arItem->media_url,
									"CREATE_TIME_UNIX"	=> $arItem->timestamp
								);
								$arrCode = pathinfo($tmpMediaArray['LINK']);
								$tmpMediaArray["SHORTCODE"] = $arrCode['basename'];
								if($arParams['SHOW_MEDIA_DESCRIPTION'] == "Y"){
									$tmpMediaArray['DESCRIPTION'] 	= $arItem->caption;
								}
								if($arParams['SHOW_TYPE'] == 'WEBSITE'){
									$tmpMediaArray['IMAGE_DETAIL'] 	= $arItem->media_url;
								}
								switch($tmpMediaArray['TYPE_TOKEN']){
									case 'IMAGE':										
										$tmpMediaArray['TYPE'] = 'image';
										$this->arResult['ITEMS'][] = $tmpMediaArray;
										break;
									case 'CAROUSEL_ALBUM':
										$tmpMediaArray['TYPE'] = 'carousel';
										$sidecarObj = $this->INSTAGRAM_BASIC_API_OBJECT->getCarousel($arItem->id);					
										$tmpMediaArray['SIDECAR'] = array();
										$showSidecarItem = false;
										foreach($sidecarObj as $sidecarID){
											if($showSidecarItem){
												$tmpSidecarItem = array();
												$tmpSidecarItem["IMAGE_PREVIEW"] 	= $sidecarID->media_url;
												if($arParams['SHOW_TYPE'] == 'WEBSITE'){
													$tmpSidecarItem["IMAGE_DETAIL"] 	= $sidecarID->media_url;
												}
												$tmpMediaArray['SIDECAR'][] = $tmpSidecarItem;
											}
											$showSidecarItem = true;
										}
										$this->arResult['ITEMS'][] = $tmpMediaArray;
										break;
									case 'VIDEO':
										$tmpMediaArray['IMAGE_PREVIEW'] = $arItem->thumbnail_url;
										if($arParams['SHOW_VIDEO_MEDIAS'] == 'Y'){											
											if($arParams['SHOW_VIDEO_IMAGE_PREVIEW'] == 'N' && $arParams['PLUGIN_TYPE'] == 'FANCYBOX3'){
												$tmpMediaArray['TYPE'] = 'video';
												$tmpMediaArray['VIDEO_SOURCE'] = $arItem->media_url;
											}else{
												$tmpMediaArray['TYPE'] = 'image';
												if($arParams['SHOW_TYPE'] == 'WEBSITE'){
													$tmpMediaArray['IMAGE_DETAIL'] 	= $arItem->thumbnail_url;
												}
											}
											$this->arResult['ITEMS'][] = $tmpMediaArray;
										}
										break;
								}										
							}else{
								break;
							}
						}
					}
					unset($mediaObj);
					if(!count($this->arResult['ITEMS'])){	
						$this->errors[] = 'Cant get medias by token';
					}					
				}				
				break;
			case "HASHTAG":
				foreach($arParams['SOURCE'] as $sourceID){
					$sourceID = trim($sourceID," #");					
					if(
						$sourceID &&
						(
							($arParams['USE_BLACKLIST']=="Y" && !in_array($sourceID,$arParams['BLACKLIST'])) ||
							$arParams['USE_BLACKLIST']=="N")
						){
						$sourceID = $APPLICATION->ConvertCharset($sourceID,(LANG_CHARSET)?LANG_CHARSET:SITE_CHARSET,"UTF8");
						try{
							for($i=0;$i<10;$i++){							
								if($i && !count($mediaObjArr) && ini_get('allow_url_fopen')){
									$proxy = $this->getProxy();
									$this->proxyIsEnable = true;
									$this->INSTAGRAM_OBJECT->setProxy(array(
										'address' => $proxy['IP'],
										'port'    => $proxy['PORT'],
										'tunnel'  => false,
										'timeout' => 10
									));/**/
								}
								if(!count($mediaObjArr)){
									try{
										$mediaObjArr[] = $this->INSTAGRAM_OBJECT->getMediasByTag($sourceID,$arParams['MEDIA_COUNT']);
									}catch(Exception $e){
										//echo $e->getMessage();
									}
								}
								if($this->proxyIsEnable) $this->INSTAGRAM_OBJECT->disableProxy();
							}
						}catch(Exception $e){
							$this->arResult['ERRORS'][] = "Can't get medias for hashtag: $sourceID";	
						}
					}
				}
				if(!count($this->arResult[ITEMS])){
					foreach($mediaObjArr as $mediaObj){
						foreach($mediaObj as $arItem){					
							$tmpMediaArray = array();
							$tmpMediaArray['TYPE'] = $arItem->getType();
							$tmpMediaArray['LINK'] = $arItem->getLink();
							$tmpMediaArray['CREATE_TIME_UNIX'] 	= $arItem->getCreatedTime();
							$tmpMediaArray['SHORTCODE']			= $arItem->getShortCode();
							$tmpMediaArray['IMAGE_PREVIEW'] 	= $this->getFileArray($arItem, $arParams["MEDIA_RESOLUTION_PREVIEW"],$arParams['SOURCE_TYPE']);
							if($arParams['SHOW_MEDIA_DESCRIPTION'] == "Y"){
								$tmpMediaArray['DESCRIPTION'] 	= $arItem->getCaption();
							}
							if($arParams['SHOW_TYPE'] == 'WEBSITE'){
								$tmpMediaArray['IMAGE_DETAIL'] 	= $this->getFileArray($arItem, $arParams["MEDIA_RESOLUTION_DETAIL"], $arParams['SOURCE_TYPE']);
							}
							$this->arResult['ITEMS'][] = $tmpMediaArray;
							/*switch($tmpMediaArray['TYPE']){
								case 'image':
									$this->arResult['ITEMS'][] = $tmpMediaArray;
									break;
								case 'sidecar':							
									$sidecarObj = $this->INSTAGRAM_OBJECT->getMediaByCode($arItem->getShortCode());							
									$sidecarObj = $sidecarObj->getSidecarMedias();							
									$tmpMediaArray['SIDECAR'] = array();
									$showSidecarItem = false;
									foreach($sidecarObj as $sidecarID){
										if($showSidecarItem){
											$tmpSidecarItem = array();
											$tmpSidecarItem["IMAGE_PREVIEW"] 	= $this->getFileArray($sidecarID, $arParams["MEDIA_RESOLUTION_PREVIEW"],$arParams['SOURCE_TYPE']);
											$tmpSidecarItem["IMAGE_DETAIL"] 	= $this->getFileArray($sidecarID, $arParams["MEDIA_RESOLUTION_DETAIL"],$arParams['SOURCE_TYPE']);
											$tmpMediaArray['SIDECAR'][] = $tmpSidecarItem;
										}
										$showSidecarItem = true;
									}
									$this->arResult['ITEMS'][] = $tmpMediaArray;
									break;
								case 'video':
									if($arParams['SHOW_VIDEO_MEDIAS'] == 'Y'){
										if($arParams['SHOW_VIDEO_IMAGE_PREVIEW'] == 'N' && $arParams['PLUGIN_TYPE'] == 'FANCYBOX3'){
											$videoObj = $this->INSTAGRAM_OBJECT->getMediaByUrl($tmpMediaArray["LINK"]);
											$tmpMediaArray['VIDEO_SOURCE'] = $videoObj->getVideoStandardResolutionUrl();
											if(!$tmpMediaArray['VIDEO_SOURCE']) $tmpMediaArray['VIDEO_SOURCE'] = $videoObj->getVideoLowResolutionUrl();
										}else{
											$tmpMediaArray['TYPE'] = 'image';
										}
										$this->arResult['ITEMS'][] = $tmpMediaArray;
									}
									break;
							}/**/				
							unset($tmpMediaArray);
						}
					}
				}
				usort($this->arResult[ITEMS], function($a, $b){
					return ($b['CREATE_TIME_UNIX'] - $a['CREATE_TIME_UNIX']);
				});		
				$this->arResult['ITEMS'] = array_slice($this->arResult['ITEMS'], 0, $arParams['MEDIA_COUNT'], $preserve_keys = true);
				break;
		}			
		return $this->arResult;
	}

	function getFileArray($arItem, $MEDIA_RESOLUTION, $SOURCE_TYPE){
		if($this->needUseToken && $SOURCE_TYPE != "HASHTAG"){
			switch($MEDIA_RESOLUTION){
				case "R320":	$returnImage = $arItem->images->low_resolution->url;break;
				case "R150":	$returnImage = $arItem->images->thumbnail->url;break;
				default: 		$returnImage = $arItem->images->standard_resolution->url;break;
			}
		}else{
			$returnImage = "";
			$arrSizes = $arItem->getSquareImages();

			switch($MEDIA_RESOLUTION){
				case "R1080":	$returnImage = $arItem->getImageHighResolutionUrl();break; 
				case "R640":	$returnImage = $arItem->getImageThumbnailUrl();break;
				case "R320":	$returnImage = $arrSizes[2];break;
				case "R150":	$returnImage = $arrSizes[0];break;
				default: 		$returnImage = $arItem->getImageThumbnailUrl();
			}
			if(!$returnImage) $returnImage = $arItem->getImageHighResolutionUrl();
			if(!$returnImage) $returnImage = $arItem->getImageStandardResolutionUrl();
			if(!$returnImage) $returnImage = $arItem->getImageLowResolutionUrl();
			if(!$returnImage) $returnImage = $arItem->getImageThumbnailUrl();
		}
		return $returnImage;
	}
}
?>