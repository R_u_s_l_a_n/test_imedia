<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"MAIN" => array(
			"NAME" => GetMessage("GROUP_MAIN"),
			"SORT" => 90
		),
        "BLACKLIST" => array(
			"NAME" => GetMessage("GROUP_BLACKLIST"),
			"SORT" => 95
		),
        "SIDECAR" => array(
			"NAME" => GetMessage("GROUP_SIDECAR"),
			"SORT" => 110
		),
        "VIDEO" => array(
			"NAME" => GetMessage("GROUP_VIDEO"),
			"SORT" => 120
		),
        "GALLERY" => array(
			"NAME" => GetMessage("GROUP_GALLERY"),
			"SORT" => 130
		),
        "SETTINGS" => array(
			"NAME" => GetMessage("GROUP_SETTINGS"),
			"SORT" => 130
		),
        "ADDITIONAL" => array(
			"NAME" => GetMessage("GROUP_ADDITIONAL"),
			"SORT" => 140
		),
	),
	"PARAMETERS" => array(
		"NOTE_TOKEN" => array(
			"PARENT" => "MAIN",
			"TYPE" => "CUSTOM",
			"JS_FILE" => "/bitrix/js/main/comp_props.js",
			"JS_EVENT" => "BxShowComponentNotes",
			"JS_DATA" => GetMessage("NOTE_TOKEN"),
		),
		"NOTE_TOKEN_2" => array(
			"PARENT" => "MAIN",
			"TYPE" => "CUSTOM",
			"JS_FILE" => "/bitrix/js/main/comp_props.js",
			"JS_EVENT" => "BxShowComponentNotes",
			"JS_DATA" => GetMessage("NOTE_TOKEN_2"),
		),
        "SOURCE_TYPE" => array(
			"PARENT" => "MAIN",
			"NAME" =>  GetMessage("SOURCE_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"TOKEN" => GetMessage("SOURCE_TYPE_TOKEN"),
				"HASHTAG" => GetMessage("SOURCE_TYPE_HASHTAG"),
				),
			"DEFAULT" => "USERNAME",
		),
        "SOURCE" => array(
			"PARENT" => "MAIN",
			"NAME" => GetMessage("SOURCE"),
			"TYPE" => "TEXT",
            "MULTIPLE" => "Y",
			"DEFAULT" => array("1cbitrix"),
		),
        "USE_BLACKLIST" => array(
			"PARENT" => "BLACKLIST",
			"NAME" => GetMessage("USE_BLACKLIST"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        "BLACKLIST" => array(
			"PARENT" => "BLACKLIST",
			"NAME" => GetMessage("BLACKLIST"),
			"TYPE" => "TEXT",
            "MULTIPLE" => "Y",
		),
		"MEDIA_COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MEDIA_COUNT"),
			"TYPE" => "TEXT",
			"DEFAULT" => "9",
		),		
        "MEDIA_RESOLUTION_PREVIEW" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MEDIA_RESOLUTION_PREVIEW"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"R150" => GetMessage("MEDIA_RESOLUTION_150"),
				"R320" => GetMessage("MEDIA_RESOLUTION_320"),
				"R640" => GetMessage("MEDIA_RESOLUTION_640"),
				"R1080" => GetMessage("MEDIA_RESOLUTION_1080")
			),
			"DEFAULT" => "R150",
		),
        "MEDIA_RESOLUTION_DETAIL" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MEDIA_RESOLUTION_DETAIL"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"R150" => GetMessage("MEDIA_RESOLUTION_150"),
				"R320" => GetMessage("MEDIA_RESOLUTION_320"),
				"R640" => GetMessage("MEDIA_RESOLUTION_640"),
				"R1080" => GetMessage("MEDIA_RESOLUTION_1080")
			),
			"DEFAULT" => "R1080",
		),
        "SHOW_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" =>  GetMessage("SHOW_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => array("WEBSITE" => GetMessage("SHOW_TYPE_WEBSITE"), "INSTAGRAM" => GetMessage("SHOW_TYPE_INSTAGRAM")),
			"DEFAULT" => "WEBSITE",
		),
        "SHOW_SIDECAR_MEDIAS" => array(
			"PARENT" => "SIDECAR",
			"NAME" => GetMessage("SHOW_SIDECAR_MEDIAS"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
		"SHOW_SIDECAR_ITEMS_ON_PAGE" => array(
			"PARENT" => "SIDECAR",
			"NAME" => GetMessage("SHOW_SIDECAR_ITEMS_ON_PAGE"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        "SHOW_SIDECAR_ICON" => array(
			"PARENT" => "SIDECAR",
			"NAME" => GetMessage("SHOW_SIDECAR_ICON"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        "SHOW_VIDEO_MEDIAS" => array(
			"PARENT" => "VIDEO",
			"NAME" => GetMessage("SHOW_VIDEO_MEDIAS"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        "SHOW_VIDEO_IMAGE_PREVIEW" => array(
			"PARENT" => "VIDEO",
			"NAME" => GetMessage("SHOW_VIDEO_IMAGE_PREVIEW"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        "SHOW_VIDEO_ICON" => array(
			"PARENT" => "VIDEO",
			"NAME" => GetMessage("SHOW_VIDEO_ICON"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        /********************************************************************************/
        "ADD_JQUERY" => array(
			"PARENT" => "GALLERY",
			"NAME" =>  GetMessage("ADD_JQUERY"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        "PLUGIN_TYPE" => array(
			"PARENT" => "GALLERY",
			"NAME" =>  GetMessage("PLUGIN_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => array(
                "FANCYBOX3" => GetMessage("PLUGIN_TYPE_FANCYBOX3"),
                "MAGNIFICPOPUP" => GetMessage("PLUGIN_TYPE_MAGNIFICPOPUP"),
            ),
			"DEFAULT" => "FANCYBOX3",
		),
        "ADD_PLUGIN" => array(
			"PARENT" => "GALLERY",
			"NAME" =>  GetMessage("ADD_PLUGIN"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        /********************************************************************************/
        "SHOW_MEDIA_DESCRIPTION" => array(
			"PARENT" => "SETTINGS",
			"NAME" =>  GetMessage("SHOW_MEDIA_DESCRIPTION"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        
        /********************************************************************************/
		"NOINDEX_WIDGET" => array(
			"PARENT" => "ADDITIONAL",
			"NAME" =>  GetMessage("NOINDEX_WIDGET"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
		"NOINDEX_LINKS" => array(
			"PARENT" => "ADDITIONAL",
			"NAME" =>  GetMessage("NOINDEX_LINKS"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "Y",
		),
        "DEL_SPEC_SYMBOLS_IN_DESCRIPTION" => array(
			"PARENT" => "ADDITIONAL",
			"NAME" =>  GetMessage("DEL_SPEC_SYMBOLS_IN_DESCRIPTION"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        "SHOW_PARSING_ERRORS" => array(
			"PARENT" => "ADDITIONAL",
			"NAME" =>  GetMessage("SHOW_PARSING_ERRORS"),
			"TYPE" => "LIST",
			"VALUES" => array("Y" => GetMessage("YES"), "N" => GetMessage("NO")),
			"DEFAULT" => "N",
		),
        "CACHE_SETTINGS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"TYPE" => "CUSTOM",
			"JS_FILE" => "/bitrix/js/main/comp_props.js",
			"JS_EVENT" => "BxShowComponentNotes",
			"JS_DATA" => GetMessage("NOTE_CACHE_SETTINGS"),
		),
		"CACHE_TIME" => array(
			"DEFAULT" => 3600*12,
		),
	),
);