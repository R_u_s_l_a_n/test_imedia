<?
$MESS["GROUP_MAIN"] = "Источники медиа";
$MESS["GROUP_BLACKLIST"] = "Чёрный список (при выводе по хэштегу)";
$MESS["GROUP_SIDECAR"] = "Настройки показа слайдеров";
$MESS["GROUP_VIDEO"] = "Настройки показа видео";
$MESS["GROUP_GALLERY"] = "Настройка для галереи показываемой \"На сайте\"";
$MESS["GROUP_SETTINGS"] = "Другие настройки";
$MESS["GROUP_ADDITIONAL"] = "Дополнительные параметры";
$MESS["NOTE_TOKEN"] = '<span style="color:#f00"><div style="margin:0 0 5px;font-weight:bold;text-align:center;font-size:17px;color:#f00">ВАЖНО!</div>
Instagram может блокировать отображение, если поступает слишком много запросов. Чтобы избежать этого:
<div style="margin:0 0 5px;color:#f00">1. Получите и укажите в <a target="_blank" href="/bitrix/admin/settings.php?lang=ru&mid=zaiv.instagramgallerypro">настройках модуля</a> Long Lived Token. <a target="_blank" href="https://zaiv.ru/blog/kak-poluchit-long-lived-token-instagram/">Инструкция по получению Token</a></div>
<div style="margin:0 0 5px;color:#f00">2. Установите максимальное время кеширования в настрйоках этого компонента и избегайте очистки кеша модуля.</div>
</span>';
$MESS["NOTE_TOKEN_2"] = 'Подробнее про <a target="_blank" href="https://zaiv.ru/blog/ispolzovanie-token-instagram/">использование Token Instagram</a> в этом компоненте.</div>';
$MESS["SOURCE_TYPE"] = "Тип источника";
$MESS["SOURCE_TYPE_TOKEN"] = "Long Lived Token Instagram";
$MESS["SOURCE_TYPE_HASHTAG"] = "Хэштеги";
$MESS["SOURCE"] = "Список хэштегов";
$MESS["USE_BLACKLIST"] = "Включить \"чёрный список\"";
$MESS["BLACKLIST"] = "Чёрный список логинов пользователей";
$MESS["BLACKLIST_TIP"] = "При выборе источником Хэштегов или Местоположения позволяет исключить показ медиа с выбранных аккаунтов";
$MESS["MEDIA_COUNT"] = "Кол-во отображаемых записей";
$MESS["MEDIA_RESOLUTION_PREVIEW"] = "Размер медиа в превью";
$MESS["MEDIA_RESOLUTION_DETAIL"] = "Размер медиа в детальном просмотре";
$MESS["MEDIA_RESOLUTION_DETAIL_TIP"] = "Для типа показа \"На сайте\"";
$MESS["MEDIA_RESOLUTION_TIP"] = "Большой размер фото увеличивает время загрузки модуля. В большинстве случаев миниатюра или малый размер будут оптимальными";
$MESS["MEDIA_RESOLUTION_150"] = "Миниатюра (150x150)";
$MESS["MEDIA_RESOLUTION_320"] = "Малый (320x320)";
$MESS["MEDIA_RESOLUTION_640"] = "Стандартный (640x640)";
$MESS["MEDIA_RESOLUTION_1080"] = "Большой (1080x1080)";
$MESS["SHOW_SIDECAR_MEDIAS"] = "Показывать все фото для типа медиа \"Слайдер\"";
$MESS["SHOW_SIDECAR_ITEMS_ON_PAGE"] = "Отображать фото для типа медиа \"Слайдер\" на странице";
$MESS["SHOW_SIDECAR_ITEMS_ON_PAGE_TIP"] = "Иначе фото будут отображаться только при детальном просмотре, а в списке фото их не будет";
$MESS["SHOW_SIDECAR_ICON"] = "Показывать иконку \"Слайдер\"";
$MESS["SHOW_VIDEO_MEDIAS"] = "Показывать медиа типа \"Видео\"";
$MESS["SHOW_VIDEO_IMAGE_PREVIEW"] = "Заменить видео на картинку анонса видео";
$MESS["SHOW_VIDEO_ICON"] = "Показывать иконку \"Видео\"";
$MESS["SHOW_TYPE"] = "Тип показа";
$MESS["SHOW_TYPE_WEBSITE"] = "На сайте";
$MESS["SHOW_TYPE_INSTAGRAM"] = "Ссылка на Instagram";
$MESS["ADD_JQUERY"] = "Подключить jQuery";
$MESS["ADD_JQUERY_TIP"] = "Выберите НЕТ если jQuery уже подключён в шаблоне сайта";
$MESS["PLUGIN_TYPE"] = "Плагин галереи";
$MESS["PLUGIN_TYPE_FANCYBOX3"] = "FancyBox3";
$MESS["PLUGIN_TYPE_MAGNIFICPOPUP"] = "Magnific Popup";
$MESS["ADD_PLUGIN"] = "Подключить плагин галереи";
$MESS["ADD_PLUGIN_TIP"] = "Выберите НЕТ если выбранный плагин уже подключён в шаблоне сайта";
$MESS["SHOW_MEDIA_DESCRIPTION"] = "Показывать описания для медиа";
$MESS["NOINDEX_WIDGET"] = "Закрыть галерею от индексирования";
$MESS["NOINDEX_LINKS"] = "Закрыть ссылки от индексирования";
$MESS["DEL_SPEC_SYMBOLS_IN_DESCRIPTION"] = "Удалять знак \"?\" в описании к медиа";
$MESS["DEL_SPEC_SYMBOLS_IN_DESCRIPTION_TIP"] = "В описании аккаунта Instagram зачастую используются иконки-изображения, которые не поддерживаются в компоненте при использовании кодировок отличных от UTF8 и заменяются на знак \"?\"";
$MESS["SHOW_PARSING_ERRORS"] = "Показывать ошибки обработки";
$MESS["SHOW_PARSING_ERRORS_TIP"] = "Будет выведен список ошибок, если вы допустите опечатку в названии аккаунта (хэтега) или если аккаунт является закрытым";
$MESS["NOTE_CACHE_SETTINGS"] = "<span style=\"color:#f00\"><b>ВАЖНО!</b> Установите кеширование соразмерно планируемой публикации в инстаграм! Это позволит сократить количество запросов к инстаграмм и в ряде случаев существенно повысить производительность модуля!</span>";
$MESS["YES"] = "Да";
$MESS["NO"] = "Нет";