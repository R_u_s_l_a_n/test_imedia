<?
$MESS["zaiv.instagramgallerypro_MODULE_NAME"] = "Галерея Инстаграм (PRO)";
$MESS["zaiv.instagramgallerypro_MODULE_DESC"] = "Модуль позволяет отобразить на сайте в виде адаптивной галереи медиа по списку открытых профилей Instagram или хэштегов";
$MESS["zaiv.instagramgallerypro_PARTNER_NAME"] = "ZaiV.RU - web development studio";
$MESS["zaiv.instagramgallerypro_PARTNER_URI"] = "https://zaiv.ru";
$MESS["zaiv.instagramgallerypro_ADMIN_NOTIFY"] = "Вы установили модуль Галерея Инстаграм (PRO). Теперь вам необходимо установить в шаблон сайта или на нужную страницу код компонента и <a href=\"/bitrix/admin/settings.php?lang=ru&mid=zaiv.instagramgallerypro&mid_menu=1\">настроить модуль</a>";
$MESS["zaiv.instagramgallerypro_ADMIN_NOTIFY_LINK1"] = "Галерея Инстаграм (PRO). <a target='_blank' href='http://cdn.zaiv.ru/marketplace/instagramgallerypro-howto.png'>Инструкция по установке кода компонента</a>";
$MESS["zaiv.instagramgallerypro_ADMIN_NOTIFY_LINK2"] = "Галерея Инстаграм (PRO). <a href=\"https://zaiv.ru/blog/kak-poluchit-long-lived-token-instagram/\">Инструкция по получению Long Lived Token Instagram</a>";
$MESS["zaiv.instagramgallerypro_ADMIN_NOTIFY_LINK3"] = "Галерея Инстаграм (PRO). Подробнее про <a href=\"https://zaiv.ru/blog/ispolzovanie-token-instagram/\">использование Token Instagram</a> в модуле.";
?>