<?
$MODULE_ID = 'zaiv.instagramgallerypro';
$MESS[$MODULE_ID . "_ENABLE"] = "Модуль активен";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN"] = "Instagram Long-Lived Token";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN_EXPIRES"] = "Срок действия Long-Lived Token";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN_UPDATE_BUTTON"] = "Обновить токен";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN_UPDATE_PROCESS"] = "Обновляем Instagram Long-Lived Token";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN_UPDATE_OK"] = "Instagram Long-Lived Token успешно обновлён";
$MESS[$MODULE_ID . "_LONG_LIVED_TOKEN_UPDATE_ERR"] = "Ошибка обновления Instagram Long-Lived Token";
$MESS[$MODULE_ID . "_TOKEN_HINT"] = "Укажите Instagram Long-Lived Token, если он у вас уже есть, либо получите его в блоке ниже на этой странице.<br><a href=\"https://zaiv.ru/blog/kak-poluchit-long-lived-token-instagram/\" target=\"_blank\">Подробная инструкция по самостоятельному получению токена</a>";
$MESS[$MODULE_ID . "_TOKEN_BLOCK"] = "Получение Instagram Long-Lived Token";
$MESS[$MODULE_ID . "_GET_TOKEN_BUTTON"] = "Запустить процесс получения Instagram Token";
$MESS[$MODULE_ID . "_GET_TOKEN_INFO"] = "
	Получение Instagram Token состоит из 4 этапов:<br>
	1. Создать и настроить приложение в панели разработчика Facebook<br>
	2. Получение Access Code<br>
	3. Обмен Access Code на Short Lived Token (действует 1 час)<br>
	4. Обмен Short Lived Token на Long Lived Token";
$MESS[$MODULE_ID . "_GOTO_STEP2_BUTTON"] = "Перейти к следующему шагу";
$MESS[$MODULE_ID . "_STEP_1"] = "Шаг 1. Настройка аккаунта Facebook Developer";
$MESS[$MODULE_ID . "_STEP_1_INFO"] = "На этом этапе вам необходимо самостоятельно настроить аккаунт Facebook Developer (<a href=\"https://zaiv.ru/blog/kak-poluchit-long-lived-token-instagram/\" target=\"_blank\">Выполнить все пункты шага 1 из этой инструкции</a>)";
$MESS[$MODULE_ID . "_STEP_2"] = "Шаг 2. Получение Access Code";
$MESS[$MODULE_ID . "_STEP_2_INFO"] = "1.Если новое окно браузера не открылось автоматически, то <a href=\"#LINK#\" target=\"_blank\">нажмите по этой ссылке</a><br>2. В открывшемся окне браузера, необходимо нажать кнопку \"Авторизовать\"<br>3. После этого скопировать параметр \"code\" в строке браузера и сохранить его в поле \"Instagram Acess Code\"<br><span class=\"require\">В случае ошибки проверьте правильность заполнения полей \"Instagram Application ID\" и \"URI переадресации для OAuth\"</span>";
$MESS[$MODULE_ID . "_STEP_3"] = "Шаг 3. Получение Instagram Short-Lived Token";
$MESS[$MODULE_ID . "_STEP_4"] = "Шаг 4. Получение Instagram Long-Lived Token";

$MESS[$MODULE_ID . "_APP_ID"] = "Instagram Application ID";
$MESS[$MODULE_ID . "_APP_SECRET"] = "Instagram Application Secret";
$MESS[$MODULE_ID . "_REDIRECT_URL"] = "URI переадресации для OAuth";
$MESS[$MODULE_ID . "_STEP_1_HINT"] = "Где получить Instagram Application ID и URI переадресации <a href=\"https://zaiv.ru/blog/kak-poluchit-long-lived-token-instagram/\" target=\"_blank\">можно узнать тут (Шаг 2, пункт 1)</a>";
$MESS[$MODULE_ID . "_GET_ACCESS_CODE"] = "Получить Access Code";
$MESS[$MODULE_ID . "_INFO_SAVE_PARAM"] = "Сохраняем #PARAM#";
$MESS[$MODULE_ID . "_INFO_SAVE_PARAM_ERROR"] = "Ошибка сохранения параметра #PARAM#";
$MESS[$MODULE_ID . "_INFO_GET_PARAM"] = "Отправляем запрос в Instagram на получение #PARAM#";
$MESS[$MODULE_ID . "_INFO_GET_PARAM_ERROR"] = "Ошибка получения параметра #PARAM#";
$MESS[$MODULE_ID . "_ACCESS_CODE"] = "Instagram Access Code";
$MESS[$MODULE_ID . "_GET_SHORT_LIVED_TOKEN_BUTTON"] = "Получить Instagram Short-Lived Token";
$MESS[$MODULE_ID . "_GET_SHORT_LIVED_TOKEN"] = "Отправляем запрос на получение Instagram Short-Lived Token";
$MESS[$MODULE_ID . "_GET_SHORT_LIVED_TOKEN_OK"] = "Instagram Short-Lived Token успешно получен!";
$MESS[$MODULE_ID . "_GET_LONG_LIVED_TOKEN"] = "Отправляем запрос на получение Instagram Long-Lived Token";
$MESS[$MODULE_ID . "_GET_LONG_LIVED_TOKEN_OK"] = "Instagram Long-Lived Token успешно получен!";
$MESS[$MODULE_ID . "_GET_LONG_LIVED_TOKEN_OK_HINT"] = "Instagram Long-Lived Token действует в течении 60 дней. Ваш токен будет автоматически продлён.";



