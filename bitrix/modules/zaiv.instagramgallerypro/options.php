<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js');

$MODULE_ID = 'zaiv.instagramgallerypro';
$RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);

if($RIGHT != "W"){
	echo('Access denied to settings of '.$MODULE_ID);
}else{
	IncludeModuleLangFile(__FILE__);
	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");

	$strWarning = "";
	
	$arOptions = [
		"main" => [
			["enable",					GetMessage($MODULE_ID . "_ENABLE"), ["checkbox"]],
			["access_long_token",		GetMessage($MODULE_ID . "_LONG_LIVED_TOKEN"), ["text",50]]
		]
	];
	
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
		array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
	);
	
	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	
	if($REQUEST_METHOD == "POST" && strlen($Update . $Apply) > 0 && $RIGHT == "W" && check_bitrix_sessid()){
	
		Option::set($MODULE_ID, 'enable', ($_REQUEST['enable']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'service_report_enable', ($_REQUEST['service_report_enable']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'app_id', $_REQUEST['app_id']);
		Option::set($MODULE_ID, 'redirect_url', $_REQUEST['redirect_url']);
		Option::set($MODULE_ID, 'app_secret', $_REQUEST['app_secret']);
		Option::set($MODULE_ID, 'access_code', $_REQUEST['access_code']);

		if(
			$_REQUEST['access_long_token'] &&
			Option::get($MODULE_ID, 'access_long_token') != $_REQUEST['access_long_token']
		){
			Option::set($MODULE_ID, 'token_expires_date',date("Y-m-d H:i:s",time() + 5184000));
		}
		Option::set($MODULE_ID, 'access_long_token', $_REQUEST['access_long_token']);
	
		//ob_start();
		//require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
		//ob_end_clean();
	
		if(strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
			LocalRedirect($_REQUEST["back_url_settings"]);
		else
			LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
	}

	/******Token manipulations*****/
	if(/*1 || /**/Option::get($MODULE_ID, 'access_long_token') && !Option::get($MODULE_ID, 'token_expires_date')){
		Option::set($MODULE_ID, 'token_expires_date',date("Y-m-d H:i:s"));
	}

	if(Option::get($MODULE_ID, 'token_expires_date')){
		$expiresDate = strtotime(Option::get($MODULE_ID, 'token_expires_date'));
		$currentDate = strtotime(date("Y-m-d H:i:s"));
		if(($expiresDate - $currentDate) < 864000){
			//update token
			?><script>$(function(){
					setTimeout(function(){
						$('[name=updateToken]').click();
					}, 1500);
			});</script><?
		}
	}
	/********************************/
	?>
	<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&amp;lang=<?= LANG ?>">
		<?
		$tabControl->Begin();
		$tabControl->BeginNextTab();
		/*?>
		<tr>
			<td colspan="2">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message">
						<span class="required"><?=GetMessage($MODULE_ID . "_OLD_TOKEN_ALERT")?></span>
					</div>
				</div>
			</td>
		</tr>
		<?/**/
		foreach($arOptions['main'] as $optionArray) {
			$val = COption::GetOptionString($MODULE_ID, $optionArray[0]);
			array_splice($optionArray, 2, 0, $val);
			__AdmSettingsDrawRow($MODULE_ID, $optionArray);
		}
		?>
		<tr>
			<td width="50%" class="adm-detail-content-cell-l"><?=GetMessage($MODULE_ID."_LONG_LIVED_TOKEN_EXPIRES")?></td>
			<td width="50%" class="adm-detail-content-cell-r">
				<input type="text" readonly size="15" name="token_expires_date" maxlength="19" value="<?=Option::get($MODULE_ID, 'token_expires_date')?>">
				<input type="button" name="updateToken" value="<?=GetMessage($MODULE_ID."_LONG_LIVED_TOKEN_UPDATE_BUTTON")?>"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<div class="wait-block updateToken">
					<div class="spinner"></div>
					<div class="text"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message">
						<?=GetMessage($MODULE_ID."_TOKEN_HINT")?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" name="startGetToken" value="<?=GetMessage($MODULE_ID . "_GET_TOKEN_BUTTON")?>">
			</td>
		</tr>
	
		<tr class="heading step1">
			<td colspan="2"><?=GetMessage($MODULE_ID."_TOKEN_BLOCK")?></td>
		</tr>
		<tr class="step1">
			<td colspan="2">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message left">
						<span><?=GetMessage($MODULE_ID . "_GET_TOKEN_INFO")?></span>
					</div>
				</div>
			</td>
		</tr>
		<tr class="heading step1">
			<td colspan="2"><?=GetMessage($MODULE_ID."_STEP_1")?></td>
		</tr>
		<tr class="step1">
			<td colspan="2">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message">
						<span><?=GetMessage($MODULE_ID . "_STEP_1_INFO")?></span>
					</div>
				</div>
			</td>
		</tr>
		<tr class="step1">
			<td colspan="2" align="center">
				<input type="submit" name="gotoStep2" value="<?=GetMessage($MODULE_ID . "_GOTO_STEP2_BUTTON")?>">
			</td>
		</tr>
	
	
	
		<tr class="heading step2">
			<td colspan="2"><?=GetMessage($MODULE_ID."_STEP_2")?></td>
		</tr>
		<tr class="step2">
			<td width="50%" class="adm-detail-content-cell-l"><?=GetMessage($MODULE_ID."_APP_ID")?></td>
			<td width="50%" class="adm-detail-content-cell-r"><input type="text" size="50" maxlength="50" value="<?=Option::get($MODULE_ID, 'app_id')?>" name="app_id"></td>
		</tr>
		<tr class="step2">
			<td width="50%" class="adm-detail-content-cell-l"><?=GetMessage($MODULE_ID."_APP_SECRET")?></td>
			<td width="50%" class="adm-detail-content-cell-r"><input type="text" size="50" maxlength="50" value="<?=Option::get($MODULE_ID, 'app_secret')?>" name="app_secret"></td>
		</tr>
		<tr class="step2">
			<td width="50%" class="adm-detail-content-cell-l"><?=GetMessage($MODULE_ID."_REDIRECT_URL")?></td>
			<td width="50%" class="adm-detail-content-cell-r"><input type="text" size="50" maxlength="50" value="<?=Option::get($MODULE_ID, 'redirect_url')?>" name="redirect_url"></td>
		</tr>
		<tr class="step2">
			<td colspan="2" align="center">
				<input type="submit" name="getAccessCode" value="<?=GetMessage($MODULE_ID . "_GET_ACCESS_CODE")?>">
				<div class="wait-block s2">
					<div class="spinner"></div>
					<div class="text m-1"></div>
				</div>
			</td>
		</tr>
		<tr class="step2-2">
			<td colspan="2">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message">
						<span></span>
					</div>
				</div>
			</td>
		</tr>
		<tr class="step2-2">
			<td width="50%" class="adm-detail-content-cell-l"><?=GetMessage($MODULE_ID."_ACCESS_CODE")?></td>
			<td width="50%" class="adm-detail-content-cell-r"><input type="text" size="50" maxlength="250" value="<?=Option::get($MODULE_ID, 'access_code')?>" name="access_code"></td>
		</tr>
		<tr class="heading step3">
			<td colspan="2"><?=GetMessage($MODULE_ID."_STEP_3")?></td>
		</tr>
		<tr class="step3">
			<td colspan="2" align="center">
				<input type="submit" name="getShortLivedToken" value="<?=GetMessage($MODULE_ID . "_GET_SHORT_LIVED_TOKEN_BUTTON")?>">
				<div class="wait-block s3">
					<div class="spinner"></div>
					<div class="text m-1"></div>
				</div>
			</td>
		</tr>
		<tr class="heading step4">
			<td colspan="2"><?=GetMessage($MODULE_ID."_STEP_4")?></td>
		</tr>
		<tr class="step4">
			<td colspan="2" align="center">
				<div class="wait-block s4">
					<div class="spinner"></div>
					<div class="text m-1"></div>
				</div>
			</td>
		</tr>
		<tr class="step4-2">
			<td colspan="2">
				<div class="adm-info-message-wrap" align="center">
					<div class="adm-info-message">
						<span><?=GetMessage($MODULE_ID."_GET_LONG_LIVED_TOKEN_OK_HINT")?></span>
					</div>
				</div>
			</td>
		</tr>
	
	
		<? $tabControl->BeginNextTab(); ?>
		<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
		<? $tabControl->Buttons(); ?>
		<div align="left">
			<input type="hidden" name="Update" value="Y">
			<input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>" class="adm-btn-save"/>
		</div>
		<? $tabControl->End(); ?>
		<?= bitrix_sessid_post(); ?>
	</form>
	
	<style>
	.step1,.step2,.step2-2,.step3,.step4,.step4-2,.wait-block{display:none;}
	.left{text-align:left}
	.m-1{margin:1rem}
		.wait-block{margin-top:1rem;}
	.wait-block .spinner{
		width: 1rem;
		height: 1rem;
		border-radius: 50%;
		border: solid 5px #87ae00;
		border-top-color: #dde6bc;
		animation: 1.5s spin infinite linear;
	}
	.wait-block .text{
		color: #87ae00;
		font-size: 1.1rem;
	}
	.wait-block.err .text{
		color: #e20000;
	}
	@keyframes spin {
		to {
			transform: rotate(360deg);
		}
	}
	</style>
	<script>
		$(function(){
			$('[name=updateToken]').click(function(e){
				e.preventDefault();
				$('.wait-block.updateToken').removeClass('err');
				$('.wait-block.updateToken .spinner').show();
				$('.wait-block.updateToken').fadeIn();
				$('.wait-block.updateToken .text').text('<?=GetMessage($MODULE_ID."_LONG_LIVED_TOKEN_UPDATE_PROCESS")?>');
				setTimeout(function(){
					$.post(
						"/bitrix/admin/<?=$MODULE_ID?>_getToken.php",
						{
							ACTION: 'Y',
							TYPE: 'UPDATE',
							access_long_token: "<?=Option::get($MODULE_ID, 'access_long_token')?>"
						},
						function(e){
							var r = JSON.parse(e);
							if(r.result == 'ok'){
								$('[name=token_expires_date]').val(r.token_expires_date);
								$('[name=access_long_token]').val(r.access_long_token);
								$('.wait-block.updateToken').removeClass('err');
								$('.wait-block.updateToken .spinner').hide();
								$('.wait-block.updateToken .text').text('<?=GetMessage($MODULE_ID."_LONG_LIVED_TOKEN_UPDATE_OK")?>');
							}else{
								$('.wait-block.updateToken .spinner').hide();
								$('.wait-block.updateToken').addClass('err');
								$('.wait-block.updateToken .text').text('<?=GetMessage($MODULE_ID."_LONG_LIVED_TOKEN_UPDATE_ERR")?> (' + r.result + ')');
							}
						}
					);
				}, 1000);
			});
			$('[name=startGetToken]').click(function(e){
				e.preventDefault();
				$('.step1').fadeIn();
				$('[name=Update]').hide();
				$(this).hide();
			});
			$('[name=gotoStep2]').click(function(e){
				e.preventDefault();
				$('.step2').fadeIn();
				$(this).hide();
			});
			$('[name=getAccessCode]').click(function(e){
				e.preventDefault();
				$(this).hide();
				$('.wait-block.s2').removeClass('err');
				$('.wait-block.s2 .spinner').show();
				$('.wait-block.s2').fadeIn();
				$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Instagram Application ID",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM"))?>');
				setTimeout(function(){
					$.post(
						"/bitrix/admin/<?=$MODULE_ID?>_saveOptions.php",
						{
							UPDATE: 'Y',
							PARAM_NAME: 'app_id',
							PARAM_VALUE: $('[name=app_id]').val()
						},
						function(e){
							if(e == 'ok'){
								$('.wait-block.s2').removeClass('err');
								$('.wait-block.s2 .spinner').show();
								$('.wait-block.s2').fadeIn();
								$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Instagram Application Secret",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM"))?>');
								setTimeout(function(){
									$.post(
										"/bitrix/admin/<?=$MODULE_ID?>_saveOptions.php",
										{
											UPDATE: 'Y',
											PARAM_NAME: 'app_secret',
											PARAM_VALUE: $('[name=app_secret]').val()
										},
										function(e){
											if(e == 'ok'){
												$('.wait-block.s2').removeClass('err');
												$('.wait-block.s2 .spinner').show();
												$('.wait-block.s2').fadeIn();
												$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Redirect URL",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM"))?>');
												setTimeout(function(){
													$.post(
														"/bitrix/admin/<?=$MODULE_ID?>_saveOptions.php",
														{
															UPDATE: 'Y',
															PARAM_NAME: 'redirect_url',
															PARAM_VALUE: $('[name=redirect_url]').val()
														},
														function(e){
															if(e == 'ok'){
																var step2info = '<?=GetMessage($MODULE_ID . "_STEP_2_INFO")?>';
																var link = "https://api.instagram.com/oauth/authorize?app_id=" + $('[name=app_id]').val() + "&redirect_uri=" + $('[name=redirect_url]').val() + "&scope=user_profile,user_media&response_type=code";
																step2info = step2info.replace("#LINK#",link)
																$('.wait-block.s2').hide();
																$('.step2-2 span').html(step2info);
																$('.step2-2').fadeIn();
																$('.step3').fadeIn();
																window.open(link, '_blank');
															}else{
																$('.wait-block.s2 .spinner').hide();
																$('.wait-block.s2').addClass('err');
																$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Redirect URL",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM_ERROR"))?>');
																$('[name=getAccessCode]').show();
															}
														}
													);
												}, 500);
											}else{
												$('.wait-block.s2 .spinner').hide();
												$('.wait-block.s2').addClass('err');
												$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Instagram Application Secret",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM_ERROR"))?>');
												$('[name=getAccessCode]').show();
											}
										}
									);
								}, 500);
							}else{
								$('.wait-block.s2 .spinner').hide();
								$('.wait-block.s2').addClass('err');
								$('.wait-block.s2 .text').text('<?=str_replace("#PARAM#","Instagram Application ID",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM_ERROR"))?>');
								$('[name=getAccessCode]').show();
							}
						}
					);
				}, 500);
			});
	
			$('[name=getShortLivedToken]').click(function(e){
				e.preventDefault();
				$(this).hide();
				$('.wait-block.s3').removeClass('err');
				$('.wait-block.s3 .spinner').show();
				$('.wait-block.s3').fadeIn();
				$('.wait-block.s3 .text').text('<?=str_replace("#PARAM#","Instagram Access Code",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM"))?>');
				setTimeout(function(){
					$.post(
						"/bitrix/admin/<?=$MODULE_ID?>_saveOptions.php",
						{
							UPDATE: 'Y',
							PARAM_NAME: 'access_code',
							PARAM_VALUE: $('[name=access_code]').val()
						},
						function(e){
							if(e == 'ok'){
								$('.wait-block.s3').removeClass('err');
								$('.wait-block.s3 .spinner').show();
								$('.wait-block.s3').fadeIn();
								$('.wait-block.s3 .text').text('<?=str_replace("#PARAM#","Short-Lived Token",GetMessage($MODULE_ID . "_INFO_GET_PARAM"))?>');
	
								setTimeout(function(){
									$.post(
										"/bitrix/admin/<?=$MODULE_ID?>_getToken.php",
										{
											ACTION: 'Y',
											TYPE: 'SHORT'
										},
										function(e){
											if(e == 'ok'){
												$('.wait-block.s3').removeClass('err');
												$('.wait-block.s3 .spinner').hide();
												$('.wait-block.s3').fadeIn();
												$('.wait-block.s3 .text').text('<?=GetMessage($MODULE_ID . "_GET_SHORT_LIVED_TOKEN_OK")?>');
	
												$('.step4').fadeIn();
												$('.wait-block.s4').fadeIn();
												$('.wait-block.s4 .text').text('<?=str_replace("#PARAM#","Long-Lived Token",GetMessage($MODULE_ID . "_INFO_GET_PARAM"))?>');
												setTimeout(function(){
													$.post(
														"/bitrix/admin/<?=$MODULE_ID?>_getToken.php",
														{
															ACTION: 'Y',
															TYPE: 'LONG'
														},
														function(e){
															if(e == 'ok'){
																$('.wait-block.s4').removeClass('err');
																$('.wait-block.s4 .spinner').hide();
																$('.wait-block.s4 .text').text('<?=GetMessage($MODULE_ID . "_GET_LONG_LIVED_TOKEN_OK")?>');
																$('.step4-2').fadeIn();
															}else{
																$('.wait-block.s4 .spinner').hide();
																$('.wait-block.s4').addClass('err');
																$('.wait-block.s4 .text').text('<?=str_replace("#PARAM#","Long-Lived Token",GetMessage($MODULE_ID . "_INFO_GET_PARAM_ERROR"))?> (' + e + ')');
															}
														}
													);
												}, 500);
	
											}else{
												$('.wait-block.s3 .spinner').hide();
												$('.wait-block.s3').addClass('err');
												$('.wait-block.s3 .text').text('<?=str_replace("#PARAM#","Short-Lived Token",GetMessage($MODULE_ID . "_INFO_GET_PARAM_ERROR"))?> (' + e + ')');
												$('[name=getShortLivedToken]').show();
											}
										}
									);
								}, 500);
							}else{
								$('.wait-block.s3 .spinner').hide();
								$('.wait-block.s3').addClass('err');
								$('.wait-block.s3 .text').text('<?=str_replace("#PARAM#","Instagram Access Code",GetMessage($MODULE_ID . "_INFO_SAVE_PARAM_ERROR"))?>');
								$('[name=getShortLivedToken]').show();
							}
						}
					);
				}, 500);
			});
		});
	</script>
<?}?>