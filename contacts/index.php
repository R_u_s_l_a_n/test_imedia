<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/header-inner1.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product');
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."include/contacts_page/contacts_page.php"
	),
false,
Array(
	'HIDE_ICONS' => 'Y'
)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>