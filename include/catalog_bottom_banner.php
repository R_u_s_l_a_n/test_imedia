<section class="bonus bonus__inner">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-between">
			
			<div class="bonus__info d-flex flex-column">
				<div class="main-title">
					<?  $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/catalog_bottom_banner_title.php"), false); ?>
				</div>
			</div>
			
			<div class="bonus__desc">
				<?  $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/catalog_bottom_banner_desc.php"), false); ?>
				
			</div>
		
		</div>
	</div>
</section>