<section class="contacts-page">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-3 contacts-page__item">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts_page/contacts_blok_1.php"), false); ?>
				
			</div>
			<div class="col-12 col-sm-6 col-md-3 contacts-page__item">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts_page/contacts_blok_2.php"), false); ?>
				
			</div>
			<div class="col-12 col-sm-6 col-md-3 contacts-page__item">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts_page/contacts_blok_3.php"), false); ?>
				
			</div>
			<div class="col-12 col-sm-6 col-md-3 contacts-page__item">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contacts_page/contacts_blok_4.php"), false); ?>
				
			</div>
		</div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:form.result.new", 
            "contacts_form", 
            array(
            	"CACHE_TIME" => "360000",
            	"CACHE_TYPE" => "Y",
            	"CHAIN_ITEM_LINK" => "",
            	"CHAIN_ITEM_TEXT" => "",
            	"COMPONENT_TEMPLATE" => "contacts_form",
            	"EDIT_URL" => "",
            	"IGNORE_CUSTOM_TEMPLATE" => "N",
            	"LIST_URL" => "",
            	"SEF_MODE" => "N",
            	"SUCCESS_URL" => "",
            	"USE_EXTENDED_ERRORS" => "Y",
            	"WEB_FORM_ID" => "1",
            	"VARIABLE_ALIASES" => array(
            		"WEB_FORM_ID" => "WEB_FORM_ID",
            		"RESULT_ID" => "RESULT_ID",
            	)
            ),
        false
        );?>
		
	</div>
	</div>
</section>