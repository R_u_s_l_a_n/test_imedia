<section class="dispatch-page">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex flex-wrap">
				<?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "", 
                    array("AREA_FILE_SHOW" => "file", 
                    "PATH" => SITE_DIR . "include/sale_info.php"),
                    false,
                    array("HIDE_ICONS" => "Y")
                );?>
			</div>
			<div class="col-12 d-flex flex-column dispatch-page__text">
				<div class="dispatch-page__title">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/dispatch_page/dispatch_title.php"), false); ?>
				</div>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/dispatch_page/dispatch_text.php"), false); ?>
				

			</div>
		</div>
	</div>
</section>