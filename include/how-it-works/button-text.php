<section class="howItWork-text">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
                <?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	"",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => SITE_DIR."include/how-it-works/left-text-block.php"
                	)
                );?>
			</div>
			<div class="col-12 col-md-6">
                <?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	"",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => SITE_DIR."include/how-it-works/right-text-block.php"
                	)
                );?>
				
			</div>
		</div>
	</div>
</section>