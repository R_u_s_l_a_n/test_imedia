<section class="howItWork-block">
	<div class="container">
		<div class="row">
			<div class="howItWork-block__item one">
				<div class="howItWork-block__img">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/one_block_img.php"
                    	)
                    );?>
				</div>
				<div class="howItWork-block__text">
                     <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/one_block_text.php"
                    	)
                    );?>
				</div>
			</div>
			<div class="howItWork-block__item two">
				<div class="howItWork-block__img">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/two_block_img.php"
                    	)
                    );?>
				</div>
				<div class="howItWork-block__text">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/two_block_text.php"
                    	)
                    );?>
				</div>
			</div>

			<div class="howItWork-block__item three">
				<div class="howItWork-block__img">
					
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/three_block_img.php"
                    	)
                    );?>
				</div>
				<div class="howItWork-block__text">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/three_block_text.php"
                    	)
                    );?>
					
				</div>
			</div>
			<div class="howItWork-block__about">
				<div class="main-title">О сервисе</div>
				<a  data-fancybox data-src="#request-popup" class="btn">Предложить сотрудничество</a>
			</div>
			<div class="howItWork-block__item four">
				<div class="howItWork-block__img">
					
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/four_block_img.php"
                    	)
                    );?>
				</div>
				<div class="howItWork-block__text">
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	"",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"PATH" => SITE_DIR."include/how-it-works/four_block_text.php"
                    	)
                    );?>
					
				</div>
			</div>

			<div class="howItWork-block__bg"></div>

		</div>
	</div>
</section>
