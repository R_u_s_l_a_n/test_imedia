<section class="bonus bonus--main">
	<div class="container-fluid">
		<div class="row d-flex align-items-center justify-content-between">

				<div class="bonus__info d-flex flex-column">
					<div class="main-title">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main_page/sale_block_title.php"), false); ?>
					</div>
					<div class="main-description">
                     <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main_page/sale_block_description.php"), false); ?>
                     </div>
				</div>

			<div class="bonus__desc">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main_page/sale_block_info.php"), false); ?>
			</div>
			<div class=" bonus__img">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/main_page/sale_block_img.php"), false); ?>
				
			</div>
		</div>
	</div>
</section>