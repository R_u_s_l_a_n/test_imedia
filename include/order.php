<?
use Bitrix\Sale;
use Bitrix\Main\Loader;

if(!$_SESSION['DELIVERY']['ID']){
    header('Location: /cart/');
}

Loader::includeModule('sale');
Loader::includeModule('main');

function getUserBasket(){
    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
    $context = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId());
    $discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $context);
    if($discounts){
        $r = $discounts->calculate();
        $result = $r->getData();
        if (isset($result['BASKET_ITEMS']))
        {
            $r = $basket->applyDiscount($result['BASKET_ITEMS']);
        }
    }

    return $basket;
}

function getUserType(){
    $type = 1;
    global $USER;
    if ($USER->IsAuthorized()){
        $arGroups = CUser::GetUserGroup($USER->GetID());
        if(in_array('5', $arGroups)){
            $type = 2;
        }
    }
    return $type;
}

function getUserInfo(){
    $arUserInfo = [];
    global $USER;
    if ($USER->IsAuthorized()){
        $order = array('sort' => 'asc');
        $tmp = 'sort';
        $arUserInfo = CUser::GetList($order, $tmp, ['ID' => $USER->GetID()], ['SELECT' => ['NAME', 'UF_*', 'PERSONAL_PHONE', 'EMAIL']]) -> fetch();
    }
    return $arUserInfo;
}

function getOrderProps($userType){
    $arProps = [];
    if($userType){
       $db_props = CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            array(
                    "PERSON_TYPE_ID" => $userType,
                    "ACTIVE" => 'Y',
                ),
            false,
            false,
            array()
        );

        while($props = $db_props->Fetch())
        {
            $arProps[$props['PROPS_GROUP_ID']][] = $props;
        }
    }
    return $arProps;
}

function getPaySystem($userType){
    $arPaySystem = [];
    $db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), Array("LID"=>SITE_ID, "ACTIVE"=>"Y", "PERSON_TYPE_ID"=>$userType, ));
    while ($ptype = $db_ptype->Fetch())
    {

        $dbRestriction = \Bitrix\Sale\Internals\ServiceRestrictionTable::getList(array(
            'select' => array('PARAMS'),
            'filter' => array(
                'SERVICE_ID' => $ptype['ID'],
                //'CLASS_NAME' => $helper->forSql('\Bitrix\Sale\Services\PaySystem\Restrictions\PersonType'),
                'SERVICE_TYPE' => \Bitrix\Sale\Services\PaySystem\Restrictions\Manager::SERVICE_TYPE_PAYMENT
            )
        ));
        $restriction = \Bitrix\Sale\Services\PaySystem\Restrictions\Delivery::prepareParamsValues(array(),$ptype['ID']);
        $ptype['DELIVERY'] = $restriction['DELIVERY'];
        $arPaySystem[] = $ptype;
    }
    return $arPaySystem;
}

$userBasket = getUserBasket();
if(count($userBasket) <= 0){
    header('Location: /cart/');
}
$userType = getUserType();
$OrderProps = getOrderProps($userType);
$paySystem = getPaySystem($userType);

if(count($OrderProps)==1){
	$OrderProps = array_shift($OrderProps);
	$quantityProps = count($OrderProps);
	$arrProps = array_chunk($OrderProps, round($quantityProps / 2));
}else{
	$arrProps = $OrderProps;
}


$arUserInfo = getUserInfo();
//pr($arUserInfo);

?>
<section class="order-block">

	<div class="container">
    <div class="err order_err"></div>
    <form id="order_form">
		<div class="row order-block__items">

            <?if($arrProps):?>
                <?foreach($arrProps as $propsGroup):?>
    			<div class="col-12 col-md-6 col-lg-4">
                    <?if($propsGroup):?>
                        <?foreach($propsGroup as $propInfo):?>
                            <?
                            $propValue = '';
		                    $dopClass = '';
                            $propType = 'text';
                            if($arUserInfo['UF_'.$propInfo['CODE']])
	                            $propValue = $arUserInfo['UF_'.$propInfo['CODE']];
                            switch ($propInfo['CODE']) {
                                case 'NAME':
                                    $propValue =  $arUserInfo['NAME'];
                                    break;
                                case 'PHONE':
                                    $propValue =  $arUserInfo['PERSONAL_PHONE'];
                                    $propType = 'tel';
                                    break;
                                case 'EMAIL':
                                    $propValue =  $arUserInfo['EMAIL'];
                                    $propType = 'email';
                                    break;
	                            case 'CITY':
	                            case 'STREET':
		                            $dopClass =  'full-width';
		                            break;
                            }
                            ?>
                            <?if($propInfo['CODE']=='CITY'):?>
                            <div class="form__inner">
                             <?endif;?>
            				<div class="form__item <?=$dopClass?>">
            					<div class="title"><?=$propInfo['NAME']?></div>
            					<input id="<?=$propInfo['CODE']?>" name="<?=$propInfo['CODE']?>" type="<?=$propType?>"  placeholder="<?=$propInfo['NAME']?>" value="<?=$propValue?>" required >
            				</div>
	                        <?if($propInfo['CODE']=='FLAT'):?>
                                </div>
		                    <?endif;?>
                            <?if($propInfo['CODE']=='NAME'):?>
                                <div class="form__item">
                                    <div class="title">Комментарий</div>
                                    <textarea name="COMMENT" class="form-input"></textarea>
                                </div>
                            <?endif;?>
                        <?endforeach;?>
                    <?endif;?>
    			</div>
                <?endforeach;?>
            <?endif;?>
			<div class="col-12 col-md-6 col-lg-4">
				<div class="order-block__final">
					<div class="product-title">Товар</div>
                    <?foreach($userBasket as $basketItem):?>
                        <?
                            $basketPropertyCollection = $basketItem->getPropertyCollection();
                            $arProp = $basketPropertyCollection->getPropertyValues();
                        ?>
					<div class="order-block__item">
						<div class="order-block__name"><b><?=$basketItem->getField('NAME');?><?if($arProp['MANUFACTORY_NAME']['VALUE']):?>,<?endif;?></b> <?=$arProp['MANUFACTORY_NAME']['VALUE']?></div>
						<div class="order-block__price"><?=$basketItem->getFinalPrice();?> <span>руб</span></div>
						<div class="order-block__weight">
                            <?if($basketItem->getWeight() >= 1000):?>
                                <?=$basketItem->getWeight()/1000;?> кг
                            <?else:?>
                                <?=(int)$basketItem->getWeight();?> г
                            <?endif;?>

                        </div>
					</div>
                    <?endforeach;?>
					<div class="order-block__price">
						<span>Итого </span> <?=$userBasket->getPrice() + $_SESSION['DELIVERY']['PRICE'];?> <span> руб </span>
					</div>
					<div class="product-title">
						Оплата
					</div>
                    <?
                    $PaySystemNumer = 0;
                    if($paySystem):?>
                        <?foreach($paySystem as $key => $payInfo):?>
                            <?if(in_array($_SESSION['DELIVERY']['ID'], $payInfo['DELIVERY'])):?>
        					<div class="form__item form__item--check">
        						<label class="checkbox"><?=$payInfo['NAME']?>
        							<input type="radio" <?if($PaySystemNumer == 0):?>checked<?endif;?> name="payment" value="<?=$payInfo['ID']?>">
        							<span class="checkmark"></span>
        						</label>
        					</div>
                            <?$PaySystemNumer++;
                            endif;?>
                        <?endforeach;?>
                    <?endif;?>
                    <input type="hidden" name="delivery" value="<?=$_SESSION['DELIVERY']['ID']?>" />
                    <input type="submit" value="Подтвердить заказ" class="btn" />
					<div id="show_pupup_btn"   data-fancybox data-src="#order-popup" ></div>
				</div>
			</div>
		</div>
       </form>
	</div>
</section>
<script>
$( "#order_form" ).submit(function( event ) {
  event.preventDefault();
  $('.payment_btn').html('');
  $('.order_ok_btn').hide();
  $.ajax({
        url:     "/local/ajax/save_order.php",
        type:     "POST",
        dataType: "json",
        data: $("#order_form").serialize(),
        success: function(result) {
            if(result.STATUS == 'OK'){
                var $form = $('#order_form');
                $('#show_pupup_btn').click();

                $form.find('input').each(function () {
                	$(this).val('');
                });

                if(result.PAYMENT_ID == '2'){
                    $.get('/order/pay.php?ORDER_ID='+result.ORDER_ID, function(data) {
                        $('.payment_btn').html(data);
                    });

                }
                else{
                    $('.order_ok_btn').show();
                }
            }
            else{
                $('.order_err').html(result.ERR_TEXT);
            }
        },
        error: function(result) {
           $('.order_err').html('Ошибка');
        }
    });
});
</script>
