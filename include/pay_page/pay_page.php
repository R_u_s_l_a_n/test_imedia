<section class="pay-page">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 pay-page__item">
				<div class="dispatch-page__title">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/left_block_title.php"), false); ?>
				</div>
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/left_block_text.php"), false); ?>
				
				<div class="pay-page__item-bg left">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/left_block_img.php"), false); ?>
					
				</div>
			</div>
			<div class="col-lg-6 pay-page__item pay-page__item--second">
				<div class="dispatch-page__title text-right">
                    <?  $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/right_block_title.php"), false); ?>
				</div>
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/right_block_text.php"), false); ?>
				<div class="pay-page__item-bg">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/pay_page/right_block_img.php"), false); ?>
					
				</div>
			</div>
		</div>
	</div>
</section>