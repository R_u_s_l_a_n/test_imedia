<section class="favorite-page history-page">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex flex-wrap">
				<?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	"",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"PATH" => SITE_DIR."include/sale_info.php"
                	),
                false,
                Array(
                	'HIDE_ICONS' => 'Y'
                )
                );?>
			</div>
            <?$_REQUEST['filter_history'] = "";
                $_REQUEST["by"]= $_REQUEST["field"]? $_REQUEST["field"] : '';
                $_REQUEST["order"]= $_REQUEST["sort"]? $_REQUEST["sort"] : '';
            ?>
            
            <?$APPLICATION->IncludeComponent(
            	"bitrix:sale.personal.order.list", 
            	"fru", 
            	array(
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "3600",
            		"CACHE_TYPE" => "A",
            		"DEFAULT_SORT" => "ID",
            		"DISALLOW_CANCEL" => "Y",
            		"HISTORIC_STATUSES" => array(
            			0 => "F",
                        1 => "FO",
            		),
            		"ID" => $ID,
            		"NAV_TEMPLATE" => "fru_order",
            		"ORDERS_PER_PAGE" => "6",
            		"PATH_TO_BASKET" => SITE_DIR."cart/",
            		"PATH_TO_CANCEL" => "",
            		"PATH_TO_CATALOG" => "/catalog/",
            		"PATH_TO_COPY" => "",
            		"PATH_TO_DETAIL" => "",
            		"PATH_TO_PAYMENT" => "payment.php",
            		"REFRESH_PRICES" => "N",
            		"RESTRICT_CHANGE_PAYSYSTEM" => array(
            			0 => "0",
            		),
            		"SAVE_IN_SESSION" => "Y",
            		"SET_TITLE" => "Y",
            		"STATUS_COLOR_F" => "gray",
            		"STATUS_COLOR_N" => "green",
            		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
            		"COMPONENT_TEMPLATE" => "fru",
            		"STATUS_COLOR_AP" => "gray",
            		"STATUS_COLOR_FO" => "gray",
            		"STATUS_COLOR_OP" => "gray",
            		"STATUS_COLOR_P" => "yellow",
            		"STATUS_COLOR_R" => "gray",
            		"STATUS_COLOR_RS" => "gray",
            		"STATUS_COLOR_S" => "gray"
            	),
            	false
            );?>
            <?$_REQUEST['filter_history']='Y';
            $_REQUEST["by"]= $_REQUEST["history_field"]? $_REQUEST["history_field"] : '';
                $_REQUEST["order"]= $_REQUEST["history_sort"]? $_REQUEST["history_sort"] : '';
            ?>
            <?$APPLICATION->IncludeComponent(
            	"bitrix:sale.personal.order.list", 
            	"history", 
            	array(
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "3600",
            		"CACHE_TYPE" => "A",
            		"DEFAULT_SORT" => "ID",
            		"DISALLOW_CANCEL" => "Y",
            		"HISTORIC_STATUSES" => array(
            			0 => "F",
                        1 => "FO",
            		),
            		"ID" => $ID,
            		"NAV_TEMPLATE" => "fru_order_history",
            		"ORDERS_PER_PAGE" => "6",
            		"PATH_TO_BASKET" => SITE_DIR."cart/",
            		"PATH_TO_CANCEL" => "",
            		"PATH_TO_CATALOG" => "/catalog/",
            		"PATH_TO_COPY" => SITE_DIR."cart/",
            		"PATH_TO_DETAIL" => "",
            		"PATH_TO_PAYMENT" => "payment.php",
            		"REFRESH_PRICES" => "N",
            		"RESTRICT_CHANGE_PAYSYSTEM" => array(
            			0 => "0",
            		),
            		"SAVE_IN_SESSION" => "Y",
            		"SET_TITLE" => "Y",
            		"STATUS_COLOR_F" => "gray",
            		"STATUS_COLOR_N" => "green",
            		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
            		"COMPONENT_TEMPLATE" => "fru",
            		"STATUS_COLOR_AP" => "gray",
            		"STATUS_COLOR_FO" => "gray",
            		"STATUS_COLOR_OP" => "gray",
            		"STATUS_COLOR_P" => "yellow",
            		"STATUS_COLOR_R" => "gray",
            		"STATUS_COLOR_RS" => "gray",
            		"STATUS_COLOR_S" => "gray"
            	),
            	false
            );?>
        </div>
	</div>
</section>