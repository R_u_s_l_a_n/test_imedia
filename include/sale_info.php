<?
use \Bitrix\Main\Loader;
Loader::includeModule('sale');
Loader::includeModule('main');

global $USER;
if ((!CSite::InGroup (array(5))) && ($USER->IsAuthorized())):
    

$calculator = new \Bitrix\Sale\Discount\CumulativeCalculator(
         $USER->GetID(),SITE_ID);$sumConfiguration = array (
    'type_sum_period'=> 'relative',
    'sum_period_data' => array(
        'period_value' => 1,
        'period_type' => 'Y'
    ));
$calculator->setSumConfiguration($sumConfiguration);
$lastOrdersSum = $calculator->calculate();


require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/handlers/discountpreset/simpleproduct.php");
$discountIterator = Bitrix\Sale\Internals\DiscountTable::getList(array(
    'select' => array(
        "*"
    ),
    'filter' => array(
        'XML_ID' => 'user_sale_%',
    ),
    'order' => array(
        "PRIORITY" => "DESC",
        "SORT" => "ASC",
        "ID" => "ASC"
    )
));
$discountProcent = 0;
while ($discount = $discountIterator->fetch()) {
    foreach($discount['CONDITIONS_LIST']['CHILDREN'] as $condition){
       switch ($condition['DATA']['logic']) {
            case 'Equal':
                if($condition['DATA']['Value']!= $lastOrdersSum){
                    continue 3;
                }
                break;
            case 'Not':
                if($condition['DATA']['Value'] == $lastOrdersSum){
                    continue 3;
                }
                break;
            case 'Great':
                if($condition['DATA']['Value'] >= $lastOrdersSum){
                    continue 3;
                }
                break;
             case 'Less':
                if($condition['DATA']['Value'] <= $lastOrdersSum){
                    continue 3;
                }
                break;
             case 'EqGr':
                if($condition['DATA']['Value'] > $lastOrdersSum){
                    continue 3;
                }
                break;
             case 'EqLs':
                if($condition['DATA']['Value'] < $lastOrdersSum){
                    continue 3;
                }
                break;
        }
    }
   
    $discountObj = new Sale\Handlers\DiscountPreset\SimpleProduct();
    $discountInfo =  $discountObj->generateState($discount);    
    if(($discountProcent < $discountInfo['discount_value']) && ($discountInfo['discount_type'] == 'Perc')){
        $discountProcent = $discountInfo['discount_value'];
    }
    
}
    

?>
<div class="favorite-page__title">
	Общая сумма Ваших заказов: <span><?=CurrencyFormat($lastOrdersSum, "BYN");?></span>
</div>
<div class="favorite-page__title">
	Ваша скидка: <span><?=(int)$discountProcent?>%</span>
</div>
<?endif;?>