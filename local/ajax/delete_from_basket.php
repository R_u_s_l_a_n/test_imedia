<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Sale;
use Bitrix\Main\Loader;
$request = Bitrix\Main\Context::getCurrent()->getRequest();
$prodId = intval($request->get('id'));
if($prodId && Loader::includeModule('sale')){
	$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
	$arItems = [];
	foreach ($basket as $basketItem) {
		if($basketItem->getProductId()==$prodId){
			$basketItem->delete();
		}
	}
	$basket->save();
	echo json_encode(['STATUS'=>'OK','REMOVED'=>'OK', 'PRICE' => $basket->getPrice()]);
}


