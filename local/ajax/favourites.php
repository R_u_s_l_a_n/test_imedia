<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("main");
$request = Bitrix\Main\Context::getCurrent()->getRequest();
$prod_id = $request->get('id');
$action = $request->get('action');
if(intval($prod_id)){
	if($USER->IsAuthorized()){
		$favourites = $USER->GetList(($by="personal_country"), ($order="desc"), array('ID'=>$USER->GetID()),array('SELECT'=>array('UF_FAVOURITES'),'FIELDS'=>array('ID')))->GetNext();
		
		if(!$favourites['UF_FAVOURITES'])
			$favourites['UF_FAVOURITES'] = array();
		
		if($action=='add' && !in_array($prod_id,$favourites['UF_FAVOURITES']))
			$favourites['UF_FAVOURITES'][] = $prod_id;
		if($action=='delete' && in_array($prod_id,$favourites['UF_FAVOURITES']))
			unset($favourites['UF_FAVOURITES'][array_search($prod_id,$favourites['UF_FAVOURITES'])]);
            $user = new CUser;
		    $res = $user->update($USER->GetID(),array('UF_FAVOURITES'=>$favourites['UF_FAVOURITES']));
            //pr($user->LAST_ERROR);
            $arResult['status'] = 'ok';
            
	}else{
		/*$favourites = unserialize($APPLICATION->get_cookie('FAVOURITES'));
		if($action=='add' && !in_array($prod_id,$favourites))
			$favourites[] = $prod_id;
		if($action=='delete' && in_array($prod_id,$favourites))
			unset($favourites[array_search($prod_id,$favourites)]);
		$APPLICATION->set_cookie('FAVOURITES',serialize($favourites));*/
        $arResult['status'] = 'registration';
	}
	
	echo json_encode($arResult);
}
$application = \Bitrix\Main\Application::getInstance();
$context = $application->getContext();
$context->getResponse()->flush("");