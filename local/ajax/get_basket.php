<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Sale;
use Bitrix\Main\Loader;
Loader::includeModule('sale');
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$arItems = [];
foreach ($basket as $basketItem) {
	$arItems[] = $basketItem->getProductId();
}

echo json_encode($arItems);