<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$favourites = [];
if($USER->IsAuthorized()){
	$favouritesUser = $USER->GetList(($by="personal_country"), ($order="desc"), array('ID'=>$USER->GetID()),array('SELECT'=>array('UF_FAVOURITES'),'FIELDS'=>array('ID')))->GetNext();
	
	if($favouritesUser['UF_FAVOURITES'])
		$favourites = $favouritesUser['UF_FAVOURITES'];
}
else
	$favourites = unserialize($APPLICATION->get_cookie('FAVOURITES'));
echo json_encode($favourites);