<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Sale;
use Bitrix\Main\Loader;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Context;
use Bitrix\Sale\PaySystem;

global $USER;

Loader::includeModule('sale');
Loader::includeModule('catalog');

$request = Context::getCurrent()->getRequest();
$deliveryId = $request['delivery'];
$paySystemId = $request['payment'];
$userId = $USER->isAuthorized() ? $USER->GetID() : 9;

$siteId = Context::getCurrent()->getSite();
$currencyCode = CurrencyManager::getBaseCurrency();
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteId);


if($USER->isAuthorized()){
    $arUserInfo = [];
    global $USER;
    if ($USER->IsAuthorized()){
        $order = array('sort' => 'asc');
        $tmp = 'sort';
        $arUserInfo = CUser::GetList($order, $tmp, ['ID' => $USER->GetID()], ['SELECT' => ['NAME', 'UF_ADDRESS', 'PERSONAL_PHONE', 'EMAIL']]) -> fetch();
        
        $arUserForUpdate = [];
        
        foreach($request as $requestCode => $requestValue){
           switch ($requestCode) {
                case 'NAME':
                    if($arUserInfo['NAME'] != $requestValue){
                        $arUserForUpdate['NAME'] = $requestValue;
                    }
                    break;
                case 'PHONE':
                    if($arUserInfo['PERSONAL_PHONE'] != $requestValue){
                        $arUserForUpdate['PERSONAL_PHONE'] = $requestValue;
                    }
                    break;
                case 'EMAIL':
                    if($arUserInfo['EMAIL'] != $requestValue){
                        $arUserForUpdate['EMAIL'] = $requestValue;
                    }
                    break;
                case 'ADDRESS':
                    if($arUserInfo['UF_ADDRESS'] != $requestValue){
                        $arUserForUpdate['UF_ADDRESS'] = $requestValue;
                    }
                    break;
            } 
        }
        
        if($arUserForUpdate){
            $user = new CUser;
            $user->Update($USER->GetID(), $arUserForUpdate);
        }
        
    }
}

if(count($basket)<=0){
   $arRes['STATUS'] = 'ERR';
   $arRes['ERR_TEXT'] = "Ваша корзина пуста";
}
if(!$paySystemId){
   $arRes['STATUS'] = 'ERR';
   $arRes['ERR_TEXT'] = "Выбирете способ оплаты";
}
if(!$deliveryId){
   $arRes['STATUS'] = 'ERR';
   $arRes['ERR_TEXT'] = "Выбирете доставку";
}
if(!$arRes){
    $order = \Bitrix\Sale\Order::create($siteId,$userId,$currency);
    $order->setPersonTypeId(1);
    $order->setBasket($basket);
    if($request['COMMENT'])
		$order->setField('USER_DESCRIPTION',$request['COMMENT']);
    $shipmentCollection = $order->getShipmentCollection();
    $shipment = $shipmentCollection->createItem();
    $service = Bitrix\Sale\Delivery\Services\Manager::getById($deliveryId);
    $delivery = $service['NAME'];
    $shipment->setFields(array(
        'DELIVERY_ID' => $service['ID'],
        'DELIVERY_NAME' => $service['NAME'],
    ));
    $shipmentItemCollection = $shipment->getShipmentItemCollection();
    foreach ($basket as $item)
    {
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());
    }
    
    $paymentCollection = $order->getPaymentCollection();
    $payment = $paymentCollection->createItem();
    $paySystemService = PaySystem\Manager::getObjectById($paySystemId);
    $payment->setFields(array(
        'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
        'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
    ));
    
    $propertyCollection = $order->getPropertyCollection();
    foreach ($propertyCollection->getGroups() as $group)
    {
    	foreach ($propertyCollection->getGroupProperties($group['ID']) as $property)
    	{
            $p = $property->getProperty();
            if($request[$p["CODE"]]){
                $property->setValue($request[$p["CODE"]]);
            }
                
    	}
    }
    
    
    
    $order->doFinalAction(true);
    $result = $order->save();
    if($result->isSuccess())
    {
        $orderId = $order->getId();
        $arRes['STATUS'] = 'OK';
        $arRes['ORDER_ID'] = $orderId;
        $arRes['PAYMENT_ID'] =$payment->getPaymentSystemId();
        
    }
    else
    {
       $arRes['STATUS'] = 'ERR';
       $arRes['ERR_TEXT'] = "Ошибка создания заказа: ".implode(", ",$result->getErrorMessages());
        
    }
}


echo json_encode($arRes);
