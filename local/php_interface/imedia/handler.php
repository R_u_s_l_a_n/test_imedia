<?
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
AddEventHandler("sale", "OnBeforeBasketAdd", "OnBeforeBasketAddHandler");



function OnBeforeBasketAddHandler(&$arFields){
    $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_MANUFACTORY.NAME");
    $arFilter = Array("IBLOCK_ID"=>$arFields['IBLOCK_ID'], "ID"=>$arFields['PRODUCT_ID']);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($arRes = $res->Fetch()){ 
        if($arRes['PROPERTY_MANUFACTORY_NAME']){
            $arFields['PROPS']['MANUFACTORY_NAME'] = array(
                 'CODE' => 'MANUFACTORY_NAME',
                 'VALUE' => $arRes['PROPERTY_MANUFACTORY_NAME'],
                 'NAME' => 'Производитель'
             );
        }
        
    }  
}


function OnBeforeUserRegisterHandler(&$arFields)
  {
    
    if(($arFields["PERSONAL_PHONE"]) && ($arFields["EMAIL"])){
       
        $arFields["LOGIN"] = $arFields["EMAIL"];
        
        $arFilter = Array(
          'PERSONAL_PHONE' => $arFields["PERSONAL_PHONE"],
        );
        $userPhone = CUser::GetList(($by="ID"), ($order="ASC"),$arFilter, array('ID', 'PERSONAL_PHONE', 'EMAIL'))->fetch();
        $arFilter = Array(
          'EMAIL' => $arFields["EMAIL"],
        );
        $userEmail = CUser::GetList(($by="ID"), ($order="ASC"),$arFilter, array('ID', 'PERSONAL_PHONE', 'EMAIL'))->fetch();
        if($userPhone){
              $arFields['ERROR']['PERSONAL_PHONE'] = 'Данный номер уже зарегистрирован';
        }
        if($userEmail){
            $arFields['ERROR']['EMAIL'] = 'Данный адрес e-mail уже зарегистрирован';
        }
        if(strlen($arFields["PASSWORD"]) < 6){
            $arFields['ERROR']['PASSWORD'] = 'Пароль должен быть длиной не менее 6 символов';
        }
        else{
            if($arFields["PASSWORD"] !== $arFields["CONFIRM_PASSWORD"]){
                $arFields['ERROR']['CONFIRM_PASSWORD'] = 'Неверное подтверждение пароля.';
            }
        }
        
        if($_REQUEST['COMPANY']){
            if(!$arFields['UF_YNP']){
                $arFields['ERROR']['UF_YNP'] = 'Введите УПН';
            }
            if(!$arFields['WORK_COMPANY']){
                $arFields['ERROR']['WORK_COMPANY'] = 'Введите Название компании';
            }
            $arFields["GROUP_ID"][] = 5;
        }
        if(!$_REQUEST['LICENSE']){
            $arFields['ERROR']['LICENSE'] = 'Необходимо согласится с условиями лицензионного соглашения';
        }
        
        if($arFields['ERROR']){
            $GLOBALS['APPLICATION']->ThrowException('Ошибка');
            return false;    
            
        }
              
             
        
    }
 
 }
 
function OnBeforeUserUpdateHandler(&$arFields)
{
	if($_POST["TYPE"] != "CHANGE_PWD"){
		if(!$arFields['UF_FAVOURITES']){
			$arFields['LOGIN'] = $arFields["EMAIL"];
			if($arFields["PERSONAL_PHONE"]){
				$arFilter = Array(
					'PERSONAL_PHONE' => $arFields["PERSONAL_PHONE"],
					'!ID' => $arFields["ID"],
				);
				$userPhone = CUser::GetList(($by="ID"), ($order="ASC"),$arFilter, array('ID', 'PERSONAL_PHONE', 'EMAIL'))->fetch();
				if($userPhone){
					
					$GLOBALS['APPLICATION']->ThrowException('Пользователь с таким телефоном уже существует.');
					return false;
				}
			}
			else{
				$GLOBALS['APPLICATION']->ThrowException('Заполните телефон');
				return false;
			}
			if(!$arFields["NAME"]){
				$GLOBALS['APPLICATION']->ThrowException('Заполните ФИО');
				return false;
			}
		}
		
		if(($arFields["PASSWORD"]) && ($arFields["CONFIRM_PASSWORD"]) && ($arFields["ID"])){
			$rsUser = CUser::GetByID( $arFields["ID"]);
			if ($arUser = $rsUser->Fetch())
			{
				if(strlen($arUser["PASSWORD"]) > 32)
				{
					$salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
					$db_password = substr($arUser["PASSWORD"], -32);
				}
				else
				{
					$salt = "";
					$db_password = $arUser["PASSWORD"];
				}
				
				$user_password =  md5($salt.$_REQUEST['OLD_PASSWORD']);
				
				if ($user_password != $db_password )
				{
					$GLOBALS['APPLICATION']->ThrowException('Неверно введен старый пароль');
					return false;
				}
			}
		}
	}
 
    

}