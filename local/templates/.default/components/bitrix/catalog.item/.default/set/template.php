<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

$manufactory = array_pop($item['DISPLAY_PROPERTIES']['MANUFACTORY']['LINK_ELEMENT_VALUE'])
?>
<a href="<?=$item['DETAIL_PAGE_URL']?>" class="set__link"></a>
<div class="set__img">
    <img class="lazy" data-src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['PREVIEWS_PICTURE']['ALT']?>">
</div>
<div class="set__info">
    <div class="product__like"></div>
    <div id="<?=$itemIds['BASKET_ACTIONS']?>">
        <div class="product__cart"  id="<?=$itemIds['BUY_LINK']?>">В корзине</div>
    </div>
    <a href="<?=$item['DETAIL_PAGE_URL']?>" class="set__name"><?=$item['NAME']?></a>
    <div class="set__inside"><?=$item['DISPLAY_PROPERTIES']['SET_PREVIEW']['DISPLAY_VALUE']?></div>
    <div class="set__price" id="<?=$itemIds['PRICE']?>"><?=$price['PRINT_RATIO_PRICE']?></div>
    <div class="hidden" data-entity="quantity-block">
        <div class="product-item-amount">
            <div class="product-item-amount-field-container">
                <span class="hidden" id="<?=$itemIds['QUANTITY_DOWN']?>"></span>
                <input id="<?=$itemIds['QUANTITY']?>" type="hidden" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$measureRatio?>">
                <span class="hidden" id="<?=$itemIds['QUANTITY_UP']?>"></span>
            </div>
        </div>
    </div>
</div>