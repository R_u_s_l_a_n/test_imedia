<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if (!empty($arResult['ITEMS']))
{
    $arItemID = [];
    foreach ($arResult['ITEMS'] as $item)
    {
	    $arItemID[] = $item['ID'];
    }
    if($arItemID){
        $arDetailPageURL = [];
        $res = CIBlockElement::getList(array(), array('ID' => $arItemID, 'IBLOCK_ID' => $arParams['IBLOCK_ID']), false, false, array('DETAIL_PAGE_URL', 'ID'));
        while($arRes = $res->GetNext()){
            $arDetailPageURL[$arRes['ID']] = $arRes['DETAIL_PAGE_URL'];
        }
         foreach ($arResult['ITEMS'] as $key => $item)
	    {
	       if($arDetailPageURL[$item['ID']]){
	           $arResult['ITEMS'][$key]['DETAIL_PAGE_URL'] = $arDetailPageURL[$item['ID']];
	       }
	    }
        
    }
}
