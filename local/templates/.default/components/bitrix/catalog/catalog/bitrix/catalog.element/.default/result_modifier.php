<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
if(!empty($arResult['PROPERTIES']['SLIDER'])){
	foreach($arResult['PROPERTIES']['SLIDER']['VALUE'] as $fileId){
		$arResult['PROPERTIES']['SLIDER']['FILE_SRC'][] = CFile::GetPath($fileId);
	}
}