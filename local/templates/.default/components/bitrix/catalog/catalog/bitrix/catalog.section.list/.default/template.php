<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(empty($arResult['SECTIONS']))
    return;
$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>
<div class="left-menu__item left-menu__list <?if($arParams['OPEN'] == 'Y'):?>open<?endif;?>">
    <div class="left-menu__list-title"><?=GetMessage('CT_SECTION_LIST_TITLE')?></div>
    <i></i>
    <ul <?if($arParams['OPEN'] == 'Y'):?>style="display: block;"<?endif;?>>
        <?foreach ($arResult['SECTIONS'] as $arSection):
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            ?>
            <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                <a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
            </li>
        <?endforeach;?>
    </ul>
</div>
<?$this->SetViewTarget('catalog_section_list');?>
    <div class="inner-header__slider slider-init swiper-slider">
        <div class="swiper-arrow swiper-prev">
            <i></i>
        </div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?foreach ($arResult['SECTIONS'] as $arSection):?>
                    <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="swiper-slide inner-header__slide"><?=$arSection['NAME']?></a>
                <?endforeach;?>
            </div>
        </div>
        <div class="swiper-arrow swiper-next">
            <i></i>
        </div>
    </div>
<?$this->EndViewTarget();?>