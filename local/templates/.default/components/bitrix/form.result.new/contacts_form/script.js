$(function(){
    
    	var sendWebForm = function(event, data){
    	var $thisForm = $(this);
        var formId = $thisForm.data("id");   
	  		$.ajax({
	  			url: webFormAjaxDir+ "?FORM_ID=" + formId,
	  			data: $thisForm.serialize(),
			    cache: false,
		        type: "POST" ,
		        dataType: "json",
	  			success: function(response){
	  			  
                   $('.webFormResult').html('');
                   
	               if(response["SUCCESS"] != "Y"){
                      $i = 0;
                       $.each(response["ERROR"], function(nextId, nextValue){
                            if($i == 0){
                                 $('.webFormResult').append(nextValue);
                                 $i++;
                            }
                           
    		  			});
                   }
                   else{
                        $('.webFormResult').append('Форма успешно отправлена');
                        $thisForm[0].reset();
                   }
		  		}

	  		});
		return false;

	}

	$(document).on("submit", "#contacts_page_form", sendWebForm);

});