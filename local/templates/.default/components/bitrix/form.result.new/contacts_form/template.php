<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="row eco-pack">
	<div class="col-12 d-flex justify-content-center align-items-center flex-wrap">
		<div class="eco-pack__title">
			<div class="main-title"><?=$arResult["FORM_TITLE"]?></div>
			<div class="main-description"><?=$arResult["FORM_DESCRIPTION"]?></div>
		</div>
        <?if($arResult["FORM_IMAGE"]["URL"]):?>
    		<div class="eco-pack__img">
    			<img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" alt="">
    		</div>
        <?endif;?>
		<div class="eco-pack__form">
            <?if($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
			<form id="contacts_page_form" action="" class="form" data-id="<?=$arResult["arForm"]["ID"]?>">
                     <div class="webFormResult"></div>   
                    <input type="hidden" name="SITE_ID" value="<?=SITE_ID?>">
                    <input type="hidden" name="web_form_apply" value="Y" />
                    <input type="hidden" name="WEB_FORM_ID" value="<?=$arResult["arForm"]["ID"]?>">
                    <input type="hidden" name="sessid" id="sessid_1" value="<?=$_SESSION['fixed_session_id']?>">
                    <?if(!empty($arResult["QUESTIONS"])):?>
                    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
                	{
                		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
                		{
                			echo $arQuestion["HTML_CODE"];
                		}
                		else
                		{
                          ?>
            				<div class="form__item" id="WEB_FORM_ITEM_<?=$FIELD_SID?>">
            					<div class="title"><?=$arQuestion['CAPTION']?></div>
            					<input type="<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>" name="form_<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$arQuestion['STRUCTURE'][0]['FIELD_ID']?>" placeholder="<?=$arQuestion['CAPTION']?>" value="<?=$arQuestion['STRUCTURE'][0]['VALUE']?>" class="<?=$arResult["arQuestions"][$FIELD_SID]['COMMENTS']?>" <?if($arQuestion['REQUIRED']):?>required=""<?endif;?>>
            				</div>
                        <?
                        }
                    }?>
                    <?endif;?>
				<input type="submit" name="web_form_submit" class="btn" value="<?=GetMessage("FORM_SEND")?>">
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	var webFormAjaxDir = "<?=$templateFolder?>/ajax.php";
</script>
