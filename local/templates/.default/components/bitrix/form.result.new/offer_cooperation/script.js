
$(function(){

    	var sendWebForm = function(event, data){
        var name = '';
        var value = '';
            var $thisForm = $('#request-popup');
            var $thisFormFields = $thisForm.find("input");
                var data = [];
    		$thisFormFields.each(function(i, nextField){
                name = nextField.name;
                value = nextField.value;
                data[name] = value;
    		});
            console.log(data);
	  		$.ajax({
	  			url: webFormAjaxDir+ "?FORM_ID="+data['WEB_FORM_ID'],
	  			data: $thisFormFields.serializeArray(),
			    cache: false,
		        type: "POST" ,
		        dataType: "json",
	  			success: function(response){
	  			  
                   $('.webFormResult').html('');
                   
	               if(response["SUCCESS"] != "Y"){
                      $i = 0;
                       $.each(response["ERROR"], function(nextId, nextValue){
                            if($i == 0){
                                 $('.webFormResult').append(nextValue);
                                 $i++;
                            }
                           
    		  			});
                   }
                   else{
                        $('.webFormResult').append('Форма успешно отправлена');
    	                    $thisFormFields.each(function(i, nextField){
                                $(nextField).val('');
                    		});
                   }
		  		}

	  		});
		return false;

	}

	$(document).on("click", "#cooperation_submit", sendWebForm);

});