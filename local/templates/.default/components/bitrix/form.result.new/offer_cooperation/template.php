<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array("jquery"));
?>
    <div class="popup__title"><?=$arResult["FORM_DESCRIPTION"]?></div>
    <div class="webFormResult"></div>   
    <input type="hidden" name="SITE_ID" value="<?=SITE_ID?>">
    <input type="hidden" name="web_form_apply" value="Y" />
    <input type="hidden" name="WEB_FORM_ID" value="<?=$arResult["arForm"]["ID"]?>">
    <input type="hidden" name="sessid" id="sessid_1" value="<?=$_SESSION['fixed_session_id']?>">
    <?if(!empty($arResult["QUESTIONS"])):?>
    <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
          ?>
			<div class="form__item" id="WEB_FORM_ITEM_<?=$FIELD_SID?>">
				<div class="title"><?=$arQuestion['CAPTION']?></div>
				<input type="<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>" name="form_<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$arQuestion['STRUCTURE'][0]['FIELD_ID']?>" placeholder="<?=$arQuestion['CAPTION']?>" value="<?=$arQuestion['STRUCTURE'][0]['VALUE']?>" class="<?=$arResult["arQuestions"][$FIELD_SID]['COMMENTS']?>" <?if($arQuestion['REQUIRED']):?>required=""<?endif;?>>
			</div>
        <?
        }
    }?>
    <div class="popup__item">
        <div class="btn" id="cooperation_submit"><?=GetMessage("COOPERATION_SEND")?></div>
    </div>
    <?endif;?>

<script type="text/javascript">
	var webFormAjaxDir = "<?=$templateFolder?>/ajax.php";
</script>
