<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>

<section class="reviews-page">
	<div class="container">
        <?if (!empty($arResult["ERRORS"])):?>
        	<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
        <?endif;
        if (strlen($arResult["MESSAGE"]) > 0):?>
        	<?ShowNote($arResult["MESSAGE"])?>
        <?endif?>
		<div class="row">
            <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
            	<?=bitrix_sessid_post()?>
            	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
    			<div class="reviews-page__item">
    				<div class="form__item">
    					<div class="title">ФИО</div>
    					<input type="text" placeholder="ФИО" name="PROPERTY[NAME][0]" value="">
    				</div>
    				<div class="form__item">
    					<div class="title">Компания</div>
    					<input name="PROPERTY[19][0]" type="text" placeholder="Компания" value="">
    				</div>
    			</div>
    			<div class="reviews-page__item">
    				<textarea name="PROPERTY[PREVIEW_TEXT][0]" id="" cols="30" maxlength="1000" rows="10" class="review-text"></textarea>
                    
                    <div class="reciew_img">
                        <div class="reciew_remove"></div>
                   	    <input type="hidden" name="PROPERTY[PREVIEW_PICTURE][0]" value="">
					    <input type="file" size="" name="PROPERTY_FILE_PREVIEW_PICTURE_0">
                    </div>
                
                    
                    <input type="submit" name="iblock_submit" class="btn reviews-page__button" value="Оправить" />
                    

    			</div>
            </form>

		</div>
	</div>
</section>
