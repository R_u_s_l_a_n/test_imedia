
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	if($arResult['ERRORS']){
        foreach($arResult['ERRORS'] as $error){
            $arReturn["ERROR"] .= $error.'<br />';
        }
			
	}else{
		$arReturn["SUCCESS"] = "Y";
	}

	echo \Bitrix\Main\Web\Json::encode($arReturn);

?>