$(function(){
    
    	var sendWebForm = function(event, data){
    	var $thisForm = $(this);
        var formId = $thisForm.data("id");   
	  		$.ajax({
	  			url: ajaxDirForgotPass,
	  			data: $thisForm.serialize(),
			    cache: false,
		        type: "POST" ,
		        dataType: "json",
	  			success: function(response){
	  			  
                   $('.webFormResult').html('');
                   
   	                    if(response["SUCCESS"] != "Y"){
                        $('.err_mess').html(response["ERROR"]);

                   }
                   else{
                        $thisForm[0].reset();
                        $('.err_mess').html('');
                        $('#forgot_password_btn').trigger('click');
                   }
		  		}

	  		});
		return false;

	}

	$(document).on("submit", "#forgot_password_form", sendWebForm);

});