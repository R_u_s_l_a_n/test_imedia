<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)
{
	die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if ($arResult['AUTHORIZED'])
{
	echo Loc::getMessage('MAIN_AUTH_PWD_SUCCESS');
	return;
}

?>
	<form id="forgot_password_form" name="bform" method="post" target="_top" action="<?= POST_FORM_ACTION_URI;?>">
        <div class="popup__title"><?= Loc::getMessage('MAIN_AUTH_TITLE');?></div>
      <div class="err_mess"></div>
    	<div class="form__item">
    		<div class="title">Email</div>
    		<input name="<?= $arResult['FIELDS']['email'];?>" type="text">
    	</div>
        <input name="<?= $arResult['FIELDS']['action'];?>" type="hidden" value="Y" type="text">
        <input type="submit" class="btn btn-primary" name="<?= $arResult['FIELDS']['action'];?>" value="<?= Loc::getMessage('MAIN_AUTH_PWD_FIELD_SUBMIT');?>" />
    </form>
    <a style="display: none;" data-fancybox data-src="#restore-popup2" href="javascript:;" class="btn" id="forgot_password_btn"></a>
	<a href="javascript:;" data-fancybox data-src="#login-popup" class="btn btn--white">Вход</a>
    <script type="text/javascript">
    	var ajaxDirForgotPass = "<?=$templateFolder?>/ajax.php";
    </script>
