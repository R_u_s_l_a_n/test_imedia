<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}
//pr($arResult);
?>



<section class="favorite-page account-page">
	<div class="container">
        <form method="post" action="<?=$arResult["FORM_TARGET"]?>">
            <?ShowError($arResult["strProfileError"]);?>
            <?
            if ($arResult['DATA_SAVED'] == 'Y')
            	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
            ?>
		<div class="row">
			<div class="col-12 d-flex flex-wrap">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:main.include",
            	"",
            	Array(
            		"AREA_FILE_SHOW" => "file",
            		"PATH" => SITE_DIR."include/sale_info.php"
            	),
            false,
            Array(
            	'HIDE_ICONS' => 'Y'
            )
            );?>

			</div>
            
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
            
			<div class="account-page__form">
				<div class="form__title">Персональные данные</div>
				<div class="form__item">
					<div class="title">Телефон*</div>
					<input name="PERSONAL_PHONE" type="text" class="phone" placeholder="Телефон*" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
				</div>
				<div class="form__item">
					<div class="title">Email*</div>
					<input name="EMAIL" value="<? echo $arResult["arUser"]["EMAIL"]?>" type="text" placeholder="Email*" value="<?=$arResult["arUser"]["EMAIL"]?>">
				</div>
				<div class="form__item">
					<div class="title">ФИО</div>
					<input type="text"  name="NAME" value="<?=$arResult["arUser"]["NAME"]?>" placeholder="ФИО" value="<?=$arResult["arUser"]["NAME"]?>">
				</div>
                <?if(in_array(5,$USER->GetGroups())):?>
				<div class="form__item">
					<div class="title">Адрес</div>
					<input name="UF_ADDRESS" type="text" placeholder="Адрес" value="<?=$arResult["arUser"]["UF_ADDRESS"]?>">
				</div>
                <?else:?>
                    <div class="form__inner">
                        <div class="form__item full-width">
                            <div class="title">Город*</div>
                            <input name="UF_CITY" type="text" placeholder="Город*" value="<?=$arResult['arUser']['UF_CITY']?>" required="" class="form-input">
                        </div>
                        <div class="form__item full-width">
                            <div class="title">Улица*</div>
                            <input name="UF_STREET" type="text" placeholder="Улица*" value="<?=$arResult['arUser']['UF_STREET']?>" required="" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Дом(корпус)*</div>
                            <input name="UF_HOUSE" type="text" placeholder="Дом(корпус)*" value="<?=$arResult['arUser']['UF_HOUSE']?>" required="" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Подъезд</div>
                            <input name="UF_ENTER" type="text" value="<?=$arResult['arUser']['UF_ENTER']?>" placeholder="Подъезд"  class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Этаж</div>
                            <input name="UF_FLOOR" type="text" value="<?=$arResult['arUser']['UF_FLOOR']?>" placeholder="Этаж" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Квартира(офис)*</div>
                            <input name="UF_FLAT" type="text" value="<?=$arResult['arUser']['UF_FLAT']?>" placeholder="Квартира(офис)*" required="" class="form-input">
                        </div>
                    </div>
                <?endif;?>
			</div>

			<div class="account-page__form">
				<div class="form__title">Изменить пароль</div>
				<div class="form__item password hide">
					<div class="title">Старый пароль</div>
					<input name="OLD_PASSWORD" type="password">
					<div class="show-password"></div>
				</div>
				<div class="form__item password hide">
					<div class="title">
						Новый пароль
					</div>
					<input name="NEW_PASSWORD"  type="password">
					<div class="show-password"></div>
				</div>
				<div class="form__item password hide">
					<div class="title">Повторите пароль</div>
					<input name="NEW_PASSWORD_CONFIRM" type="password">
					<div class="show-password"></div>
				</div>
                <input type="submit" class="btn" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
			</div>
           
		</div>
         </form>
	</div>
</section>

