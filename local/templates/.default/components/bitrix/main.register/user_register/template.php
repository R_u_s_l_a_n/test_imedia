<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}

?>

<section class="favorite-page account-page register-page">
	<div class="container">
    <?if($USER->IsAuthorized()):?>
        <p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
        <?if($_REQUEST['REGISTER']):?>
            <div id="show_reg_popap"  data-fancybox data-src="#account-popup"  class="btn" style="display: none;"></div>
            <script>
                $('#show_reg_popap').trigger('click');
            </script>
        <?endif;?>
    
    <?else:?>    
    <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
             <?
            if($arResult["BACKURL"] <> ''):
            ?>
            	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
            endif;
            ?>
            <input type="hidden" name="REGISTER[LOGIN]" value="user_<?=strtotime("now")?>" />
    		<div class="row">
    			<div class="account-page__form ">
    				<div class="form__title">Персональные данные</div>
    				<div class="form__item <?if(($arResult['VALUES']['ERROR']['PERSONAL_PHONE']) || ($arResult['ERRORS']['PERSONAL_PHONE'])):?>form__error<?endif;?>">
    					<div class="title " >Телефон*</div>
    					<input name="REGISTER[PERSONAL_PHONE]" type="text" class="phone" placeholder="Телефон*" value="<?=$arResult['VALUES']['PERSONAL_PHONE']?>">
                        <?if($arResult['VALUES']['ERROR']['PERSONAL_PHONE']):?>
                            <div class="form__error-text"><?=$arResult['VALUES']['ERROR']['PERSONAL_PHONE']?></div>
                        <?endif;?>
    				</div>
    				<div class="form__item <?if(($arResult['VALUES']['ERROR']['EMAIL']) || ($arResult['ERRORS']['EMAIL'])):?>form__error<?endif;?>">
    					<div class="title">Email*</div>
    					<input name="REGISTER[EMAIL]" type="text" placeholder="Email*" value="<?=$arResult['VALUES']['EMAIL']?>">
                        <?if($arResult['VALUES']['ERROR']['EMAIL']):?>
                            <div class="form__error-text"><?=$arResult['VALUES']['ERROR']['EMAIL']?></div>
                        <?endif;?>
    				</div>
    				<div class="form__item <?if($arResult['ERRORS']['NAME']):?>form__error<?endif;?>">
    					<div class="title">ФИО</div>
    					<input name="REGISTER[NAME]" type="text" placeholder="ФИО" value="<?=$arResult['VALUES']['NAME']?>">
    				</div>
    				
                    <div class="form__inner">
                        <div class="form__item full-width">
                            <div class="title">Город*</div>
                            <input name="UF_CITY" type="text" placeholder="Город*" value="<?=$arResult['VALUES']['UF_CITY']?>" required="" class="form-input">
                        </div>
                        <div class="form__item full-width">
                            <div class="title">Улица*</div>
                            <input name="UF_STREET" type="text" placeholder="Улица*" value="<?=$arResult['VALUES']['UF_STREET']?>" required="" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Дом(корпус)*</div>
                            <input name="UF_HOUSE" type="text" placeholder="Дом(корпус)*" value="<?=$arResult['VALUES']['UF_HOUSE']?>" required="" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Подъезд</div>
                            <input name="UF_ENTER" type="text" value="<?=$arResult['VALUES']['UF_ENTER']?>" placeholder="Подъезд"  class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Этаж</div>
                            <input name="UF_FLOOR" type="text" value="<?=$arResult['VALUES']['UF_FLOOR']?>" placeholder="Этаж" class="form-input">
                        </div>
                        <div class="form__item">
                            <div class="title">Квартира(офис)*</div>
                            <input name="UF_FLAT" type="text" value="<?=$arResult['VALUES']['UF_FLAT']?>" placeholder="Квартира(офис)*" required="" class="form-input">
                        </div>
                    </div>
    			</div>
    
    			<div class="account-page__form">
    				<div class="form__title">Пароль</div>
    				<div class="form__item password hide <?if(($arResult['VALUES']['ERROR']['PASSWORD']) || ($arResult['ERRORS']['PASSWORD'])):?>form__error<?endif;?>">
    					<div class="title">Придумайте пароль</div>
    					<input name="REGISTER[PASSWORD]" type="password" value="">
                        <?if($arResult['VALUES']['ERROR']['PASSWORD']):?>
                            <div class="form__error-text"><?=$arResult['VALUES']['ERROR']['PASSWORD']?></div>
                        <?endif;?>
    					<div class="show-password"></div>
    				</div>
    				<div class="form__item password hide <?if(($arResult['VALUES']['ERROR']['CONFIRM_PASSWORD']) || ($arResult['ERRORS']['CONFIRM_PASSWORD'])):?>form__error<?endif;?>">
    					<div class="title">Повторите пароль</div>
    					<input name="REGISTER[CONFIRM_PASSWORD]" type="password">
                        <?if($arResult['VALUES']['ERROR']['CONFIRM_PASSWORD']):?>
                            <div class="form__error-text"><?=$arResult['VALUES']['ERROR']['CONFIRM_PASSWORD']?></div>
                        <?endif;?>
    					<div class="show-password"></div>
    				</div>
    				<div class="form__item form__item--check">
                        <?if($arResult['VALUES']['ERROR']['LICENSE']):?>
                                <span class="license_err"><?=$arResult['VALUES']['ERROR']['LICENSE']?></span>
                        <?endif;?>
    					<label class="checkbox">Согласен с условиями &nbsp; <a href="#"> Лицензионного соглашения</a>
    						<input name="LICENSE" type="checkbox" checked>
    						<span class="checkmark"></span>
    					</label>
    				</div>
                        <input type="submit" name="register_submit_button" class="btn" value="<?=GetMessage("AUTH_REGISTER")?>" />
    				    <!--<div  data-fancybox data-src="#account-popup"  class="btn">Создать аккаунт</div>-->
    			</div>
    		</div>
     </form>
    <?endif?>
	</div>
</section>
