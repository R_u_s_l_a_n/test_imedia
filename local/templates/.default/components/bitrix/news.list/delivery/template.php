<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="delivery-page">
	<div class="container">
		<div class="row">
			<p class="delivery-page__caption">
                 <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array("AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/delivery_page_text.php"),
                    false
                ); ?>
			</p>
		</div>
	</div>
<?if($arResult["ITEMS"]):?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
    	<?
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="container-fluid delivery-page__item" style="background-image: url('<?=CFile::GetPath($arItem['PROPERTIES']['ICON']['VALUE']);?>')">
    		<div class="row">
    			<div class="container">
    				<div class="row">
    					<div class="delivery-page__left">
    						<div class="dispatch-page__title"><?=$arItem['NAME']?></div>
    						<?=$arItem['DETAIL_TEXT']?>
    						<p class="bottom"><?=$arItem['PREVIEW_TEXT']?></p>
    					</div>
    					<div class="delivery-page__right">
    						<img src="" alt="">
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    <?endforeach;?>
<?endif;?>
</section>

