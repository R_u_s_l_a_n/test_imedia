<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if($arResult["ITEMS"]){
    $arResult['PRICE'] = [];
    $arResult['PRODUCT_LINK'] = [];
    $arProductID = [];
    foreach($arResult["ITEMS"] as $item){
        if($item['PROPERTIES']['PRODUCT']['VALUE']){
            $arProductID[] = $item['PROPERTIES']['PRODUCT']['VALUE'];
            $productPrice = CCatalogProduct::GetOptimalPrice($item['PROPERTIES']['PRODUCT']['VALUE'], '1', $USER->GetUserGroupArray(), 'N');
            $arResult['PRICE'][$item['PROPERTIES']['PRODUCT']['VALUE']] = $productPrice['DISCOUNT_PRICE'];
        }
    }
    if($arProductID){
        $rs = CIBlockElement::GetList(
           [],
           ['ID' => $arProductID],
           false, false,
           ['ID', 'DETAIL_PAGE_URL',]
        );
        while ($ar = $rs->GetNext()) {
           $arResult['PRODUCT_LINK'][$ar['ID']] = $ar['DETAIL_PAGE_URL'];
        }
    }
}
   