<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult["ITEMS"]):?>
<section class="top-slider">
	<div class="container-fluid">
		<div class="row">
			<div class="top-slider__slider">
            <?if($arParams["DISPLAY_TOP_PAGER"]):?>
            	<?=$arResult["NAV_STRING"]?><br />
            <?endif;?>
            <?foreach($arResult["ITEMS"] as $arItem):?>
            	<?
            	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="top-slider__slide">
					<div class="top-slider__inner">
						<div class="main-title"><?=$arItem['NAME']?></div>
                        <?if($arItem['PROPERTIES']['TEXT']['VALUE']):?>
						  <div class="description"><?=$arItem['PROPERTIES']['TEXT']['VALUE']?></div>
                        <?endif;?>
                        <?if(($arResult['PRICE'][$arItem['PROPERTIES']['PRODUCT']['VALUE']]) && ($arItem['PROPERTIES']['PRODUCT']['VALUE'])):?>
						<div class="top-slider__price">от <span><?=$arResult['PRICE'][$arItem['PROPERTIES']['PRODUCT']['VALUE']]?></span> byn</div>
                        <?endif;?>
                        <?if($arItem['PROPERTIES']['LINK']['VALUE'] || (($arItem['PROPERTIES']['PRODUCT']['VALUE']) && ( $arResult['PRODUCT_LINK'][$arItem['PROPERTIES']['PRODUCT']['VALUE']]))):?>
                          <?
                          if($arResult['PRODUCT_LINK'][$arItem['PROPERTIES']['PRODUCT']['VALUE']]){
                            $link = $arResult['PRODUCT_LINK'][$arItem['PROPERTIES']['PRODUCT']['VALUE']];
                          }
                          else{
                            $link = $arItem['PROPERTIES']['LINK']['VALUE'];
                          }
                            
                          ?>   
						  <div class="btn top-slider__btn" onclick="location.replace('<?=$link?>');"><?=$arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']?></div>
                        <?endif;?>
                        <?if($arItem['PREVIEW_PICTURE']['SRC']):?>
						  <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="package-img" alt="">
                        <?endif;?>
					</div>
                    <?if($arItem['DETAIL_PICTURE']['SRC']):?>
					   <img class="bg-img" src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="">
                    <?endif;?>
				</div>
            <?endforeach;?>
            </div>
		</div>
	</div>
</section>
<?endif;?>


