<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
?>
<?if($arResult["ITEMS"]):?>
<section class="reviews-page">

	<div class="container">
        <div class="row row_reviews_button">
             <a href="/reviews/add/" class="btn">Оставить отзыв</a>
        </div>
       
		<div class="row row_reviews_item">
        
            <?foreach($arResult["ITEMS"] as $arItem):?>
        	<?
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	
            
            if(strlen ($arItem["PREVIEW_TEXT"]) > 300){
                $pos = -1;
                $result = [];
                $position = 0;
                while(($pos = strpos($arItem["PREVIEW_TEXT"], '.', $pos+1))!==false) {
                    if(($pos < 300) && ($pos > $position)){
                        $position = $pos;
                    }
                    
                }
                $pos = -1;
                while(($pos = strpos($arItem["PREVIEW_TEXT"], '?', $pos+1))!==false) {
                    if(($pos < 300) && ($pos > $position)){
                        $position = $pos;
                    }
                }
                $pos = -1;
                while(($pos = strpos($arItem["PREVIEW_TEXT"], '!', $pos+1))!==false) {
                    if(($pos < 300) && ($pos > $position)){
                        $position = $pos;
                    }
                }
                
                if(!$position){
                    $position = 300;
                }
                
                $openText =  substr($arItem["PREVIEW_TEXT"], 0, $position);
                $hiddenText = substr($arItem["PREVIEW_TEXT"], $position);
                
            }
            else{
                $openText = $arItem["PREVIEW_TEXT"];
            }
            ?>
			<div class="reviews-page__item">
                
				<div class="reviews-page__img">
                    <?if($arItem["PREVIEW_PICTURE"]["SRC"]):?>
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                    <?endif;?>
				</div>
                
				<div class="reviews-page__desc">
					<div class="reviews-page__name"><?=$arItem['NAME']?> <?if($arItem['PROPERTIES']['COMPANY']['VALUE']):?>, <?=$arItem['PROPERTIES']['COMPANY']['VALUE']?><?endif;?></div>
					<div class="reviews-page__text "><?=$openText?> 
                        <?if($hiddenText):?>
                            <span>
                                <?=$hiddenText?>
                            </span>
                        <?endif;?>
					</div>

				</div>
                <?if($hiddenText):?>
    				<a href="#" class="reviews-page__more">
    					<span><?=Loc::getMessage('SHOW_TEXT_LINK');?></span> <i></i>
    				</a>
                <?endif;?>
			</div>
            <?endforeach;?>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  	              <?=$arResult["NAV_STRING"]?>
            <?endif;?>
		</div>
	</div>
</section>
<?endif;?>
