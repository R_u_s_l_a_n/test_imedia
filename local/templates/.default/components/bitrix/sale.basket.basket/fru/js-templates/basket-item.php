<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

?>
<script id="basket-item-template" type="text/html">
    <div class="cart__item" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
		<a href="{{DETAIL_PAGE_URL}}" class="cart__img">
			<img src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}" alt="{{NAME}}">
		</a>
		<div class="cart__desc">
			<div class="cart__info">
				<a href="{{SECTION_URL}}" class="cart__category">Категория: {{SECTION_NAME}}</a>
				<a href="{{DETAIL_PAGE_URL}}" class="cart__name">
					<b>{{NAME}}{{#MANUFACTORY}},{{/MANUFACTORY}}</b> {{MANUFACTORY}}
				</a>
				<div class="cart__weight">Вес: {{PRODUCT_WEIGHT}}</div>
			</div>
			<div class="counter" data-entity="basket-item-quantity-block">
					<div class="counter__min" data-entity="basket-item-quantity-minus"></div>
				<div class="counter__text">
					<input maxlength="4" type="text" value="{{QUANTITY}}" data-step="1" data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field" id="basket-item-quantity-{{ID}}">
				</div>
				<div class="counter__plus" data-entity="basket-item-quantity-plus"></div>
			</div>
			<div class="cart__price" id="basket-item-price-{{ID}}">{{{SUM_PRICE_FORMATED}}}</div>
			<div class="close"  data-entity="basket-item-delete"></div>
		</div>
	</div>
</script>