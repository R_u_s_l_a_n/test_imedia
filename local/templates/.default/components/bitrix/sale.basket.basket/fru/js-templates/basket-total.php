<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */

?>
<script id="basket-total-template" type="text/html">
	<div class="col-12" data-entity="basket-checkout-aligner">
        <div class="row">
				<div class="cart__coupon">
					<div class="form__item">
						<input type="text" maxlength="20" placeholder="Код купона" id='basket-coupon-input' data-entity="basket-coupon-input">
					</div>
					<div class="btn" data-entity="basket-coupon-btn">ОК</div>
                    {{#COUPON_LIST}}
					<div class="basket-coupon-alert text-{{CLASS}}">
						<span class="basket-coupon-text">
							<strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
							{{#DISCOUNT_NAME}}({{DISCOUNT_NAME}}){{/DISCOUNT_NAME}}
						</span>
						<span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
							<?=Loc::getMessage('SBB_DELETE')?>
						</span>
					</div>
					{{/COUPON_LIST}}
				</div>
                <?if($arResult['DELIVERY']):?>
    				<div class="cart__delivery" data='basket-delivery-radio'>
                        <?$i = 0;?>
                        <?foreach($arResult['DELIVERY'] as  $deliveryInfo):?>
                            <?if($deliveryInfo['ID'] > 1):?>
        					<div class="form__item form__item--check">
        						<label class="checkbox"><?=$deliveryInfo['NAME']?>
                                    <?if($deliveryInfo['CONFIG']['MAIN']['PRICE'] > 0):?>
                                     : <?=CCurrencyLang::CurrencyFormat($deliveryInfo['CONFIG']['MAIN']['PRICE'], $deliveryInfo['CONFIG']['MAIN']['CURRENCY'], true);?>
                                     <?endif;?>
        							<input data-price="<?=$deliveryInfo['CONFIG']['MAIN']['PRICE']?>"  type="radio"  name="delivery" value='<?=$deliveryInfo['ID']?>' {{DELIVERY_<?=$deliveryInfo['ID']?>}}>
        							<span class="checkmark"></span>
        						</label>
        					</div>
                            <?$i++;?>
                            <?endif;?>
                        <?endforeach;?>
    				</div>
                <?endif;?>
			</div>
			<div class="row">
				<div class="cart__final-price">
					<p >Итого: <span data-entity="basket-total-price">{{{PRICE_FORMATED}}}</span></p>
					<div class="btn" data-entity="basket-checkout-button"><?=Loc::getMessage('SBB_ORDER')?></div>
				</div>
			</div>
        </div>
</script>