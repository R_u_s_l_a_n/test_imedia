<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);
if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		//$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
?>
<div class="col-12 d-flex flex-column">
    <div class="col-12 d-flex flex-wrap align-items-start flex-basis-unset">
	<div class="favorite-page__title">
		Текущие заказы
	</div>
	<div class="sort-block">
		Сортировать по
		<div <?if($_REQUEST['field'] == 'PRICE' && $_REQUEST['sort'] == 'ASC' ):?> onclick="sort_order('PRICE', 'DESC')"    <?else:?> onclick="sort_order('PRICE', 'ASC')"  <?endif;?> class="sort-block__item">Цене
            <?if($_REQUEST['field'] == 'PRICE' && $_REQUEST['sort'] == 'ASC' ):?>
                <div class="sort-arrow up"></div>
            <?else:?>
                <div  class="sort-arrow"></div>
            <?endif;?>
		</div>
		<div  <?if($_REQUEST['field'] == 'DATE_INSERT' && $_REQUEST['sort'] == 'ASC' ):?> onclick="sort_order('DATE_INSERT', 'DESC')"  <?else:?> onclick="sort_order('DATE_INSERT', 'ASC')" <?endif;?> class="sort-block__item">Дате
            <?if($_REQUEST['field'] == 'DATE_INSERT' && $_REQUEST['sort'] == 'ASC' ):?>
                <div   class="sort-arrow up"></div>
            <?else:?>
                <div   class="sort-arrow"></div>
            <?endif;?>
			
		</div>
	</div>
    <script>
        function sort_order(field, sort){
              oldUrl = window.location.search;
              var regex = new RegExp("(?:([?&])sort\s*=[^?&]*)");
              if (regex.test(oldUrl)) {
                newUrl = oldUrl.replace(regex, "$1sort=" + sort);
              } else if (/\?/.test(oldUrl)) {
                newUrl = oldUrl + "&sort=" + sort;
              } else {
                newUrl = oldUrl + "?sort=" + sort;
              }
              var regex = new RegExp("(?:([?&])field\s*=[^?&]*)");
              if (regex.test(newUrl)) {
                newUrl = newUrl.replace(regex, "$1field=" + field);
              } else if (/\?/.test(newUrl)) {
                newUrl = newUrl + "&field=" + field;
              } else {
                newUrl = newUrl + "?field=" + field;
              }
            window.location.replace(newUrl);
        }
    </script>
</div>
<div class="history-page__items current_orders">
    <?if($arResult['ORDERS']):?>
         <?foreach ($arResult['ORDERS'] as $key => $order):
         ?>
		<div class="col-12 col-sm-6 col-md-6 col-lg-4 history-page__item current_order_item">
			<div class="order-block__final">
				<div class="product-title">Заказ <p class="paid"><?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME']?></p> <span><?=$order['ORDER']['DATE_INSERT_FORMATED']?></span></div>
                <?if($order['BASKET_ITEMS']):?>  
                    <?$numerProduct = 0;?>
                    <?foreach ($order['BASKET_ITEMS'] as $keyItem => $item):?> 
                        <?$numerProduct++;?> 
					<div class="order-block__item <?if($numerProduct > 3):?>hidden<?endif;?>">
						<div class="order-block__name"><a href="<?=$item['DETAIL_PAGE_URL']?>"><b><?=$item['NAME']?><?if($arResult['BASKET_PROPS'][$item['ID']]['MANUFACTORY_NAME']):?>,<?endif;?></b><?=$arResult['BASKET_PROPS'][$item['ID']]['MANUFACTORY_NAME']?></a></div>
						<div class="order-block__price"><?=CurrencyFormat($item['PRICE'], $item['CURRENCY']);?></div>
						<div class="order-block__weight">
                            <?if($item['WEIGHT'] >= 1000):?>
                                <?=(float)$item['WEIGHT']/1000?> кг
                            <?else:?>
                                 <?=(float)$item['WEIGHT']?> г
                            <?endif;?>
                        </div>
					</div>
                    <?endforeach;?>
                <?endif;?>
				<div class="order-block__price">
					<?if(count($order['BASKET_ITEMS']) > 3):?><a href="#" class="more">Подробнее</a><?endif;?> <span>Итого </span> <?=$order['ORDER']['FORMATED_PRICE']?> 
				</div>
                <?if($order['ORDER']['PAY_SYSTEM_ID'] == 2  && $order['ORDER']['PAYED'] == 'N'):?>
                <?
                Loader::includeModule("sale");
                $orderObj = Sale\Order::load($order['ORDER']['ID']);
                $paymentCollection = $orderObj->getPaymentCollection();
                $payment = $paymentCollection[0];
                $service = Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
                $context = \Bitrix\Main\Application::getInstance()->getContext();
                $initResult = $service->initiatePay($payment, $context->getRequest(), \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
                $buffered_output = $initResult->getTemplate();
                echo '<pre>';
                print_r($buffered_output);
                echo '</pre>';
                ?>
                <?endif;?>
				<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>" class="btn">Повторить заказ</a>
			</div>
		</div>
        <?endforeach;?>
    <?endif;?>	
</div>
<?=$arResult["NAV_STRING"];?>
</div>		
<?
}
