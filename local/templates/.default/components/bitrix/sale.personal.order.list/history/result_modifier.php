<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
if($arResult['ORDERS']){
    $arProductBasketID = [];
    foreach ($arResult['ORDERS'] as $key => $order){
        foreach ($order['BASKET_ITEMS'] as $keyItem => $item){
            $arProductBasketID[] = $item['ID'];
        }
        
    }
    if($arProductBasketID){
        $db_res = CSaleBasket::GetPropsList(
        array(),
        array("BASKET_ID" => $arProductBasketID)
    );
    while ($ar_res = $db_res->Fetch())
    {
       $arResult['BASKET_PROPS'][$ar_res['BASKET_ID']][$ar_res['CODE']] = $ar_res['VALUE'];
    }
    }
}