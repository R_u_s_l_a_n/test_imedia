<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);
if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
?>
<div class="col-12 d-flex flex-column">
    <div class="col-12 d-flex flex-wrap align-items-start flex-basis-unset">
	<div class="favorite-page__title">
		Прошлые заказы
	</div>
	<div class="sort-block">
		Сортировать по
		<div  <?if($_REQUEST['history_field'] == 'PRICE' && $_REQUEST['history_sort'] == 'ASC' ):?> onclick="sort('PRICE', 'DESC')" <?else:?> onclick="sort('PRICE', 'ASC')" <?endif;?> class="sort-block__item">Цене
            <?if($_REQUEST['history_field'] == 'PRICE' && $_REQUEST['history_sort'] == 'ASC' ):?>
                <div  class="sort-arrow up"></div>
            <?else:?>
                <div  class="sort-arrow"></div>
            <?endif;?>
		</div>
		<div <?if($_REQUEST['history_field'] == 'DATE_INSERT' && $_REQUEST['history_sort'] == 'ASC' ):?> onclick="sort('DATE_INSERT', 'DESC')" <?else:?> onclick="sort('DATE_INSERT', 'ASC')"  <?endif;?> class="sort-block__item">Дате
            <?if($_REQUEST['history_field'] == 'DATE_INSERT' && $_REQUEST['history_sort'] == 'ASC' ):?>
                <div class="sort-arrow up"></div>
            <?else:?>
                <div class="sort-arrow"></div>
            <?endif;?>
			
		</div>
	</div>
    <script>
        function sort(field, sort){
              oldUrl = window.location.search;
              var regex = new RegExp("(?:([?&])history_sort\s*=[^?&]*)");
              if (regex.test(oldUrl)) {
                newUrl = oldUrl.replace(regex, "$1history_sort=" + sort);
              } else if (/\?/.test(oldUrl)) {
                newUrl = oldUrl + "&history_sort=" + sort;
              } else {
                newUrl = oldUrl + "?history_sort=" + sort;
              }
              var regex = new RegExp("(?:([?&])history_field\s*=[^?&]*)");
              if (regex.test(newUrl)) {
                newUrl = newUrl.replace(regex, "$1history_field=" + field);
              } else if (/\?/.test(newUrl)) {
                newUrl = newUrl + "&history_field=" + field;
              } else {
                newUrl = newUrl + "?history_field=" + field;
              }
            window.location.replace(newUrl);
        }
    </script>
</div>
<div class="history-page__items history_orders">
    <?if($arResult['ORDERS']):?>
         <?foreach ($arResult['ORDERS'] as $key => $order):?>
		<div class="col-12 col-sm-6 col-md-6 col-lg-4 history-page__item history_order_item">
			<div class="order-block__final">
				<div class="product-title">Заказ <p class="paid"><?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]['NAME']?></p> <span><?=$order['ORDER']['DATE_INSERT_FORMATED']?></span></div>
                <?if($order['BASKET_ITEMS']):?>  
                    <?$numerProduct = 0;?>
                    <?foreach ($order['BASKET_ITEMS'] as $keyItem => $item):?> 
                        <?$numerProduct++;?> 
					<div class="order-block__item <?if($numerProduct > 3):?>hidden<?endif;?>">
						<div class="order-block__name"><a href="<?=$item['DETAIL_PAGE_URL']?>"><b><?=$item['NAME']?><?if($arResult['BASKET_PROPS'][$item['ID']]['MANUFACTORY_NAME']):?>,<?endif;?></b><?=$arResult['BASKET_PROPS'][$item['ID']]['MANUFACTORY_NAME']?></a></div>
						<div class="order-block__price"><?=CurrencyFormat($item['PRICE'], $item['CURRENCY']);?></div>
						<div class="order-block__weight">
                            <?if($item['WEIGHT'] >= 1000):?>
                                <?=(float)$item['WEIGHT']/1000?> кг
                            <?else:?>
                                 <?=(float)$item['WEIGHT']?> г
                            <?endif;?>
                        </div>
					</div>
                    <?endforeach;?>
                <?endif;?>
				<div class="order-block__price">
					<?if(count($order['BASKET_ITEMS']) > 3):?><a href="#" class="more">Подробнее</a><?endif;?> <span>Итого </span> <?=$order['ORDER']['FORMATED_PRICE']?> 
				</div>
				<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>" class="btn">Повторить заказ</a>
			</div>
		</div>
        <?endforeach;?>
    <?endif;?>	
</div>
<?=$arResult["NAV_STRING"];?>
</div>		
<?
}
