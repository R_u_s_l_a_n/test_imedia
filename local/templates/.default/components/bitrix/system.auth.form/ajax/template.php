
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	if($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']){
			$arReturn["ERROR"] = $arResult['ERROR_MESSAGE'];
	}else{
		$arReturn["SUCCESS"] = "Y";
	}

	echo \Bitrix\Main\Web\Json::encode($arReturn);

?>