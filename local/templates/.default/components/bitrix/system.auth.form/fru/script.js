$(function(){
    	var sendWebForm = function(event, data){
    	var $thisForm = $(this);
      
	  		$.ajax({
	  			url: ajaxAuthDir,
	  			data: $thisForm.serialize(),
			    cache: false,
		        type: "POST" ,
		        dataType: "json",
	  			success: function(response){
			   	 
                   $('.err_mess').html('');
	               if(response["SUCCESS"] != "Y"){
                        $('.auth_err_mess').html(response["ERROR"]['MESSAGE']);
                    
                   }
                   else if(response["SUCCESS"] == "Y"){
                        $thisForm[0].reset();
                        $(location).attr('href',bakUrlAuth);
                   }
		  		}

	  		});
		return false;

	}
	$(document).on("submit", "#auth_form", sendWebForm);

});