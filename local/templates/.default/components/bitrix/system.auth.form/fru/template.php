<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>


<?
//if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
//	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<form id="auth_form" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
    
    	<div class="popup__title"><?=GetMessage("AUTH_LOGIN_BUTTON")?></div>
        <div class="auth_err_mess err">
        </div>
    	<div class="form__item">
    		<div class="title"><?=GetMessage("AUTH_LOGIN")?></div>
    		<input name="USER_LOGIN" type="text">
            <script>
				BX.ready(function() {
					var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
					if (loginCookie)
					{
						var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
						var loginInput = form.elements["USER_LOGIN"];
						loginInput.value = loginCookie;
					}
				});
			</script>
    	</div>
    	<div class="form__item">
    		<div class="title"><?=GetMessage("AUTH_PASSWORD")?></div>
    		<input name="USER_PASSWORD" type="password">
    		<div class="show-password"></div>
    	</div>
    	<div class="popup__item">
    		<a data-fancybox data-src="#restore-popup" href="javascript:;" ><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
            <input class="btn" type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" />
            <!--<div class="btn">Вход</div>-->
    	</div>
    
    	<div class="popup__item register">
    		<span><?=GetMessage("AUTH_REGISTER_QUESTION")?></span>
    		<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="btn btn--white"><?=GetMessage("AUTH_REGISTER")?></a>
    	</div>
 
</form>
<script type="text/javascript">
	var ajaxAuthDir = "<?=$templateFolder?>/ajax.php";
    var bakUrlAuth = "<?=$arParams["PROFILE_URL"] ? $arParams["PROFILE_URL"] : SITE_DIR?>";
</script>

