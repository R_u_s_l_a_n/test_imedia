$(document).ready(function(){

    $(document).on('click', '#up', function(event){
        event.preventDefault();
        var targetContainer = $('.row_reviews_item'),     
        url =  $('#up').attr('data-url'); 

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    $('.load_more').remove();

                    var elements = $(data).find('.reviews-page__item'),
                        pagination = $(data).find('.load_more');
                    
                    targetContainer.append(elements); 
                    $(elements).find('.reviews-page__more').on('click', function (e) {
                		e.preventDefault();
                		if ($(this).parent().hasClass('show')) {
                			$(this).parent().removeClass('show');
                			$(this).children('span').text('Подробнее');
                		} else {
                			$(this).parent().addClass('show');
                			$(this).children('span').text('Скрыть');
                		}
                	});
                    targetContainer.append(pagination);
                     $(function ($) {
                        $('.lazy').lazy({
                          visibleOnly: true
                        });
                      });
                }
            })
        }

    });
});