<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
?>
<?if($arResult["NavPageCount"] > 1):?>

    <?if ($arResult["NavPageNomer"]+1 <= $arResult["nEndPage"]):?>
        <?
            $plus = $arResult["NavPageNomer"]+1;
            $url = $arResult["sUrlPathParams"] . "PAGEN_".$arResult["NavNum"]."=".$plus;

        ?>
        <div class="col-12 p-0 d-flex align-items-start justify-content-start load_more">
        	<a href="#" id="up" class="up"  data-url="<?=$url?>">
        		<img src="<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/icons/up.svg" alt="">
        		<?=Loc::getMessage('BOTTOM_PAGER');?>
            </a>
        </div>

    <?endif?>

<?endif?>

