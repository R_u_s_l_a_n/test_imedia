$(document).ready(function(){

    $(document).on('click', '#up', function(event){
		event.preventDefault();
        var targetContainer = $('.current_orders'),     
        url =  $('#up').attr('data-url'); 
    

		$('body').css({'overflow': 'hidden'});
        //masonry
    	var masonryOptions = {
    		itemSelector: '.current_order_item',
    		horizontalOrder: true,
    		gutter: 0
    	};
                                      
		var $grid = $('.current_orders');

		var moreButtonPos = $('.history-page__more').offset().top;
		
		var scrollTo = $('.history-page__items.current_orders').innerHeight() + $('.history-page__items.current_orders').offset().top;
        
        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    $('.load_more_curent_orders').remove();

                    var elements = $(data).find('.current_order_item'),
						pagination = $(data).find('.load_more_curent_orders');
                    targetContainer.append(elements); 
                    
                    $(elements).find('.more').on('click', function (e) {
                		e.preventDefault();
                        var $counter = 0;
                		var $text = '';
						var $historyItem = $(this).parent().parent();
						var offset = $(this).offset().top;
                        
                		if ($('.current_orders').length && $(window).width() > 575) {
                			$grid.masonry('destroy');
                		}
                
                
                		$(this).parent().parent().find('.order-block__item').each(function () {
                			$counter++;
                			if ($counter > 3) {
                				if ($(this).hasClass('hidden')) {
                					$(this).slideDown({
                						start: () => {
                							$(this).css({
                								display: 'flex'
                							});
                						}
                					});
                					$text = 'Свернуть';
                					$(this).removeClass('hidden');
                					$historyItem.addClass('open');
                
                				} else {
                					$(this).attr('style');
                					$(this).slideUp();
                					$(this).addClass('hidden');
                					$historyItem.removeClass('open');
                					$text = 'Подробнее';
                				}
                			}
                		});
                        
						$(this).text($text);
						
                		if ($('.current_orders').length && $(window).width() > 575) {
                			setTimeout(function () {
								$grid.masonry(masonryOptions);
								
								$('html, body').animate({
									scrollTop: offset
								}, 0);
                			}, 400);
                		}
                        
					});
					
                   
                    
                    setTimeout(function () {
                        $grid.masonry('destroy');
                    	if ($('.current_orders').length && $(window).width() > 575) {
								$grid.masonry(masonryOptions);
								
								if (pagination.length) {
									pagination.insertAfter(targetContainer);

									$('html, body').animate({
										scrollTop: moreButtonPos - 500
									}, 0);
								} else {
									$('html, body').animate({
										scrollTop: moreButtonPos - 500
									}, 0);
								}
                		} 
						
						$('body').css({'overflow': ''});
					}, 400);	
					
                    
                }
            })
        }

    });
});