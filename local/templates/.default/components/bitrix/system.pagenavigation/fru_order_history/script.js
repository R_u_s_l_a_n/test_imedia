$(document).ready(function(){

    $(document).on('click', '#up_history', function(event){
        event.preventDefault();
        var targetContainer = $('.history_orders'),     
        url =  $('#up_history').attr('data-url'); 
        
		console.log('123');

		$('body').css({'overflow': 'hidden'});

        //masonry
    	let masonryOptions = {
    		itemSelector: '.history_order_item',
    		horizontalOrder: true,
    		gutter: 0
    	};
                                      
    	let $grid = $('.history_orders');
        
        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    $('.load_more_history_orders').remove();

                    var elements = $(data).find('.history_order_item'),
                        pagination = $(data).find('.load_more_history_orders');
                    
                    targetContainer.append(elements); 
                    
                    $(elements).find('.more').on('click', function (e) {
                		e.preventDefault();
                        let $counter = 0;
                		let $text = '';
                		let $historyItem = $(this).parent().parent();
                        
                		if ($('.history_orders').length && $(window).width() > 575) {
                			$grid.masonry('destroy');
                		}
                
                
                		$(this).parent().parent().find('.order-block__item').each(function () {
                			$counter++;
                			if ($counter > 3) {
                				if ($(this).hasClass('hidden')) {
                					$(this).slideDown({
                						start: () => {
                							$(this).css({
                								display: 'flex'
                							});
                						}
                					});
                					$text = 'Свернуть';
                					$(this).removeClass('hidden');
                					$historyItem.addClass('open');
                
                				} else {
                					$(this).attr('style');
                					$(this).slideUp();
                					$(this).addClass('hidden');
                					$historyItem.removeClass('open');
                					$text = 'Подробнее';
                				}
                			}
                		});
                        
                		$(this).text($text);
                		if ($('.history_orders').length && $(window).width() > 575) {
                			setTimeout(function () {
                				$grid.masonry(masonryOptions);
                			}, 400);
                		}
                        
                	});
                   
                    
                    setTimeout(function () {
                        $grid.masonry('destroy');
                    	if ($('.history_orders').length && $(window).width() > 575) {
                				$grid.masonry(masonryOptions);
                		} 	
                    }, 300);	
                    
                     //targetContainer.append(pagination);
                     targetContainer.after(pagination);
                    
                }
            })
        }

    });
});