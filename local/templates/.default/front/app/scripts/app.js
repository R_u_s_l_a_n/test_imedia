const app = {
	pathToLibsFiles: '/local/templates/.default/front/dist/assets/libs'
};
window.app = app;

function getBasket() {
	if ($('.product__cart').length) {
		$.ajax('/local/ajax/get_basket.php', {
			dataType: 'json',
			success(result) {
				if (result.length) {
					for (const i in result) {
						$('[data-id="' + result[i] + '"]')
							.find('.product__cart')
							.closest('.product__actions')
							.find('.counter.counter_catalog')
							.addClass('counter--hidden');
						$('[data-id="' + result[i] + '"]').find('.product__cart').addClass('active');
					}
				}
			},
			error() {

			}
		});
	}
}

function getFavourites() {
	if ($('.product__like').length) {
		$.ajax('/local/ajax/get_favourites.php', {
			dataType: 'json',
			success(result) {
				if (result.length) {
					$('.wish-list .small-basket__count').text(result.length);
					for (const i in result) {
						$('[data-id="' + result[i] + '"]').find('.product__like').addClass('active');
					}
				}
			},
			error() {

			}
		});
	}
}

$(document).ready(function () {
	let $w_w = $(window).width();
	$(window).on('resize', function () {
		$w_w = $(window).width();
	});

	$('.product__tab')
		.find('.product__tab-title')
		.on('click', function () {
			let $parent = $(this).parents('.product__tab');
			let $eq = $(this).index();
			$parent.find('.product__tab-title').removeClass('active');
			$(this).addClass('active');
			$parent.find('.product__tab-content').fadeOut(300);
			setTimeout(function () {
				$parent.find('.product__tab-content').removeClass('acitve');
				$parent
					.find('.product__tab-content')
					.eq($eq)
					.addClass('active')
					.fadeIn();
			}, 300);
		});

	$('.insta-slider__slider').slick({
		dots: false,
		arrows: true,
		slidesToScroll: 1,
		variableWidth: true
	});

	const topSlideCount = $('.top-slider__slider').find('.top-slider__slide')
		.length;
	let topSlideShow = 1;
	let topSlideWidth = $w_w * 0.3667;

	if (topSlideCount > 2 && topSlideCount > 5) {
		topSlideShow = 2;
	} else if (topSlideCount > 4) {
		topSlideShow = 3;
		topSlideWidth = $w_w * 0.1667;
	}

	//calc width top slide
	function calc_slideWidth() {
		if ($w_w > 991) {
			//let $slide_width = $w_w * 0.1667;
			//let $slide_width = $w_w * 0.3667;
			let $active_slide_width = $w_w * 0.5069;
			$('.top-slider__inner').css('width', $active_slide_width);
			$('.top-slider__slider').css('width', $active_slide_width);
			$('.top-slider__slide').each(function () {
				$(this)
					.children('.bg-img')
					.css('width', $active_slide_width);
				$(this).css('width', $active_slide_width);
				if ($(this).hasClass('slick-active slick-center')) {
					$(this)
						.find('.top-slider__inner')
						.addClass('active');
					$(this).removeClass('slick-slide-change');
				} else {
					$(this).css('width', topSlideWidth);
					$(this)
						.find('.top-slider__inner')
						.removeClass('active');
					$(this).addClass('slick-slide-change');
				}
			});
		}
	}

	if (topSlideCount > 1) {
		$('.top-slider__slider').slick({
			dots: true,
			arrows: false,
			slidesToScroll: 1,
			variableWidth: true,
			slidesToShow: topSlideShow,
			infinite: true,
			centerMode: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						variableWidth: false,
						dots: true
					}
				}
			]
		});

		calc_slideWidth();
		$('.top-slider__slider').slick('slickGoTo', 0);
		$('.top-slider__slider').on('beforeChange', function () {
			$('.top-slider__slide').removeClass('slick-slide-change');
		});
		$('.top-slider__slider').on('afterChange', function () {
			if ($w_w > 991) {
				calc_slideWidth();
			}
		});
	} else {
		$('.top-slider__slide').addClass('no-init');
		$('.top-slider__inner').addClass('no-init');
	}

	$.fn.inlineStyle = function (prop) {
		return this.prop('style')[jQuery.camelCase(prop)]; // returns property value or empty string
	};

	function checkHeaderElem($elem, $appendTo) {
		if ($($elem).length) {
			$($elem).appendTo($appendTo);
		}
	}

	//mobile-menu
	if ($w_w < 991) {
		let $appendTo = '#mobile-menu .container .row';
		let $appendToLogin = '#mobile-menu .container .row .header__mob-user';
		checkHeaderElem('.header-menu .container .row > ul', $appendTo);
		checkHeaderElem('.header-menu .footer__phone', $appendTo);
		if ($($appendTo).find('.header__mob-user').length === 0) {
			$('<div class="header__mob-user"></div>').appendTo($appendTo);
			checkHeaderElem('.header-menu .header__user-icon', $appendToLogin);
			checkHeaderElem('.header-menu .header__profile', $appendToLogin);
		}
	}

	$(window).on('resize', function () {
		if ($w_w < 991) {
			let $appendTo = '#mobile-menu .container .row';
			let $appendToLogin =
				'#mobile-menu .container .row .header__mob-user';
			checkHeaderElem('.header-menu .container .row > ul', $appendTo);
			checkHeaderElem('.header-menu .footer__phone', $appendTo);
			if ($($appendTo).find('.header__mob-user').length === 0) {
				$('<div class="header__mob-user"></div>').appendTo($appendTo);
				checkHeaderElem(
					'.header-menu .header__user-icon',
					$appendToLogin
				);
				checkHeaderElem(
					'.header-menu .header__profile',
					$appendToLogin
				);
			}
		} else {
			let $appendTo = '.header-menu .container .row';
			let $empty = $('#mobile-menu .container .row');
			checkHeaderElem('.header-menu .footer__phone', $appendTo);
			checkHeaderElem('.header-menu .container .row > ul', $appendTo);
			checkHeaderElem('.header-menu .header__user-icon', $appendTo);
			checkHeaderElem('.header-menu .header__profile', $appendTo);
			$('body').removeClass('menu-open');
			$('#mobile-menu').removeClass('menu-open');
			$('.hamburger').removeClass('is-active');
			$empty.empty();
		}
	});

	//open mobile menu
	(function () {
		const $header = $('.mobile-menu');
		const $icon = $('.hamburger');
		const $body = $('body');
		$(document).on('click', '.hamburger', function () {
			$icon.toggleClass('is-active');
			$header.toggleClass('menu-open');
			$body.toggleClass('menu-open');
		});
	})();

	//left menu
	$('.left-menu__list > .left-menu__list-title, .left-menu__list > i').on(
		'click',
		function () {
			$(this)
				.parent()
				.children('ul')
				.slideToggle(300);
			$(this)
				.parent()
				.toggleClass('open');
		}
	);

	//range slider
	$('.js-range-slider').ionRangeSlider({
		type: 'double',
		grid: false,
		step: '0.01'
	});

	//header slider width

	let $sliderOffset = 30;

	function headerSlider() {
		if ($w_w < 991) {
			$sliderOffset = 15;
		}
		if ($('.inner-header__slider').length) {
			let $back =
				$('.inner-header__category .row .back').outerWidth(true) +
				$sliderOffset;
			let $span = $(
				'.inner-header__category .row .main-title'
			).outerWidth(true);
			let $parent = $('.inner-header__category .row').width();
			let $slider_width = $parent - $span - $back;
			let $slideWidth = 0;

			$('.inner-header__category .inner-header__slider').css(
				'width',
				$slider_width
			);

			$('.inner-header__slider .swiper-slide').each(function () {
				$slideWidth += $(this).outerWidth(true);
			});

			if ($slideWidth > $slider_width) {
				// раскомментить
				const mySwiper = new Swiper('.swiper-container', {
					loop: false,
					slidesPerView: 'auto',
					navigation: {
						nextEl: '.swiper-next',
						prevEl: '.swiper-prev'
					}
				});
				console.log(mySwiper);

				$('.inner-header__slider').removeClass('slider-init');
			} else {
				$('.inner-header__slider').addClass('no-slider');
			}
		}
	}

	setTimeout(function () {
		headerSlider();
	}, 300);

	$(window).on('resize', function () {
		headerSlider();
	});
	//up button
	$('section:not(.history-page)')
		.find('#up')
		.on('click', function (e) {
			e.preventDefault();
			$('html, body').animate({scrollTop: 0}, '500');
		});

	$('.history-page')
		.find('#up')
		.on('click', function (e) {
			e.preventDefault();
		});

	//tabs product page
	// $('.product__tabs')
	// 	.find('.product__tab')
	// 	.on('click', function () {
	// 		let $parent = $(this).parent();
	// 		let $eq = $(this).index();
	// 		$parent.find('.product__tab').removeClass('active');
	// 		$(this).addClass('active');
	// 		$('.product__content .product__tab-content').fadeOut(300);
	// 		setTimeout(function () {
	// 			$('.product__content .product__tab-content').removeClass(
	// 				'acitve'
	// 			);
	// 			$('.product__content .product__tab-content')
	// 				.eq($eq)
	// 				.addClass('active')
	// 				.fadeIn();
	// 		}, 300);
	// 	});

	//counter

	$('.counter_catalog').find('input').on('keyup', function () {
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9.]/g, '');
		}
	});

	// $('.counter_catalog .counter__min').on('click', function () {
	// 	let $val = $(this).next().children('input').val();
	// 	let $step = $(this).next().children('input').data('step');
	// 	$step *= 1;
	// 	$val *= 1;
	//
	// 	if ($val > $step) {
	// 		$val -= $step;
	// 		$(this).next().children('input').val($val);
	// 	}
	// });
	// $('.counter_catalog .counter__plus').on('click', function () {
	// 	let $step = $(this).prev().children('input').data('step');
	// 	let $val = $(this).prev().children('input').val();
	// 	$step *= 1;
	// 	$val *= 1;
	// 	$val += $step;
	// 	$(this).prev().children('input').val($val);
	// });

	//product card slider
	$('.product-cart__slider .row').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		asNavFor: '.product-cart__thumb'
	});
	$('.product-cart__thumb').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		dots: false,
		asNavFor: '.product-cart__slider .row'
	});

	//input title

	$('.form__item .form-input').focus(function () {
		$(this)
			.parent()
			.addClass('focus');
	});

	$('.form__item .form-input').focusout(function () {
		let $val = $(this).val();
		if ($val === '' || $val === '+375 (__) ___-__-__') {
			$(this)
				.parent()
				.removeClass('focus');
		}
	});

	$('.form__item').each(function () {
		let $val = $(this)
			.children('.form-input').val();

		if ($val !== '') {
			$(this).addClass('focus');
		}

	});

	//show password

	$('.show-password').on('click', function () {
		let $attr = $(this)
			.parent()
			.children('input')
			.attr('type');
		let $input = $(this)
			.parent()
			.children('input');
		if ($attr === 'text') {
			$input.attr('type', 'password');
			$(this)
				.parent()
				.addClass('hide');
		} else {
			$input.attr('type', 'text');
			$(this)
				.parent()
				.removeClass('hide');
		}
	});

	//masonry

	let masonryOptions = {
		itemSelector: '.history-page__item',
		horizontalOrder: true,
		gutter: 0
	};
	// initialize Masonry
	let $grid = $('.history-page__items');

	if ($('.history-page__items').length && $(window).width() > 575) {
		$grid.masonry(masonryOptions);
	}

	//more

	$('.more').on('click', function (e) {
		e.preventDefault();
		let $counter = 0;
		let $text = '';
		let $historyItem = $(this)
			.parent()
			.parent();

		if ($('.history-page__items').length && $(window).width() > 575) {
			$grid.masonry('destroy');
		}

		$(this)
			.parent()
			.parent()
			.find('.order-block__item')
			.each(function () {
				$counter++;
				if ($counter > 3) {
					if ($(this).hasClass('hidden')) {
						$(this).slideDown(200, function () {
							$(this).css({
								display: 'flex'
							});
						});
						$text = 'Свернуть';
						$(this).removeClass('hidden');
						$historyItem.addClass('open');
					} else {
						$(this).attr('style');
						$(this).slideUp(200);
						$(this).addClass('hidden');
						$historyItem.removeClass('open');
						$text = 'Подробнее';
					}
				}
			});
		$(this).text($text);
		if ($('.history-page__items').length && $(window).width() > 575) {
			$grid.masonry(masonryOptions);
			let count = 0;
			let $interval = setInterval(function () {
				if (count < 10) {
					$grid.masonry('destroy');
					$grid.masonry(masonryOptions);
					count++;
				} else {
					clearInterval($interval);
				}
			}, 25);
		}
	});

	//history more
	$('.history-page__more').on('click', function (e) {
		e.preventDefault();
		if ($('.history-page__items').length && $(window).width() > 575) {
			let count = 0;
			$grid.masonry(masonryOptions);
			let $interval = setInterval(function () {
				if (count < 5) {
					$grid.masonry('destroy');
					$grid.masonry(masonryOptions);
					count++;
				} else {
					clearInterval($interval);
				}
			}, 50);
		}
	});

	$('.btn--close').on('click touchstart', function () {
		$.fancybox.close();
	});

	//phone mask
	if ($('.phone').length) {
		$('.phone').mask('+375 (99) 999-99-99', {
			autoclear: false,
			selectOnFocus: true,
			clearIfNotMatch: true
		});
	}

	if ($('input[name=PHONE]').length) {
		$('input[name=PHONE]').attr('placeholder', '+375 (__) ___-__-__');
		$('input[name=PHONE]').mask('+375 (99) 999-99-99');

	}

	if ($('.datepicker-here').length) {
		$('.datepicker-here').datepicker();
	}

	if ($('input[name=DATE]').length) {
		$('input[name=DATE]').attr('autocomplete', 'off');
		$('input[name=DATE]').attr('maxlength', '10');
		$('input[name=DATE]').datepicker();
		$('input[name=DATE]').on('input', function () {
			$(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''));
		});
	}

	$('.reviews-page')
		.find('.reviews-page__more')
		.on('click', function (e) {
			e.preventDefault();
			if (
				$(this)
					.parent()
					.hasClass('show')
			) {
				$(this)
					.parent()
					.removeClass('show');
				$(this)
					.children('span')
					.text('Подробнее');
			} else {
				$(this)
					.parent()
					.addClass('show');
				$(this)
					.children('span')
					.text('Скрыть');
			}
		});

	$(function ($) {
		$('.lazy').lazy({
			visibleOnly: true
		});
	});

	$('.sort-arrow').on('click', function () {
		if ($(this).hasClass('up')) {
			$(this).removeClass('up');
		} else {
			$(this).addClass('up');
		}
	});

	$('.review-text').on('keypress', function (event) {
		let $regex = new RegExp('^[а-яА-Яa-zA-Z0-9.,-]+$');
		let $key = String.fromCharCode(
			!event.charCode ? event.which : event.charCode
		);
		if (!$regex.test($key)) {
			event.preventDefault();
			return false;
		}
	});

	$('.reciew_img input[type=file]').on('change', function () {
		let $val = $(this)
			.val()
			.replace(/C:\\fakepath\\/i, '');
		if ($val !== '') {
			$(this)
				.parent()
				.addClass('file-choose');
			$(this)
				.parent()
				.find('span')
				.remove();
			$('<span>' + $val + '</span>').appendTo($(this).parent());
		} else {
			$(this)
				.parent()
				.removeClass('file-choose');
			$(this)
				.parent()
				.find('span')
				.remove();
		}
	});

	$('.reciew_remove').on('click', function () {
		$(this).parent().find('input').val('');
		$(this).parent().find('span').html('');
		$(this).parent().removeClass('file-choose');
	});
	//validation

	let $form = $('#order_form');

	$.validator.addMethod('customemail',
		function (value) {
			return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([a-zA-Zа-яА-Я]{2,50})$/.test(value);
		},
		'Пожалуйста, введите корректный адрес электронной почты.'
	);

	$form.validate({
		rules: {
			PHONE: {
				required: true,
				minlength: 19,
				maxlength: 19
			},
			EMAIL: {
				required: {
					depends: function () {
						$(this).val($.trim($(this).val()));
						return true;
					}
				},
				customemail: true
			}
		},
		messages: {
			PHONE: {
				minlength: 'Это поле необходимо заполнить'
			}
		}
	});


	$('#order_form .btn').on('click', function (e) {
		e.preventDefault();
		$form.valid();

		$form.find('input').each(function () {
			$(this).val($.trim($(this).val()));
		});

		if ($form.valid()) {
			$form.submit();

			$form.find('input').each(function () {
				$(this).val('');
			});
		}
	});
	getBasket();

	$(document).on('click', '.product__cart', function () {
		if ($(this).hasClass('active')) {
			$(this)
				.closest('.product__actions')
				.find('.counter.counter_catalog')
				.removeClass('counter--hidden');
			$(this)
				.closest('.product__actions')
				.find('.product__like')
				.removeClass('offset');
			return;
		}
		$(this)
			.closest('.product__actions')
			.find('.counter.counter_catalog')
			.addClass('counter--hidden');
	});
	$(document).on('click', '.product__like', function () {
		BX.showWait();
		let action = '';
		let id = $(this).parents('.product__item').attr('data-id');
		if (!id) {
			id = $(this).parents('.detail_product').attr('data-id');
		}
		if (!id) {
			id = $(this).parents('.set__item').attr('data-id');
		}
		if ($(this).hasClass('active')) {
			action = 'delete';
		} else {
			action = 'add';
		}

		const tag = $(this);
		$.ajax('/local/ajax/favourites.php', {
			data: {
				action,
				id
			},
			dataType: 'json',
			success(result) {
				if (result.status === 'ok') {
					if (action === 'delete') {
						$(tag).removeClass('active');
					}
					if (action === 'add') {
						$(tag).addClass('active');
					}
				}
				BX.closeWait();
			},
			error() {
				BX.closeWait();
			}
		});
		return false;

	});

	getFavourites();
});
