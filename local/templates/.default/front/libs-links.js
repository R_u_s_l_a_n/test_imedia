export const libsJsLink = [
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/svg4everybody/dist/svg4everybody.min.js',
	'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
	'node_modules/slick-carousel/slick/slick.min.js',
	'node_modules/wow.js/dist/wow.min.js',
	'node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
	'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
	'node_modules/swiper/js/swiper.min.js',
	'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
	'node_modules/air-datepicker/dist/js/datepicker.min.js',
	'node_modules/jquery-lazy/jquery.lazy.min.js'
];

export const libsCssLink = [
	'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
	'node_modules/slick-carousel/slick/slick-theme.css',
	'node_modules/slick-carousel/slick/slick.css',
	'node_modules/animate.css/animate.min.css',
	'node_modules/bootstrap/dist/css/bootstrap-grid.min.css',
	'node_modules/ion-rangeslider/css/ion.rangeSlider.min.css',
	'node_modules/swiper/css/swiper.min.css',
	'node_modules/air-datepicker/dist/css/datepicker.min.css'
];
