<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$assetManager = \Bitrix\Main\Page\Asset::getInstance();

$assets = [
    "js" => [
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/jquery.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/slick.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/swiper.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/jquery.fancybox.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/wow.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/ion.rangeSlider.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/masonry.pkgd.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/jquery.mask.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/jquery.maskedinput.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/datepicker.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/jquery.lazy.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/validation/jquery.validate.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/js/validation/localization/messages_ru.min.js',"skip_moving" => false,"additional" => true],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/scripts/app.min.js',"skip_moving" => false,"additional" => true],
    ],
    "css" => [
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/bootstrap-grid.min.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/jquery.fancybox.min.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/slick-theme.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/slick.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/swiper.min.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/ion.rangeSlider.min.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/libs/css/datepicker.min.css',],
	    ["src" => DEFAULT_TEMPLATE_DIST_PATH . '/assets/styles/app.min.css',],
    ],
    "strings" => [
	    ["str" => '<meta charset="utf-8">',],
	    ["str" => '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">',],
	    ["str" => '<meta http-equiv="X-UA-Compatible" content="IE=edge">',],
	    ["str" => '<meta name="imagetoolbar" content="no">',],
	    ["str" => '<meta name="msthemecompatible" content="no">',],
	    ["str" => '<meta name="cleartype" content="on">',],
	    ["str" => '<meta name="HandheldFriendly" content="True">',],
	    ["str" => '<meta name="format-detection" content="telephone=no">',],
	    ["str" => '<meta name="format-detection" content="address=no">',],
	    ["str" => '<meta name="google" value="notranslate">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="57x57" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-57x57.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="60x60" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-60x60.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="72x72" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-72x72.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="76x76" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-76x76.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="114x114" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-114x114.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="120x120" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-120x120.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="144x144" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-144x144.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="152x152" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-152x152.png">',],
	    ["str" => '<link rel="apple-touch-icon" sizes="180x180" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/apple-icon-180x180.png">',],
	    ["str" => '<link rel="icon" type="image/png" sizes="192x192"  href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/android-icon-192x192.png">',],
	    ["str" => '<link rel="icon" type="image/png" sizes="32x32" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon-32x32.png">',],
	    ["str" => '<link rel="icon" type="image/png" sizes="96x96" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon-96x96.png">',],
	    ["str" => '<link rel="icon" type="image/png" sizes="16x16" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon-16x16.png">',],
	    ["str" => '<link rel="manifest" href="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/manifest.json">',],
	    ["str" => '<meta name="msapplication-TileColor" content="#ffffff">',],
	    ["str" => '<meta name="msapplication-TileImage" content="'.DEFAULT_TEMPLATE_DIST_PATH.'/favicon/ms-icon-144x144.png">',],
	    ["str" => '<meta name="theme-color" content="#ffffff">',],
	    ["str" => '<meta name="theme-color" content="#ffffff">',],
	    ["str" => '<meta name="apple-mobile-web-app-capable" content="yes">',],
	    ["str" => '<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">',],
	    ["str" => '<meta name="description" content="Description">',],
	    ["str" => '<meta name="keywords" content="">',],
	    ["str" => '<link href="https://fonts.googleapis.com/css?family=Gilda+Display&display=swap" rel="stylesheet">',],
    ],
];
foreach ($assets['js'] as $script) {
    if (!$script['skip_moving']) {
        $assetManager->addJs($script['src'], $script['additional']);
    } else {
        $assetManager->addString('<script data-skip-moving="true" type="text/javascript" src="' . $script['src'] . '"></script>');
    }
}
foreach ($assets['css'] as $style) {
    $assetManager->addCss($style['src'], $style['additional']);
}
foreach ($assets['strings'] as $string) {
    $assetManager->addString(
        $string['str'],
        $string['unique'] ?: false,
        $string['location'] ?: \Bitrix\Main\Page\AssetLocation::AFTER_JS_KERNEL,
        $string['mode'] ?: null
    );
}
