$(function(){
    
    	var sendWebForm = function(event, data){
    	var $thisForm = $(this);
        var formId = $thisForm.data("id");   
	  		$.ajax({
	  			url: mainPageFormAjaxDir+ "?FORM_ID=" + formId,
	  			data: $thisForm.serialize(),
			    cache: false,
		        type: "POST" ,
		        dataType: "json",
	  			success: function(response){
	  			  
                   $('.main_page_form_result').html('');
                   
	               if(response["SUCCESS"] != "Y"){
                      $i = 0;
                       $.each(response["ERROR"], function(nextId, nextValue){
                            if($i == 0){
                                 $('.main_page_form_result').append(nextValue);
                                 $i++;
                            }
                           
    		  			});
                   }
                   else{
                        $('.main_page_form_result').append('Форма успешно отправлена');
                        $thisForm[0].reset();
                   }
		  		}

	  		});
		return false;

	}

	$(document).on("submit", "#main_page_form", sendWebForm);

});