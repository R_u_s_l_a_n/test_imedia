<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div class="inner-header__slider slider-init swiper-slider">
	<div class="swiper-arrow swiper-prev">
		<i></i>
	</div>
	<div class="swiper-container">
		<div class="swiper-wrapper">
        <?
        foreach($arResult as $arItem):
        	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
        		continue;
        ?>
        	<?if($arItem["SELECTED"]):?>
                <a href="<?=$arItem["LINK"]?>" class="swiper-slide login-link active"><?=$arItem["TEXT"]?></a>
        	<?else:?>
                <a href="<?=$arItem["LINK"]?>" class="swiper-slide login-link"><?=$arItem["TEXT"]?></a>
        	<?endif?>
        <?endforeach?>
            <a href="?logout=yes" class="swiper-slide logout-link"><i></i> Выход</a>
        </div>
	</div>
	<div class="swiper-arrow swiper-next">
		<i></i>
	</div>
</div>
<?endif?>