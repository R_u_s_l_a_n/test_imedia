<?php

use Bitrix\Sale;
use Bitrix\Main\Loader;
Loader::includeModule('sale');
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$context = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId());
$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $context);
if($discounts){
    $r = $discounts->calculate();
    $result = $r->getData();
    if (isset($result['BASKET_ITEMS']))
    {
        $r = $basket->applyDiscount($result['BASKET_ITEMS']);
    }
}

$arResult["PRICE"] = CCurrencyLang::CurrencyFormat(
	$basket->getPrice(),
	Sale\Internals\SiteCurrencyTable::getSiteCurrency(SITE_ID),
	true
);