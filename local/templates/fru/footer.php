<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="insta-slider">
    <div class="container">
        <div class="row d-flex flex-column">
            <div class="main-title">Мы <br> в Instagram</div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array("AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/soc.php"),
                    false
                ); ?>
        </div>
    </div>
    <?$APPLICATION->IncludeComponent("zaiv:instagramgallerypro", "fru", Array(
    	"ADD_JQUERY" => "Y",	// Подключить jQuery
    		"ADD_PLUGIN" => "Y",	// Подключить плагин галереи
    		"BLACKLIST" => "",	// Чёрный список логинов пользователей
    		"CACHE_TIME" => "43200",	// Время кеширования (сек.)
    		"CACHE_TYPE" => "A",	// Тип кеширования
    		"COMPONENT_TEMPLATE" => ".default",
    		"DEL_SPEC_SYMBOLS_IN_DESCRIPTION" => "N",	// Удалять знак "?" в описании к медиа
    		"MEDIA_COUNT" => "9",	// Кол-во отображаемых записей
    		"MEDIA_RESOLUTION_DETAIL" => "R1080",	// Размер медиа в детальном просмотре
    		"MEDIA_RESOLUTION_PREVIEW" => "R150",	// Размер медиа в превью
    		"NOINDEX_LINKS" => "Y",	// Закрыть ссылки от индексирования
    		"NOINDEX_WIDGET" => "Y",	// Закрыть галерею от индексирования
    		"PLUGIN_TYPE" => "FANCYBOX3",	// Плагин галереи
    		"SHOW_MEDIA_DESCRIPTION" => "N",	// Показывать описания для медиа
    		"SHOW_PARSING_ERRORS" => "N",	// Показывать ошибки обработки
    		"SHOW_SIDECAR_ICON" => "Y",	// Показывать иконку "Слайдер"
    		"SHOW_SIDECAR_ITEMS_ON_PAGE" => "Y",	// Отображать фото для типа медиа "Слайдер" на странице
    		"SHOW_SIDECAR_MEDIAS" => "Y",	// Показывать все фото для типа медиа "Слайдер"
    		"SHOW_TYPE" => "INSTAGRAM",	// Тип показа
    		"SHOW_VIDEO_ICON" => "Y",	// Показывать иконку "Видео"
    		"SHOW_VIDEO_IMAGE_PREVIEW" => "Y",	// Заменить видео на картинку анонса видео
    		"SHOW_VIDEO_MEDIAS" => "Y",	// Показывать медиа типа "Видео"
    		"SOURCE" => "",	// Список хэштегов
    		"SOURCE_TYPE" => "TOKEN",	// Тип источника
    		"USE_BLACKLIST" => "N",	// Включить "чёрный список"
    	),
    	false
    );?>
   <!-- <div class="insta-slider__slider">
        <div class="insta-slider__slide"  style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom1.png');">
            <a href="" class="like">
                234
            </a>
        </div>
        <div class="insta-slider__slide"  style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom2.png');">
            <a href="" class="like">
                122
            </a>
        </div>
        <div class="insta-slider__slide" style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom3.png');">

            <a href="" class="like">432
            </a>
        </div>
        <div class="insta-slider__slide" style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom4.png');">
            <a href="" class="like">122
            </a>
        </div>
        <div class="insta-slider__slide"  style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom1.png');">
            <a href="" class="like">
                234
            </a>
        </div>
        <div class="insta-slider__slide" style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom2.png');">
            <a href="" class="like">
                122
            </a>
        </div>
        <div class="insta-slider__slide" style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom3.png');">
            <a href="" class="like">432
            </a>
        </div>
        <div class="insta-slider__slide"  style="background-image: url('<?=DEFAULT_TEMPLATE_DIST_PATH?>/assets/images/bottom4.png');">
            <a href="" class="like">122
            </a>
        </div>
    </div>-->
</section>

</main>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="footer__items d-flex flex-wrap">
                <div class="col-md-12 col-lg-6 footer__left">
                    <a href="" class="footer__logo">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array("AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/logo-footer.php"),
                            false
                        ); ?>
                    </a>
                    <div class="footer__phone">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array("AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/phone-footer.php"),
                            false
                        ); ?>
                    </div>
                    <div class="footer__soc">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array("AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/mail.php"),
                            false
                        ); ?>
                        
                         <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array("AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR . "include/soc.php"),
                            false
                        ); ?>
                        
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 footer__menu">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
                    	"COMPONENT_TEMPLATE" => ".default",
                    		"ROOT_MENU_TYPE" => "footer",	// Тип меню для первого уровня
                    		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    		"MAX_LEVEL" => "1",	// Уровень вложенности меню
                    		"CHILD_MENU_TYPE" => "footer",	// Тип меню для остальных уровней
                    		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    		"DELAY" => "N",	// Откладывать выполнение шаблона меню
                    		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    	),
                    	false
                    );?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid copyright">
        <div class="row">
            <div class="container">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array("AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/pay_img.php"),
                    false
                ); ?>
                <div class="copyright__link">
                    <p>© 2009-2020</p>
                    <a href="//imedia.by">Разработка: <span>Imedia Solutions</span></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="dialogs">

</div>

<div id="login-popup" class="popup popup--login">
    <?$APPLICATION->IncludeComponent(
    	"bitrix:system.auth.form", 
    	"fru", 
    	array(
    		"FORGOT_PASSWORD_URL" => "",
    		"PROFILE_URL" => "/personal/",
    		"REGISTER_URL" => "/register/",
    		"SHOW_ERRORS" => "Y",
    		"COMPONENT_TEMPLATE" => "fru"
    	),
    	false
    );?>

</div>

<div id="restore-popup" class="popup popup--restore">
    <?$APPLICATION->IncludeComponent("bitrix:main.auth.forgotpasswd", "fru", Array(
    	"AUTH_AUTH_URL" => "",	// Страница для авторизации
    		"AUTH_REGISTER_URL" => "",	// Страница для регистрации
    	),
    	false
    );?>
</div>

<div id="restore-popup2" class="popup popup--restore popup--restore-second">
	<div class="popup__title">Восстановление <br>
		пароля</div>
	<p>Мы отправили Вам письмо для <br> сброса пароля на почтовый ящик.</p>
	<p>Пожалуйста, перейдите по ссылке <br> в письме и задайте новый пароль.</p>
	<a href="javascript:;" data-fancybox data-src="#login-popup" class="btn btn--white">Вход</a>
</div>

<div id="order-popup" class="popup popup--restore popup--order">
    <div class="popup__title">Заказ
        <br>подтвержден
    </div>
    <p>
        Спасибо за Ваш заказ! <br>
Наш менеджер свяжется <br>
        с Вами в течении часа <br>
        после оформления заказа!
    </p>
    <div class="payment_btn"></div>
    <a href="/" class="order_ok_btn btn btn--white btn--close">Ок</a>
</div>
<div id="account-popup" class="popup popup--restore popup--account">
    <div class="popup__title">Аккаунт создан
    </div>
    <p>
        Спасибо за регистарцию!
    </p>
    <a href="/personal/" class="btn btn--white btn--close">Ок</a>
</div>
<div id="logout-popup" class="popup popup--restore popup--logout">
    <div class="popup__title">Выйти <br>
        из аккаунта?
    </div>
    <a href="?logout=yes" class="btn">Выйти</a>
    <div class=" btn btn--white btn--close">Отмена</div>
</div>
<div id="request-popup" class="popup popup--request">
     <?$APPLICATION->IncludeComponent(
    	"bitrix:form.result.new", 
    	"offer_cooperation", 
    	array(
    		"CACHE_TIME" => "360000",
    		"CACHE_TYPE" => "Y",
    		"CHAIN_ITEM_LINK" => "",
    		"CHAIN_ITEM_TEXT" => "",
    		"COMPONENT_TEMPLATE" => "offer_cooperation",
    		"EDIT_URL" => "",
    		"IGNORE_CUSTOM_TEMPLATE" => "N",
    		"LIST_URL" => "",
    		"SEF_MODE" => "N",
    		"SUCCESS_URL" => "",
    		"USE_EXTENDED_ERRORS" => "Y",
    		"WEB_FORM_ID" => "2",
    		"VARIABLE_ALIASES" => array(
    			"WEB_FORM_ID" => "WEB_FORM_ID",
    			"RESULT_ID" => "RESULT_ID",
    		)
    	),
    	false
    );?>
</div>

</body>
</html>


