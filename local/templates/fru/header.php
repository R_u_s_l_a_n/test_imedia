<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
$curPage = $APPLICATION->GetCurDir();
$bIndex = $curPage == SITE_DIR;
require_once('assets_include.php');

$dir = $APPLICATION->GetCurDir();
$arDir = explode('/', $dir);
global $USER;
if (!$USER->IsAuthorized()){
    if($arDir[1] == 'personal'){
        header('Location: /register/');
        exit;
    }
}
?>
<!DOCTYPE html>
<html class="no-js" lang="<?=LANGUAGE_ID?>">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
	<? $APPLICATION->ShowHead(); ?>
</head>
<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<section class="header-menu header-menu--login">
    <div class="container">
        <div class="row">
            <a  href="/" class="header__logo">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array("AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/logo-header.php"),
                    false
                ); ?>
            </a>
            
            <div class="footer__phone">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array("AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR . "include/phone-header.php"),
                    false
                ); ?>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "main", Array(
            	"COMPONENT_TEMPLATE" => ".default",
            		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
            		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
            		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
            		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
            		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
            		"MAX_LEVEL" => "1",	// Уровень вложенности меню
            		"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
            		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            		"DELAY" => "N",	// Откладывать выполнение шаблона меню
            		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            	),
            	false
            );?>
            <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"header_basket", 
	array(
		"HIDE_ON_BASKET_PAGES" => "N",
		"PATH_TO_AUTHORIZE" => "",
		"PATH_TO_BASKET" => SITE_DIR."cart/",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "N",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_PRODUCTS" => "N",
		"SHOW_REGISTRATION" => "N",
		"SHOW_TOTAL_PRICE" => "Y",
		"COMPONENT_TEMPLATE" => "header_basket"
	),
	false
);?>
			
            <?
            global $USER;
            if ($USER->IsAuthorized()):?>
            <a href="/personal/" class="header__user-icon"></a>
            <div data-fancybox data-src="#logout-popup"  class="header__profile">Выйти</div>
            <?else:?>
            <div data-fancybox data-src="#login-popup" class="header__profile">Войти</div>
            <?endif;?>

            <div class="hamburger">
                <div class="hamburger__box">
                    <div class="hamburger__inner"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="mobile-menu" class="mobile-menu">
        <div class="container">
            <div class="row"></div>
        </div>
    </div>
</section>

<main class="<?=$APPLICATION->ShowProperty('MAIN_CLASS')?>">
    <?if($dir != SITE_DIR):?>
    <section class="inner-header <?=$APPLICATION->ShowProperty('SECOND_CLASS')?>" style="background-image: url( <?=$APPLICATION->ShowProperty('HEADER_BACKGROUND')?>)">
        <div class="container">
            <div class="row">
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "fru", Array(
                	),
                	false
                );?>
                <div class="inner-header__category">
                    <div class="row">
                        <!--<a href="javascript:history.back()" class="back"></a>-->
                        <div class="main-title"><?$APPLICATION->ShowTitle(false)?></div>
	                    <?$APPLICATION->ShowViewContent('product_manufactory');?>
	                    <?$APPLICATION->ShowViewContent('catalog_section_list');?>
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", Array(
                        	"COMPONENT_TEMPLATE" => ".default",
                        		"ROOT_MENU_TYPE" => "personal",	// Тип меню для первого уровня
                        		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        		"MAX_LEVEL" => "1",	// Уровень вложенности меню
                        		"CHILD_MENU_TYPE" => "personal",	// Тип меню для остальных уровней
                        		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        		"DELAY" => "N",	// Откладывать выполнение шаблона меню
                        		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        	),
                        	false
                        );?>
                        <?
                        if(($dir == SITE_DIR.'register/') || ($dir == SITE_DIR.'register-company/')):?>
                            <div class="inner-header__slider swiper-slider">
    							<div class="swiper-arrow swiper-prev">
    								<i></i>
    							</div>
    							<div class="swiper-container">
    								<div class="swiper-wrapper">
    									<a href="<?=SITE_DIR?>register/" class="swiper-slide login-link <?if($dir == SITE_DIR.'register/'):?>active<?endif;?>">Физическое лицо</a>
    									<a href="<?=SITE_DIR?>register-company/" class="swiper-slide login-link <?if($dir == SITE_DIR.'register-company/'):?>active<?endif;?>">Юридическое лицо</a>
    								</div>
    							</div>
    							<div class="swiper-arrow swiper-next">
    								<i></i>
    							</div>
    						</div>
                        <?endif;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?endif;?>