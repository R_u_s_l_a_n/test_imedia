<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/header-inner1.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product');?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "", 
    array("AREA_FILE_SHOW" => "file", 
    "PATH" => SITE_DIR . "include/order.php"),
    false,
    array("HIDE_ICONS" => "Y")
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>