<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/header-inner2.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product');
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "", 
    array("AREA_FILE_SHOW" => "file", 
    "PATH" => SITE_DIR . "include/pay_page/pay_page.php"),
    false,
    array("HIDE_ICONS" => "Y")
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>