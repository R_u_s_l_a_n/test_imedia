<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" Избранное");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/favorite-bg.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product inner-header--login');
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."include/favorite.php"
	),
false,
Array(
	'HIDE_ICONS' => 'Y'
)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>