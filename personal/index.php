<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/header-inner2.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product');
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.profile", 
	"user_profile", 
	array(
		"CHECK_RIGHTS" => "N",
		"SEND_INFO" => "N",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(
			0 => "UF_ADDRESS",
			1 => "UF_CITY",
			2 => "UF_STREET",
			3 => "UF_HOUSE",
			4 => "UF_ENTER",
			5 => "UF_FLOOR",
			6 => "UF_FLAT",
		),
		"USER_PROPERTY_NAME" => "",
		"COMPONENT_TEMPLATE" => "user_profile"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>