<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/favorite-bg.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product inner-header--login register-header');
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	"company_register", 
	array(
		"COMPONENT_TEMPLATE" => "company_register",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
			3 => "WORK_COMPANY",
		),
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
			3 => "WORK_COMPANY",
		),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(
			0 => "UF_YNP",
			1 => "UF_ADDRESS",
		),
		"USER_PROPERTY_NAME" => ""
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>