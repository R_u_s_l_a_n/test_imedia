<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/favorite-bg.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product inner-header--login register-header');
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	"user_register", 
	array(
		"COMPONENT_TEMPLATE" => "user_register",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
		),
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
		),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(
			1 => "UF_CITY",
			2 => "UF_STREET",
			3 => "UF_HOUSE",
			4 => "UF_ENTER",
			5 => "UF_FLOOR",
			6 => "UF_FLAT",
		),
		"USER_PROPERTY_NAME" => ""
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>