<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мы в соц. сетях");
$APPLICATION->SetPageProperty('HEADER_BACKGROUND','/local/templates/.default/front/dist/assets/images/header-inner2.png');
$APPLICATION->SetPageProperty('SECOND_CLASS','inner-header__product');
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "", 
    array("AREA_FILE_SHOW" => "file", 
    "PATH" => SITE_DIR . "include/social-media-page.php"),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>